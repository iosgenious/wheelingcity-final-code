//
//  NewsWithNoLoginViewController.m
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 07/06/11.
//  Copyright 2011 W. All rights reserved.
//

#import "NewsWithNoLoginViewController.h"


@implementation NewsWithNoLoginViewController
@synthesize selectedCellTableView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"News";
    [text setText:selectedCellTableView];
    //change the size if we want
    text.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16)];
    text.textColor=[UIColor yellowColor];
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle:@"Wheeling City App" 
                          message:@"Please go back and log in Facebook to see the comments of the news" 
                          delegate:nil 
                          cancelButtonTitle:@"OK" 
                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
    text.dataDetectorTypes = UIDataDetectorTypeLink;
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
