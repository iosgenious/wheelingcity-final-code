//
//  NewsWithNoLoginViewController.h
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 07/06/11.
//  Copyright 2011 W. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NewsWithNoLoginViewController : UIViewController {
    
    NSString *selectedCellTableView;
    IBOutlet UITextView *text;
    
}
@property(nonatomic, retain) NSString *selectedCellTableView;


@end
