    //
//  JITHAppDelegate.m
//  JITH
//
//  Created by Urko Fernandez on 3/24/10.
//  Copyright 2010 W. All rights reserved.
// 

#import "JITHAppDelegate.h"
#import "ASIFormDataRequest.h"
#import <CommonCrypto/CommonDigest.h>


//Production APS keys
/*
 #define kApplicationKey @""
 #define kApplicationSecret @""
 */

//Development APS keys (through Urban Airship)
#define kApplicationKey @"b895yEeZQDq9VFaBNBQaeQ"
#define kApplicationSecret @"gSzAk9TZT7euDbZuRMDNLQ"


@implementation JITHAppDelegate

@synthesize window;
@synthesize myNavController;
@synthesize iTunesURL;
@synthesize infoMessage;
@synthesize deviceToken;
@synthesize deviceAlias;
@synthesize hostReach,check3GConnection,checkWifiConnection,internetReach,wifiReach,myTimer;



- (void)applicationDidFinishLaunching:(UIApplication *)application {
	
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];

    //Register for notifications
	[[UIApplication sharedApplication]
	 registerForRemoteNotificationTypes:(UIRemoteNotificationTypeSound |
										 UIRemoteNotificationTypeAlert)];
    
    [window setRootViewController:myNavController];
    [window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    [[UIApplication sharedApplication]
	 registerForRemoteNotificationTypes:(UIRemoteNotificationTypeSound |
										 UIRemoteNotificationTypeAlert)];
    
	[window addSubview:myNavController.view];
	[window makeKeyAndVisible];
	return TRUE;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if (self.myTimer) {
        [self.myTimer invalidate];
    }
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
       /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    if (mbSpinner) {
        [mbSpinner removeFromSuperview];
    }
    mbSpinner=[[MBProgressHUD alloc] initWithView:self.window];
    [self.window addSubview:mbSpinner];
    mbSpinner.delegate=self;
    mbSpinner.labelText=@"Updating";
    [mbSpinner show:true];
    dataBaseSingleton = [[DataBaseSingleton alloc] init];
	[dataBaseSingleton setDatabaseVars];
	[dataBaseSingleton checkAndCreateDatabase];
    
}



#pragma mark -
#pragma mark Reachability

- (BOOL) updateInterfaceWithReachability: (Reachability*) curReach
{
	NetworkStatus netStatus = [curReach currentReachabilityStatus];
	BOOL connectionRequired= [curReach connectionRequired];
	
	BOOL retVal = FALSE;
    if(curReach == hostReach)
	{
        if(connectionRequired)
        {
			retVal = NO;
        }
        else
        {
			retVal = YES;
        }
    }
	if(curReach == internetReach)
	{
		switch (netStatus)
		{
			case NotReachable:
			{
				retVal  = NO;
				break;
			}
				
			case ReachableViaWWAN:
			{
				retVal = YES;
				break;
			}
			case ReachableViaWiFi:
			{
				retVal = YES;
				break;
			}
		}
		
	}
	if(curReach == wifiReach)
	{
		switch (netStatus)
		{
			case NotReachable:
			{
				retVal  = NO;
				break;
			}
				
			case ReachableViaWWAN:
			{
				retVal = YES;
				break;
			}
			case ReachableViaWiFi:
			{
				retVal = YES;
				break;
			}
		}
		
	}
	
	return retVal;
}

- (BOOL)isReachable {
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
	hostReach = [Reachability reachabilityWithHostName: @"www.apple.com"] ;
	[hostReach startNotifier];
	BOOL recHost = [self updateInterfaceWithReachability: hostReach];
	
    internetReach = [Reachability reachabilityForInternetConnection] ;
	[internetReach startNotifier];
	BOOL recInt = [self updateInterfaceWithReachability: internetReach];
	
    wifiReach = [Reachability reachabilityForLocalWiFi] ;
	[wifiReach startNotifier];
	BOOL recWifi = [self updateInterfaceWithReachability: wifiReach];
	
    if (recWifi) {
		self.checkWifiConnection = YES;
	}else if (recHost && recInt) {
		self.check3GConnection = YES;
	}
	else {
		self.checkWifiConnection = NO;
		self.check3GConnection = NO;
	}
	
	if ((recHost && recInt) || recWifi) {
		hostActive = YES;
	}
	else {
		
		hostActive = NO;
	}
	return hostActive;
}

- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateInterfaceWithReachability: curReach];
}


// Push notification methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)_deviceToken {
	// Get a hex string from the device token with no spaces or < >
	self.deviceToken = [[[[_deviceToken description] stringByReplacingOccurrencesOfString:@"<"withString:@""] 
						 stringByReplacingOccurrencesOfString:@">" withString:@""] 
						stringByReplacingOccurrencesOfString: @" " withString: @""];
	
	
	if ([application enabledRemoteNotificationTypes] == 0) {
		NSLog(@"Notifications are disabled for this application. Not registering with Urban Airship");
		return;
	}
	
	
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
    self.deviceAlias = [userDefaults stringForKey: @"_UADeviceAliasKey"];
	
	// Display the network activity indicator
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	// We like to use ASIHttpRequest classes, but you can make this register call how ever you like
	// just notice that it's an http PUT
	NSOperationQueue *queue = [[[NSOperationQueue alloc] init] autorelease];
	NSString *UAServer = @"https://go.urbanairship.com";
	NSString *urlString = [NSString stringWithFormat:@"%@%@%@/", UAServer, @"/api/device_tokens/", self.deviceToken];
	NSURL *url = [NSURL URLWithString:  urlString];
	ASIHTTPRequest *request = [[[ASIHTTPRequest alloc] initWithURL:url] autorelease];
	request.requestMethod = @"PUT";
	
	// Send along our device alias as the JSON encoded request body
	if(self.deviceAlias != nil && [self.deviceAlias length] > 0) {
		[request addRequestHeader: @"Content-Type" value: @"application/json"];
		[request appendPostData:[[NSString stringWithFormat: @"{\"alias\": \"%@\"}", self.deviceAlias]
								 dataUsingEncoding:NSUTF8StringEncoding]];
	}
	
	// Authenticate to the server
	request.username = kApplicationKey;
	request.password = kApplicationSecret;
	
	[request setDelegate:self];
	[request setDidFinishSelector: @selector(successMethod:)];
	[request setDidFailSelector: @selector(requestWentWrong:)];
	[queue addOperation:request];
	
	
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *) error {
	NSLog(@"Failed to register with error: %@", error);
}

- (void)successMethod:(ASIHTTPRequest *) request {
	NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setValue: self.deviceToken forKey: @"_UALastDeviceToken"];
	[userDefaults setValue: self.deviceAlias forKey: @"_UALastAlias"];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)requestWentWrong:(ASIHTTPRequest *)request {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	NSError *error = [request error];
	UIAlertView *someError = [[UIAlertView alloc] initWithTitle: 
							  @"Network error" message: @"Error registering with server"
													   delegate: self
											  cancelButtonTitle: @"Ok"
											  otherButtonTitles: nil];
	[someError show];
	[someError release];
	NSLog(@"ERROR: NSError query result: %@", error);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //NSLog(@"remote notification: %@",[userInfo description]);
    
	//if (no_push){
    //NSString *message = [userInfo descriptionWithLocale:nil indent:1];
    NSString *message = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
    [alert release];
}
// -- End of push notification methods


// Process a LinkShare/TradeDoubler/DGM URL to something iPhone can handle
- (void)openReferralURL:(NSURL *)referralURL {
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:referralURL] delegate:self startImmediately:YES];
    [conn release];
	//NSLog(@"this is the link we are opening %@", referralURL);
}

// Save the most recent URL in case multiple redirects occur
// "iTunesURL" is an NSURL property in your class declaration
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response {
    self.iTunesURL = [response URL];
	//NSLog(@"Is self.itunerurl being assigned? %@", self.iTunesURL);
	return request;
}

// No more redirects; use the last URL saved
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [[UIApplication sharedApplication] openURL:self.iTunesURL];
}

- (void) updatingDatabase {
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkTimeout) userInfo:nil repeats:YES];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

	//UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
	//av.frame=CGRectMake(145, 230, 25, 25);
	//av.tag  = 1;
	//[self.window addSubview:av];
	//[av startAnimating];
}

- (void) sponsorTimeout {
    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkTimeoutForSponsor) userInfo:nil repeats:YES];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void) finishedUpdatingDatabase {
    [self sponsorTimeout];
    //UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.window viewWithTag:1];
	//[tmpimg removeFromSuperview];
    
}

- (void) dismissSponsorView {
    JITHAppDelegate *del = (JITHAppDelegate *)[UIApplication sharedApplication].delegate;
    [del.myNavController dismissModalViewControllerAnimated:YES];
    del.myNavController.view.hidden = FALSE;   
    if (self.myTimer) {
        [mbSpinner hide:true];
        [self.myTimer invalidate];
    }
}

- (void)checkTimeout
{
    static int number = 0;
    number ++;
    NSLog(@"Counter while loading database? is at %d", number);

    if(number == 10)
    {
        number=0;
        [mbSpinner hide:true];
        [self dismissSponsorView];
        
    }
}

- (void)checkTimeoutForSponsor
{
    static int number = 0;
    number ++;
    if(number == 3)
    {
        number=0;
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [mbSpinner hide:true];
        [self dismissSponsorView];
    }
}

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [mbSpinner removeFromSuperview];
    [mbSpinner release];
	mbSpinner = nil;
}


 
 - (void)dealloc {
     [myNavController release];self.myNavController=nil;
     [window release];self.window=nil;
     [infoMessage release];	self.infoMessage=nil;
     [iTunesURL release];self.iTunesURL=nil;
     [super dealloc];
 }


@end
