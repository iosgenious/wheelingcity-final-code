//
//  YouTubeModel.h
//  JITH
//
//  Created by Santosh Singh on 07/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YouTubeModel : NSObject
@property(nonatomic,retain)NSString *title;
@property(nonatomic,retain)NSString *imageUrl;
@property(nonatomic,retain)NSString *likes;
@property(nonatomic,retain)NSString *views;
@property(nonatomic,retain)NSString *duration;
@property(nonatomic,retain)NSString *videoUrl;
@property(nonatomic,retain)NSString *description;
@property(nonatomic,retain)NSString *publisheddate;

+(YouTubeModel*)youTubeTyPeWithParameter :(NSMutableDictionary *)dictValue;
@end
