//
//  WhatsHappeningViewController.h
//  JITH
//
//  Created by Denis Santxez on 4/11/11.
//  Copyright 2011 W. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "WhatsHappeningInfoViewController.h"
#import "MBProgressHUD.h"
#import "Kal.h"
#import "GDataCalendar.h"

@interface WhatsHappeningViewController:UIViewController<UIActionSheetDelegate,MBProgressHUDDelegate,KalDataSource,KalViewDelegate,UITableViewDelegate> {
	NSMutableArray *dataArray; 
	NSMutableDictionary *dataDictionary;
    WhatsHappeningInfoViewController* myWhatsHappeningInfoViewController;
    UIImageView* background;
    KalViewController *kal;
    dispatch_queue_t eventStoreQueue; // Serializes access to eventStore and offloads the query work to a background thread.
    EKEventStore *eventStore;
    NSInteger W2Wmonth, W2Wyear;
    NSArray *daysOfEachMonth;
    
    //Event List
    
    NSMutableArray *eventList;
}

@property(nonatomic,retain) GDataServiceGoogleCalendar *calendarService;
@property (retain,nonatomic) NSMutableArray *dataArray;
@property (retain,nonatomic) NSMutableDictionary *dataDictionary;
@property (retain,nonatomic) WhatsHappeningInfoViewController* myWhatsHappeningInfoViewController;
@property (retain,nonatomic) UIImageView* background;



@end
