//
//  JITHAppDelegate.h
//  JITH
//
//  Created by Urko Fernandez on 3/24/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "MBProgressHUD.h"
#import "Reachability.h"

@class Artist;

@interface JITHAppDelegate : NSObject <UIApplicationDelegate,MBProgressHUDDelegate> {
    DataBaseSingleton *dataBaseSingleton;
    UIWindow *window;
    UINavigationController *myNavController;
	NSURL *iTunesURL;
	NSString *infoMessage;
	NSString *deviceToken;
	NSString *deviceAlias;
    UIImageView *splashView;
    MBProgressHUD *mbSpinner;
    //BOOL no_push;
    
    BOOL hostActive;
	BOOL checkWifiConnection;
	BOOL check3GConnection;
    Reachability* hostReach;
    Reachability* internetReach;
    Reachability* wifiReach;
}
@property(nonatomic,retain)NSTimer* myTimer;
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *myNavController;
@property (nonatomic, retain) NSURL *iTunesURL;
@property (nonatomic, retain) NSString *infoMessage;
@property (nonatomic, retain) NSString *deviceToken;
@property (nonatomic, retain) NSString *deviceAlias;

@property (nonatomic , assign)BOOL checkWifiConnection;
@property (nonatomic , assign)BOOL check3GConnection;
@property (nonatomic , retain)Reachability * hostReach;
@property (nonatomic , retain)Reachability * internetReach;
@property (nonatomic , retain)Reachability * wifiReach;

- (void)openReferralURL:(NSURL *)referralURL;
- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)response;
- (void)connectionDidFinishLoading:(NSURLConnection *)connection;
- (void) updatingDatabase;
- (void) checkTimeout;
- (void) checkTimeoutForSponsor;
- (void) sponsorTimeout;
- (void) finishedUpdatingDatabase;
- (void) dismissSponsorView;
- (BOOL)isReachable ;
@end