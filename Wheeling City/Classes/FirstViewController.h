//
//  FirstViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/24/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "HomePagerViewController.h"
#import "LodgingViewController.h"
#import "AttractionViewController.h"
#import "SiteMapViewController.h"
#import "youtube_channelViewController.h"
#import "DiningViewController.h"
#import "HistoricViewController.h"
#import "ResourceViewController.h"
#import "ShoppingViewController.h"
#import "InfoViewController.h"
#import "SponsorLoadViewController.h"
#import "TouchXML.h"

///Cool

@interface FirstViewController : UIViewController <InfoViewControllerDelegate, SponsorLoadViewControllerDelegate> {
	DataBaseSingleton *dataBaseSingleton;
    
    LodgingViewController *myLodgingViewController;
    AttractionViewController *myAttractionViewController;
    ResourceViewController *myResourceViewController;
    SiteMapViewController *mySiteMapViewController;
    DiningViewController *myDiningViewController;
    HistoricViewController *myHistoricViewController;
    ShoppingViewController *myShoppingViewController;
    InfoViewController* myInfoViewController;
    SponsorLoadViewController *mySponsorLoadViewController;
    IBOutlet UIButton* NewsLabel;
    UILabel* myNewsTicker;
    IBOutlet UIImageView* arrow;
    youtube_channelViewController *myYoutubeChannelViewController;
    HomePagerViewController* slideShowController;
    IBOutlet UIImageView *weatherBubble;
    IBOutlet UILabel *weatherLabel;
    IBOutlet UIButton *lodgingButton;
}

@property (nonatomic, retain) LodgingViewController *myLodgingViewController;
@property (nonatomic, retain) ShoppingViewController *myShoppingViewController;
@property (nonatomic, retain) HistoricViewController *myHistoricViewController;
@property (nonatomic, retain) AttractionViewController *myAttractionViewController;
@property (nonatomic, retain) DiningViewController *myDiningViewController;
@property (nonatomic, retain) ResourceViewController *myResourceViewController;
@property (nonatomic, retain) SiteMapViewController *mySiteMapViewController;
@property (nonatomic, retain) youtube_channelViewController *myYoutubeChannelViewController;
@property (nonatomic, retain) InfoViewController* myInfoViewController;
@property (nonatomic, retain) SponsorLoadViewController *mySponsorLoadViewController;
@property (nonatomic, retain) IBOutlet UIImageView* arrow;
@property (nonatomic, retain) IBOutlet UIButton* NewsLabel;
@property (nonatomic, retain) IBOutlet UILabel* myNewsTicker;
@property (nonatomic, retain) IBOutlet UIImageView *weatherBubble;
@property (nonatomic, retain) IBOutlet UILabel *weatherLabel;
@property (nonatomic, retain) IBOutlet UIButton *lodgingButton;

//

@property (nonatomic, retain) IBOutlet UIView* newsFeedView;


- (IBAction)bringLodgingView:(id)sender;

- (IBAction)bringAttractionsView:(id)sender;

- (IBAction)bringWhatsHappening:(id)sender;

- (IBAction)bringShoppingView:(id)sender;

- (IBAction)bringHistoricView:(id)sender;

- (IBAction)bringResourcesView:(id)sender;

- (IBAction)bringSiteMapView:(id)sender;

- (IBAction)bringMyPhotosView:(id)sender;

- (IBAction)bringSpotLightView:(id)sender;

- (IBAction)bringTwiterView:(id)sender;

- (IBAction)bringInfoView:(id)sender;

- (IBAction)callJITH4Tickets:(id)sender;

- (IBAction)bringDiningView:(id)sender;

- (IBAction)bringInFacebook:(id)sender;

- (void)bringLoadingView;

- (void)checkNetwork;

- (void)updateNewsTicker:(BOOL)firstTime;

- (void)getWeather;

-(void)flushCache;


@end

