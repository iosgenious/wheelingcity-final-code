//
//  WhatsHappeningViewController.m
//  JITH
//
//  Created by Ryan Wall on 4/11/11.
//  Copyright 2011 W. All rights reserved.
//

#import "WhatsHappeningViewController.h"
#import "Macro.h"
#import "NSString+Null.h"
#import "EventCell.h"
#import "WhatsHappeningInfoViewController.h"

@implementation WhatsHappeningViewController
@synthesize dataArray, dataDictionary, myWhatsHappeningInfoViewController,background;


//UIActivityIndicatorView *spinner;
MBProgressHUD *mbSpinner;
BOOL fetchingGoogleCalendar=true;
NSDate *firstDate;
NSDate *endDate;
NSMutableDictionary* currentEvent;

#pragma mark - Class Instance Method

-(void)addActivityView{
    mbSpinner=[[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:mbSpinner];
    mbSpinner.delegate=self;
    mbSpinner.labelText=@"Loading";
    [mbSpinner show:true];
    
}

-(void)hideActivity{
    [mbSpinner hide:true];
}
#pragma mark - View Life Cycle Method

- (void) viewDidLoad{
	[super viewDidLoad];
    self.navigationItem.title = @"What's Happening";
    [background setImage:[UIImage imageNamed:@"BackgroundColor.png"]];
    [self.view insertSubview:background atIndex:1];
    UIView * kalBgView=[[UIView alloc] initWithFrame:CGRectMake(0, 24, 320, 416)];
    [kalBgView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    kal = [[KalViewController alloc] init];
    
    CGRect  calFrame=[kalBgView frame];
    calFrame.origin.y=20;
    calFrame.size.height=416;
    [kal.view setFrame:calFrame];
    kal.delegate = self;
    kal.dataSource=self;
    [kal showAndSelectDate:[NSDate date]];

    [kalBgView addSubview:kal.view];
    [self.view addSubview:kalBgView];
    [kalBgView release];
    
    //Initlize Event Array
    
    eventList=[[NSMutableArray alloc] init];
   
    // Month Array
    
    daysOfEachMonth =[[NSArray alloc]initWithObjects:@"",@"31",@"28",@"31",@"30",@"31",@"30",@"31",@"31",@"30",@"31",@"30",@"31", nil];

}

- (void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self loadGoogleCalendarEvents];
}


#pragma mark -Class Instance Method

- (void) dealloc
{
    [firstDate release];
    [endDate release];
    [currentEvent release];
    [dataArray release];
    [dataDictionary release];
    [myWhatsHappeningInfoViewController release];
    //[spinner release];
    [super dealloc];
}



/**
 ActionSheet is the  method selector for the last method. here is selected to take a photo with the camera or to select one picture of the library
 
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
	} else if (buttonIndex == 1) {
	} 
    else if (buttonIndex ==2)
    {
    }
    else if (buttonIndex == 3)
    {
    }
    
}


#pragma mark - Google Calendar API 

- (GDataServiceGoogleCalendar *)calendarService {
    static GDataServiceGoogleCalendar* service = nil;
    if (!service) {
        service = [[GDataServiceGoogleCalendar alloc] init];
        [service setServiceShouldFollowNextLinks:YES];
    }
    [service setUserCredentialsWithUsername:GOOGLE_ACCOUNT_USERNAME password:GOOGLE_ACCOUNT_PASSWORD]; return service;
}
    
#pragma mark - Calendars Method
    
-(void)loadGoogleCalendarEvents{
    [self fetchAllCalendars];
}



#pragma mark -  Events

- (void)calendarEventsTicket:(GDataServiceTicket *)ticket finishedWithFeed:(GDataFeedCalendarEvent *)feed error:(NSError *)error {
    
    if( !error ){
        
        NSArray * tempEventList=[feed entries];
            [eventList removeAllObjects];
        [tempEventList enumerateObjectsUsingBlock:^(GDataEntryCalendarEvent *entry, NSUInteger idx, BOOL *stop) {
            
            WCEvent * event=[[WCEvent  alloc] init];
            [event setEventTitle:[NSString stringWithFormat:@"%@",[[entry title] stringValue]]];
            [event setEventDescription:[NSString stringWithFormat:@"%@",[[entry content] stringValue]]];
            
            //Get The Event Date Info
            
            GDataWhen *when = [[entry objectsForExtensionClass:[GDataWhen class]] objectAtIndex:0];
            
            if( when ){
                NSDate *date = [[when startTime] date];
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                
                NSDateFormatter *dateF = [[NSDateFormatter alloc] init];
                [dateF setDateFormat:@"HH:mm a"];

                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                [event setEventTime:[dateF stringFromDate:date]];
                [event setEventStartTime:date];
                
                NSDate *date11=[[when endTime] date];
                NSDate *date2 = [date11 dateByAddingTimeInterval:18000.0];
                [event setEventFinishTime:date2];
                [dateFormatter release];
            }
            
            //Get the Event Location
            //NSLog(@"locations %@",[entry locations]);
            GDataWhere *addr = [[entry locations] objectAtIndex:0];
            if(addr)
            {
                [event setEventLocation:[addr stringValue]];
            }
            
            [eventList addObject:event];
            [event release];
        }];
    }
    [self hideActivity];
    [kal loadedDataSource:self];

}


#pragma mark -
#pragma mark Calendars



- (void)fetchAllCalendars {
    
    GDataServiceGoogleCalendar *service = [self calendarService];
    GDataServiceTicket *ticket;
    
    ticket = [service fetchFeedWithURL:[NSURL URLWithString:kGDataGoogleCalendarDefaultOwnCalendarsFeed]
                              delegate:self
                     didFinishSelector:@selector(calendarListTicket:finishedWithFeed:error:)];
}

// fetch calendar metafeed callback
- (void)calendarListTicket:(GDataServiceTicket *)ticket finishedWithFeed:(GDataFeedCalendar *)feed error:(NSError *)error {
    NSArray *calendars = [feed entries];
    if(calendars.count !=0){
        
        GDataEntryCalendar *calendar = [[feed entries] objectAtIndex:0];
        [self fetchCalendarEvents:calendar];
    }
    else{
        NSLog(@"User has no calendars...");
        [self hideActivity];
    }
}

#pragma mark -
#pragma mark Events

- (void)fetchCalendarEvents:(GDataEntryCalendar*)calService {
    if (self.calendarService) {
        // fetch the events feed
        
        NSURL *feedURL = [[calService alternateLink] URL];
            if( feedURL ){
                GDataQueryCalendar* query = [GDataQueryCalendar calendarQueryWithFeedURL:feedURL];
                
                    // Currently, the app just shows calendar entries from 15 days ago to 31 days from now.
                    // Ideally, we would instead use similar controls found in Google Calendar web interface, or even iCal's UI.
                    NSString* maxDay;
                    NSString* currentMonth;
                    //in case is a leap year
                    if(W2Wmonth ==2)
                    {
                        if (((W2Wyear % 400)==0) || ((W2Wyear % 100)==0) || ((W2Wyear % 4)==0))
                            maxDay=@"29";
                        else
                            maxDay=@"28";
                    }
                    else{
                        maxDay=[daysOfEachMonth objectAtIndex:W2Wmonth];
                    }
                    if(W2Wmonth < 10)
                        currentMonth=[[NSString alloc]initWithFormat:@"0%d",W2Wmonth];
                    else
                        currentMonth=[[NSString alloc]initWithFormat:@"%d",W2Wmonth];
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss ZZ"];
                    NSString* nowasmountaintime = [dateFormatter stringFromDate:[NSDate date]];
                    NSArray* things=[nowasmountaintime componentsSeparatedByString:@" "];
                    NSString* GMT=[things objectAtIndex:2];
                    
                    // Beginning of the month
                    NSString * minDateString=[NSString stringWithFormat:@"%d-%@-01 00:00:00 %@",W2Wyear,currentMonth,GMT];
                    
                    NSDate *minDate = [dateFormatter dateFromString:minDateString];
                    minDateString=nil;
                NSDate *maxDate=nil;
                    // end of the month.
                
                    NSString * maxDateString=[NSString stringWithFormat:@"%d-%@-%@ 23:59:59 %@",W2Wyear,currentMonth,maxDay,GMT];
                    
                    maxDate = [dateFormatter dateFromString:maxDateString];
                    maxDateString=nil;

//                    NSLog(@"Minimum Date:%@",minDate);
//                    NSLog(@"MAximum Date:%@",maxDate);
                    [query setMinimumStartTime:[GDataDateTime dateTimeWithDate:minDate timeZone:[NSTimeZone systemTimeZone]]];
                    [query setMaximumStartTime:[GDataDateTime dateTimeWithDate:maxDate timeZone:[NSTimeZone systemTimeZone]]];
                    
                [query setOrderBy:@"starttime"];
                [query setIsAscendingOrder:YES];
                [query setShouldExpandRecurrentEvents:YES];
                [query setMaxResults:1000];
                GDataServiceGoogleCalendar *service = [self calendarService];

                [service fetchFeedWithQuery:query delegate:self didFinishSelector:@selector(calendarEventsTicket:finishedWithFeed:error:)];
            }
    }
}



#pragma mark - UITableView Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
	
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
		return [eventList count];
}

- (UITableViewCell *)tableView:(UITableView *)tv cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *eventCellID=@"eventCellID";
    EventCell* cell = (EventCell*)[tv dequeueReusableCellWithIdentifier:eventCellID];
    
    static NSString *cellNib = @"EventCell" ;
    NSArray *topLevelObjectsProducts = [[NSBundle mainBundle] loadNibNamed:cellNib owner:self options:nil];
    if (!cell) {
        cell = (EventCell *)[topLevelObjectsProducts objectAtIndex:0];

    }

    if ([eventList count]>indexPath.row) {
        WCEvent *event=[eventList objectAtIndex:[indexPath row]];
        [cell.lblTime setText:[event eventTime]];
        [cell.lblName setText:[event eventTitle]];
        [cell.lblLocation setText:[event eventLocation]];
        if (![event.eventLocation length]) {
            [cell.lblName setFrame:CGRectMake(99, 15, 177, 15)];
        }
    }
   
    return cell;
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WhatsHappeningInfoViewController * eventDetailVC=[[WhatsHappeningInfoViewController alloc] initWithNibName:@"WhatsHappeningInfoViewController" bundle:nil];
    [eventDetailVC setEventInfo:[eventList objectAtIndex:[indexPath row]]];
    [self.navigationController pushViewController:eventDetailVC animated:YES];
    [eventDetailVC release];
    eventDetailVC=nil;
   
}

#pragma mark - KalViewDelegate
 

- (void)showPreviousMonth{
    
}
- (void)showFollowingMonth{
    
}
- (void)didSelectDate:(KalDate *)date{
    
}



#pragma mark - KalViewDatasource

- (void)presentingDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate delegate:(id<KalDataSourceCallbacks>)delegate{
    
    [self addActivityView];
    firstDate=[fromDate copy];
    endDate=[toDate copy];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:firstDate];
   
    NSDateFormatter *dtF=[[NSDateFormatter alloc] init];
    [dtF setDateFormat:@"MM"];
    NSInteger month = [components month]; //gives months from 0 to 11
    NSInteger day = [components day];
    NSInteger year = [components year];
    if (day >= 25 && month==12)//this means the first day is from the month before and of december
    {
        month=1;//goes back to jannuary
        year++;//goes to the next year
    }
    else//previous month
        month=[[dtF stringFromDate:toDate] integerValue];
        W2Wmonth=month;
        W2Wyear=year;
    [self loadGoogleCalendarEvents];
}
- (NSArray *)markedDatesFrom:(NSDate *)fromDate to:(NSDate *)toDate{
    
    return nil;
}
- (void)loadItemsFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate{
    
}
- (void)removeAllItems{
    
}

- (void)selectedDate:(KalDate *)selectedDate{
    
}
#pragma mark - MBProgressHUD Delegate Method

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [mbSpinner removeFromSuperview];
    [mbSpinner release];
	mbSpinner = nil;
    
}

@end
