



#import "SiteMapViewController.h"
#import "MyAnnotation.h"
#import "Dining.h"
#import "Map.h"


@implementation SiteMapViewController
@synthesize diningDict,actionButton;

/**
 Load de data of the map and the categories to select then in a pickerview. Create the array with the different sections of the pickerview. In this moment the application see all the coordenates for the differente point of interest. It saw what is the max and de min lat and long to do the best zoom.
 */
- (void)viewDidLoad {
    
    dataBaseSingleton = [[DataBaseSingleton alloc] init];
    //diningDict = [[NSMutableDictionary alloc ] initWithDictionary:dataBaseSingleton.coorArray];
    
    diningDict=[[NSMutableArray alloc]init];
    
    
    pickerView.hidden=YES;
    arrayNo = [[NSMutableArray alloc] init];
    //[arrayNo addObject:@"All"];
    [arrayNo addObject:@"Attractions"];
    [arrayNo addObject:@"Dining"];
    [arrayNo addObject:@"Historical"];
    [arrayNo addObject:@"Lodging"];
    [arrayNo addObject:@"Resources"];
    [arrayNo addObject:@"Shopping"];
    
    [pickerView selectRow:0 inComponent:0 animated:YES];    
    self.title=@"Wheeling Map";
    
    
	breweries = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] 
														 pathForResource:@"Locations" 
														 ofType:@"plist"]];
    
	
	double minLat = [[breweries valueForKeyPath:@"@min.latitude"] doubleValue];
	double maxLat = [[breweries valueForKeyPath:@"@max.latitude"] doubleValue];
	double minLon = [[breweries valueForKeyPath:@"@min.longitude"] doubleValue];
	double maxLon = [[breweries valueForKeyPath:@"@max.longitude"] doubleValue];
	
	MKCoordinateRegion region;
	region.center.latitude = (maxLat + minLat) / 2.0;
	region.center.longitude = (maxLon + minLon) / 2.0;
	region.span.latitudeDelta = (maxLat - minLat) * 1.05;
	region.span.longitudeDelta = (maxLon - minLon) * 1.05;
	map.region = region;
    map.showsUserLocation=YES;
    map.scrollEnabled=YES;
    
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:FALSE animated:NO];

}

///////////////////////////////////////////////////////////////////////////////////////////
//MKMAPVIEWDELEGATE
/////////////////////////////////////////////////////////////////////////////////////////


/**
 MapView delegate. This function put the pins of the selected category in the map. This function is call when someone click in the pin and show the information about the pin. This informacion contain the name of the shop/restaurante.. and the address. This method return the information about the pin like the location and name.
 */

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
	
	if (map.userLocation == annotation){
		return nil;
	}
	
	NSString *identifier = @"MY_IDENTIFIER";
	
    annotationView = [map dequeueReusableAnnotationViewWithIdentifier:identifier];
	if (annotationView == nil){
		annotationView = [[[MKAnnotationView alloc] initWithAnnotation:annotation 
													   reuseIdentifier:identifier] 
						  autorelease];
        MKPinAnnotationView *pinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil ]autorelease];        
        
        
        pinView.pinColor=MKPinAnnotationColorGreen;
        
        
        pinView.canShowCallout=YES;
        annotationView=pinView;
		annotationView.canShowCallout = YES;
		
		annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
		
	}
	return annotationView;
}

/**
 This method is used to go fron your current location to the point that you select before. The system give an alert to say that the application is going to close and it is going to open google maps.
 */

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
        
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Go to that location" message:@"This will open the Maps App" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
    [alert release];
    NSString *googleWeb=nil;
    if([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
    {
        googleWeb = [NSString stringWithFormat:@"http://maps.google.com/maps?saddr="];
    }
    else
    {
        googleWeb = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr="];
    }
    NSString *coma=@",";
    NSString *first=@"&daddr=";
    NSString *final=@"&output=dragdir";
    NSString *actual=[NSString stringWithFormat:@"%f%@%f",map.userLocation.location.coordinate.latitude,coma,map.userLocation.location.coordinate.longitude];
    NSString *destination=[NSString stringWithFormat:@"%f%@%f",view.annotation.coordinate.latitude,coma,view.annotation.coordinate.longitude];
    urlString= [NSString stringWithFormat:@"%@%@%@%@%@",googleWeb,actual,first,destination,final];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [urlString retain];
}

/**
 This is the delegate of the alert, it is used if you select ok the system go to google maps, and if you select cancel the systen didn´t do anything
 */
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
}




- (void)dealloc {
	[breweries release];
	[map release];
    [urlString release];
    [categories release];
    [pickerView release];
    [arrayNo release];
    [locationCurrent release];
    [annotationView release];
    [diningDict release];
    //[super dealloc];
}
////////////////////////////////////////////////////////////////////
//UI PICKER VIEW DELEGATE
///////////////////////////////////////////////////////////////////

/**
 this method is to be sure that we can only select one category in the pickerview
 */
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;

{
    
    return 1;
    
}

/**
 This method see what type of category we select in the pickerview, and depending to that the method is able to call another method to put all the pins in the map.
 */
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component


{
    self.title= [arrayNo objectAtIndex:row];
    NSString *selectedCategory=[arrayNo objectAtIndex:row];
    pickerView.hidden=YES;
    categories.text=selectedCategory;
//    int lon=[diningDict count];
    [map removeAnnotations:map.annotations];
    [dataBaseSingleton setDatabaseVars];
    [diningDict removeAllObjects];
    
    
    if ([selectedCategory isEqualToString:@"Attractions"])
    {
        NSLog(@"Attractions");
        [dataBaseSingleton readCoordenatesAttractions];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorAttractionsArray];
        
        
        
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;
            annotation.description=theMap.description;
            
            
            if ([annotation.category isEqualToString:@"Attractions"] && (annotation.coordinate.longitude!=0.000000))
            {

                [map addAnnotation:annotation];
                
                
            }
            [annotation release];
        }
        
        
        
        
    }
    else if  ([selectedCategory isEqualToString:@"Shopping"])
    {
        NSLog(@"Shopping");
        [dataBaseSingleton readCoordenatesShopping];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorShoppingArray];
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;
            annotation.description=theMap.description;

            
            if ([annotation.category isEqualToString:@"Shopping"] && (annotation.coordinate.longitude!=0.000000))
            {
                [map addAnnotation:annotation];
                
            }
            [annotation release];
        }
        
    }
    
    else if ( [selectedCategory isEqualToString:@"Resources"])
    {
        NSLog(@"Resources");
        [dataBaseSingleton readCoordenatesResources];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorResourcesArray];
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;
            annotation.description=theMap.description;

            
            if ([annotation.category isEqualToString:@"Resources"] && (annotation.coordinate.longitude!=0.000000))
            {
                
                [map addAnnotation:annotation];
                
            }
            [annotation release];
        }
    }
    
    
    
    
    else if ([selectedCategory isEqualToString:@"Historical"])
    {
        [dataBaseSingleton readCoordenatesHistorical];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorHistoricalArray];
        
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;
            annotation.description=theMap.description;

            
            if ([annotation.category isEqualToString:@"Historical"] && (annotation.coordinate.longitude!=0.000000))
            {

                
                [map addAnnotation:annotation];
                
            }
            [annotation release];
        }
        
    }
    else if ([selectedCategory isEqualToString:@"Lodging"])
    {
        NSLog(@"Lodging");
        
        [dataBaseSingleton readCoordenatesLodging];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorLodgingArray];
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;
            annotation.description=theMap.description;

            
            if ([annotation.category isEqualToString:@"Lodging"] && (annotation.coordinate.longitude!=0.000000))
            {

                
                [map addAnnotation:annotation];
                
                
            }
            [annotation release];
        }
        
    }
    else if  ([selectedCategory isEqualToString:@"Shopping"])
    {
        NSLog(@"Shopping");
        [dataBaseSingleton readCoordenatesShopping];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorShoppingArray];
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;
            annotation.description=theMap.description;

            if ([annotation.category isEqualToString:@"Shopping"] && (annotation.coordinate.longitude!=0.000000))
            {
                [map addAnnotation:annotation];
            }
            [annotation release];
        }
    }
    else if  ([selectedCategory isEqualToString:@"Dining"])
    {
        [dataBaseSingleton readCoordenatesDinning];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorArray];
        
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;
            annotation.description=theMap.description;

            
            if ([annotation.category isEqualToString:@"Dining"] && (annotation.coordinate.longitude!=0.000000))
            {
                [map addAnnotation:annotation];
                
                
            }
            [annotation release];
        }
    }

    else
    {
        [dataBaseSingleton readCoordenatesAttractions];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorAttractionsArray];
        [dataBaseSingleton readCoordenatesDinning];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorArray];
        [dataBaseSingleton readCoordenatesHistorical];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorHistoricalArray];
        [dataBaseSingleton readCoordenatesLodging];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorLodgingArray];
        [dataBaseSingleton readCoordenatesResources];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorResourcesArray];
        [dataBaseSingleton readCoordenatesShopping];
        [diningDict addObjectsFromArray:dataBaseSingleton.coorShoppingArray];
        
        int len=[diningDict count];
        for(int i=0;i<len;i++){
            theMap=  [diningDict objectAtIndex:i];
            MyAnnotation *annotation = [[MyAnnotation alloc] init];
            annotation.title=theMap.name;
            annotation.subtitle=theMap.address;
            CLLocationDegrees coorLat = [theMap.latitude doubleValue];
            CLLocationDegrees coorLong = [theMap.longitude doubleValue];
            CLLocationCoordinate2D newCoor={coorLat , coorLong};
            annotation.coordinate=newCoor;
            annotation.category=theMap.category;  
            annotation.description=theMap.description;

            [map addAnnotation:annotation];
            [annotation release];
        }
    }
}

/**
 this method the number of different categories that are in the uipickerview
 */
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [arrayNo count];
}

/**
 This method return the category selected on the pickerview, it depends of the row selected in the array
 */

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;

{
    return [arrayNo objectAtIndex:row];
}

/**
 This method is to open the pickerview when we click in the textfield
 */

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    pickerView.hidden=NO;
}

/**
 When we select a category in tne pickerview the pickerview is closed
 */
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    pickerView.hidden=YES;
}
/**
 When we select a category in tne pickerview the textfield appear
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(IBAction) hireTheKeyboard:(UITextField *)textField
{
    textField.text=nil;
    [textField resignFirstResponder];
}

/**
 This method is used to appear the bar that we can go back
 */
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:FALSE animated:YES];
}
/**
 When the view disapear the picker dissapear
 */
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    pickerView.hidden=YES;
}
/**
 With this method we know the current location of the person
 */
-(IBAction) currentP:(id)sender{
    
    CLLocationCoordinate2D coor;
    
    MyAnnotation *annotation = [[MyAnnotation alloc] init];
    
    coor.latitude=map.userLocation.location.coordinate.latitude;
    
    coor.longitude=map.userLocation.location.coordinate.longitude;
    annotation.coordinate=coor;
    annotation.title=@"Current location";
    annotation.subtitle=nil;
    [map addAnnotation:annotation];
    [annotation release];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Will it create a new empty infoview controller? Apparently it does
}


@end
