//
//  AttractionViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "AttractionInfoViewController.h"
#import "PagerViewController.h"

@interface AttractionViewController : UIViewController 
<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate>  
{
	DataBaseSingleton *dataBaseSingleton;
    AttractionInfoViewController *myAttractionInfoViewController;
    UITableView *attractionTableView;
	NSDictionary *attractionsDict;
	int previousAttractionId;
	int currentAttractionId;
    NSMutableDictionary *images;
    PagerViewController* slideShowController;
    UIBarButtonItem *categoryButton;
    NSMutableArray *categoryArray;
}

@property (nonatomic, retain) AttractionInfoViewController *myAttractionInfoViewController;
@property (nonatomic, retain) NSDictionary *attractionsDict;
@property (nonatomic, retain) IBOutlet UITableView *attractionTableView;
@property (nonatomic, retain) NSDictionary *images;
@property (nonatomic, retain) NSMutableArray *categoryArray;

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height;

@end
