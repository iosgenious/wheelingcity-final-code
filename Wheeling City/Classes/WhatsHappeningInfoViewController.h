//
//  WhatsHappeningInfoViewController.h
//  JITH
//
//  Created by Ryan Wall on 4/18/11.
//  Copyright 2011 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "LodgingWebsiteViewController.h"
#import "NewsFeedViewController.h"
#import "WCEvent.h"

@interface WhatsHappeningInfoViewController : UIViewController<UIActionSheetDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate> {
    IBOutlet UILabel* text;
    IBOutlet UIButton* shareButton;
    IBOutlet UIScrollView* contentScroll;
    NSDictionary* currentEvent;
    NSInteger currentDay;
    LodgingWebsiteViewController* cvbWebViewController;
    NewsFeedViewController* postInFacebook;
    
}
@property (nonatomic,retain) IBOutlet UILabel* text;
@property (nonatomic,retain) NSDictionary* currentEvent;
@property (nonatomic) NSInteger currentDay;
@property (nonatomic,retain) IBOutlet UIButton* shareButton;
@property (nonatomic,retain) LodgingWebsiteViewController* cvbWebViewController;
@property (nonatomic,retain) NewsFeedViewController* postInFacebook;
@property (nonatomic,retain) IBOutlet UIScrollView* contentScroll;
@property(nonatomic,retain) WCEvent *eventInfo;
-(IBAction) shareButtonPushed:(id)sender;

@end
