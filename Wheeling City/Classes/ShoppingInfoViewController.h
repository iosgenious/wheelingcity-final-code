//
//  ShoppingInfoViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "PagerViewController.h"

@interface ShoppingInfoViewController : UIViewController {

	DataBaseSingleton *dataBaseSingleton;
    UILabel *descriptionLabel;
    UIImageView *descriptionBubble;
    UIImageView *shoppingImage;
    UIScrollView *contentScroll;
	BOOL sameShopping;

}
@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UIImageView *shoppingImage;
@property (nonatomic, retain) IBOutlet UIImageView *descriptionBubble;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScroll;
@property (nonatomic, assign) BOOL sameShopping;


@end
