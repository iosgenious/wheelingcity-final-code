//
//  ResourceWebsiteViewController.h
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ResourceWebsiteViewController : UIViewController <UIWebViewDelegate>{
    UIWebView *resourceWeb;
}
@property (nonatomic, retain) IBOutlet UIWebView *resourceWeb;
- (void) blankPage;

@end
