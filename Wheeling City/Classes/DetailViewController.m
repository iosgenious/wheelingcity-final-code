//
//  DetailViewController.m
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 10/03/11.
//  Copyright 2011 W. All rights reserved.
//

#import "DetailViewController.h"
#import "Macro.h"

@implementation DetailViewController
@synthesize names, comments,commentData,ids, scrollView;


#pragma mark -Class Instance Method

-(void)drawImageBorder{
    [[facebookPhoto layer] setShadowColor:SHADOW_COLOR];
    [[facebookPhoto layer] setShadowOffset:SHADOW_OFFSET];
    [[facebookPhoto layer] setShadowOpacity:OPTICITY];
    [[facebookPhoto layer] setShadowRadius:RADIUS];
    [[facebookPhoto layer] setBorderWidth:BORDER_WIDTH];
    [[facebookPhoto layer]setBorderColor:BORDER_COLOR];
}


-(void)populateDetailData{
    NSString *strImageUrl=[NSString stringWithFormat:@"https://graph.facebook.com/%@%@",ids,@"/picture"];
    strImageUrl = [strImageUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    UIImage *myImage=[[UIImage alloc]initWithContentsOfFile:@"icon-loading-animated.gif"];
    myImage = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strImageUrl]]];
    facebookPhoto.image=myImage;
    
    //publish data in the view
    NSString *time=[self dateDiff:commentData];
    NSString *commentString = [NSString stringWithFormat:@"%@\r\r%@",comments,time];
    self.navigationItem.title = @"Comments";
    name.text=names;
    
    comment.text=commentString;
    
    //it is to change the font of the letter that you van view
    comment.textColor=[UIColor yellowColor];
    CGRect frame = comment.frame;
    frame.size.height = comment.contentSize.height;
    comment.frame = frame;
    scrollView.contentSize = CGSizeMake (319,comment.frame.size.height + 10);
    
    [myImage release];

}
#pragma mark - View lifecycle

/**
 This method is very important, first it does a nsurqlrequest to get the image of the person who has written the message. then the message and when was it written appears in the screen. Then we selected the size, the type of leter of the letter that appears there.
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self drawImageBorder];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    dispatch_async(dispatch_get_current_queue(), ^{
        [self populateDetailData];
    });
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    [comment release];
    [name release];
    [facebookPhoto release];
    
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


/**
 This is a method to convert the time that gives facebook that was the message written to put for example 4 hours ago, 3 minutes ago and something like that. It´s a method to convert the data
 */

-(NSString*)dateDiff:(NSDate *)origDate {
	NSString *interval;
    NSDate *todayDate = [NSDate date];
    double ti = [origDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    if (ti >  31536000) {
        int diff = round(ti / 60 / 60 / 24 / 365);
        interval = [NSString stringWithFormat:@" more than %d year ago", diff];
	}
    else if (ti >  86400) {
        int diff = round(ti / 60 / 60 / 24);
        interval = [NSString stringWithFormat:@"%d days ago", diff];
	}
	else if (ti > 3600) {
		int diff = round(ti / 60 / 60);
		interval = [NSString stringWithFormat:@"%d hours ago", diff];
	}
	else if (ti > 60) {
        int diff = round(ti / 60);
        interval = [NSString stringWithFormat:@"%d minutes ago", diff];
	}
	else if (ti <= 60) {
        interval = @"less than a minute ago";
	}
    else
		interval = @"never";
    return interval;
}


@end