/**
 * @file LPAugmentedRealityViewController.h
 *
 * @author Lawrence Lee, Layar B.V.
 * @date 25th October 2010
 */

//Local imports
#import "LPBIW.h"
#import "LPExtendedBIW.h"
#import "LPShowDialog.h"
#import "LPViewControllerBase.h"

#define kBirdsEyeViewCheckedNotification	@"BirdsEyeViewCheckedNotification"

@class AVCaptureSession;
@class AVCaptureDevice;
@class AVCaptureDeviceInput;
@class AVCaptureVideoPreviewLayer;

@class LPAugmentedRealityView;
@class LPAugmentedRealityOverlayView;

/**
 * @class LPAugmentedRealityViewController
 * @brief UIViewController subclass that manages the Augmented Reality View
 *
 * The most important functionality of this class is to facilitate the loading
 * of a layer. This can be done using the loadLayerWithName method. When an
 * instance of this class is presented, it will request that the location
 * services and accelerometer services be enabled. It will also initialize
 * the camera and various overlay views for the Augmented Reality view, and
 * create the underlying OpenGL contexts for rendering.
 * The class also manages the various resources needed to render the scene,
 * and if necessary will also download any resources the POIs in the scene
 * require.
 */
@interface LPAugmentedRealityViewController : LPViewControllerBase <LPBIWDelegate, LPExtendedBIWDelegate, LPShowDialogDelegate>
{
	AVCaptureSession *captureSession;
	AVCaptureDevice *videoCaptureDevice;
	AVCaptureDeviceInput *videoInput;
	AVCaptureVideoPreviewLayer *previewLayer;
	LPAugmentedRealityView *arView;
	LPAugmentedRealityOverlayView *overlayView;
//	BOOL headingAvailable;
	
	LPShowDialog *showDialog;
}

#if defined(LPINTERNALBUILD)

@property (nonatomic, readonly) LPAugmentedRealityOverlayView *overlayView;

- (void)deviceOrientationChanged:(NSNotification*)notification;

#endif

- (BOOL)isBirdsEyeToggled;

/**
 * @brief Use this function to load a layer
 *
 * The layer @p layerName will be loaded and the POIs will be downloaded for the layer.
 * The @p oauthParameters dictionary must contain the consumer key and consumer secret required
 * by the layer. These must be added to @p oauthParameters using the LPConsumerKeyParameterKey
 * and LPConsumerSecretParameterKey respectively.
 * Any filter options for the layer that differ from the default values can be passed using the @p layerFilters dictionary.
 * The @p options parameter can be used to pass any options to the program.
 *
 * @param layerName The name of the layer to load
 * @param oauthParameters The oauth consumer key and consumer secret required by the layer.
 * These must be added using the LPConsumerKeyParameterKey and LPConsumerSecretParameterKey respectively.
 * @param layerFilters The layer filters to pass with the request. If nil or an empty dictionary the default values for all filters for the layer will be used.
 * @param options Options to pass to the program
 */
-(void)loadLayerWithName:(NSString*)layerName
		 oauthParameters:(NSDictionary*)oauthParameters
			layerFilters:(NSDictionary*)layerFilters
				 options:(LPOptions)options;

@end
