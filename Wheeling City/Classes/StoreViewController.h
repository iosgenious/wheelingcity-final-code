//
//  StoreViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DataBaseSingleton.h"
#import "ShoppingInfoViewController.h"

@interface StoreViewController : UIViewController 
<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate,CLLocationManagerDelegate>  
{
	DataBaseSingleton *dataBaseSingleton;
    ShoppingInfoViewController *myShoppingInfoViewController;
    UITableView *shoppingTableView;
	NSArray *shoppingArray;
	int previousShoppingId;
	int currentShoppingId;
    NSMutableDictionary *images;
    PagerViewController* slideShowController;
    NSString *areaImage;
    NSString *storeCall;
    UIBarButtonItem *flipButton;
    NSString *descriptionArea;
    NSString *storeLongitude;
    NSString *storeLatitude;
    UIImage *slideImage;
}

@property (nonatomic, retain) ShoppingInfoViewController *myShoppingInfoViewController;
@property (nonatomic, retain) NSArray *shoppingArray;
@property (nonatomic, retain) IBOutlet UITableView *shoppingTableView;
@property (nonatomic, retain) NSString *areaImage;
@property (nonatomic, retain) NSString *storeCall;
@property (nonatomic, retain) NSString *descriptionArea;
@property (nonatomic, retain) NSString *storeLongitude;
@property (nonatomic, retain) NSString *storeLatitude;
@property (nonatomic, retain) NSMutableDictionary *images;
@property (nonatomic, retain) UIImage *slideImage;

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height;
//- (void)callStore;
- (void)flipDesc:(id)sender;
- (void)infoArea:(id)sender;
- (void)bringGoogleMaps;

@end
