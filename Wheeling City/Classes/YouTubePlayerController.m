//
//  YouTubePlayerController.m
//  JITH
//
//  Created by Santosh Singh on 07/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import "YouTubePlayerController.h"
#import "Macro.h"

@interface YouTubePlayerController ()

@end

@implementation YouTubePlayerController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - UIWebViewDelegate Method
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [lodingActivity startAnimating];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [lodingActivity stopAnimating];
    [self performSelector:@selector(hideActivity) withObject:nil afterDelay:2.0];
}

- (void)embedYouTube:(NSString*)youtubeurl WebView:(UIWebView *)webView {
    
    CGRect frame = webView.frame;
    NSString *embedHTML = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    background-color: transparent;\
    color: white;\
    }\
    </style>\
    </head><body style=\"margin:0\">\
    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
    width=\"%0.0f\" height=\"%0.0f\"></embed>\
    </body></html>";
    
    NSString* html = [NSString stringWithFormat:embedHTML, youtubeurl, frame.size.width, frame.size.height];
    webView.allowsInlineMediaPlayback = YES;
    [webView loadHTMLString:html baseURL:nil];
}


#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:self.toptitle];
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:107.0/255 green:107.0/255 blue:107.0/255 alpha:1.0]];
    UIWebView *web=[[UIWebView alloc] initWithFrame:CGRectMake(0, 44, 320, IS_IPHONE5?400:500)];
    [web setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:web];
    [self embedYouTube:self.weburl WebView:web];
    [web release];
    [self.navigationController setNavigationBarHidden:FALSE animated:NO];

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.toptitle=nil;
    self.weburl=nil;
    self.videoWebView=nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}

-(void)dealloc{
    [super dealloc];
}

@end
