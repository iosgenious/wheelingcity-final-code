//
//  Map.m
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 04/05/11.
//  Copyright 2011 W. All rights reserved.
//

#import "Map.h"


@implementation Map

@synthesize name, address,latitude, longitude,category,description; 

- (id) initWithName:(NSString *)in_Name address:(NSString *)in_Address latitude:(NSString *)in_Latitude longitude:(NSString *)in_Longitude category:(NSString *)in_Category description:(NSString *)in_Description {
	
	self = [super init];
	if(self)
	{
		self.name = in_Name;
		self.address = in_Address;
        self.latitude=in_Latitude;
        self.longitude=in_Longitude;
        self.category=in_Category;
        self.description=in_Description;
        
	}
	return self;
}


- (void) dealloc {
	
	[name release];
	[address release];
    [longitude release];
    [latitude release];
    [category release];
    [description release];
	[super dealloc];
}

@end
