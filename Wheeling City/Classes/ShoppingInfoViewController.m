//
//  ShoppingInfoViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import "ShoppingInfoViewController.h"


@implementation ShoppingInfoViewController

@synthesize descriptionLabel, descriptionBubble, shoppingImage, contentScroll, sameShopping;



- (void)viewDidLoad {
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	contentScroll.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	descriptionLabel.userInteractionEnabled = TRUE;
}
	 

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self release];
}
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
	[descriptionLabel release];self.descriptionLabel=nil;
	[shoppingImage release];self.shoppingImage=nil;
	[contentScroll release];self.contentScroll=nil;
	[super dealloc];
}

# pragma Class Instance Method



@end

