//
//  YouTubeModel.m
//  JITH
//
//  Created by Santosh Singh on 07/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import "YouTubeModel.h"

@implementation YouTubeModel
@synthesize title;
@synthesize imageUrl;
@synthesize likes;
@synthesize views;
@synthesize duration;
@synthesize description;
@synthesize publisheddate;
@synthesize videoUrl;

-(NSString*)equixdateFormatForRFC:(NSString*)date{
    NSString *_dateString = date;
    NSDateFormatter *equixdateFormat = [[NSDateFormatter alloc] init];
    [equixdateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSSZ"];
    NSDate *tempdate = [equixdateFormat dateFromString:_dateString];
    [equixdateFormat setDateFormat:@"MMM dd,yyyy : hh:mm "];
    _dateString = [equixdateFormat stringFromDate:tempdate];
    return _dateString;
}

-(NSString*)getFormattedTime:(NSInteger)seconds{
    NSUInteger m = (seconds / 60) % 60;
    NSUInteger s = seconds % 60;
    NSString *formattedTime = [NSString stringWithFormat:@"%02u:%02u",m, s];
    return formattedTime;
}
-(NSString*)convetNumToDecimalSystem:(NSInteger)number{
    NSNumberFormatter *num = [[NSNumberFormatter alloc] init];
    [num setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *numberAsString = [num stringFromNumber:[NSNumber numberWithInt:number]];
    [num release];
    return numberAsString;
    [num release];
}
-(NSString*)getlikes:(float)number{
    NSInteger f =number*20;
    NSString *likesStr = [NSString stringWithFormat:@"%d%@",f,@"%"];
    return likesStr;
}

+(YouTubeModel*)youTubeTyPeWithParameter :(NSMutableDictionary *)dictValue {
    
    YouTubeModel *values = [[[self alloc] init] autorelease];
    
    values.title = [[dictValue valueForKey:@"title"] valueForKey:@"$t"];
    values.imageUrl = [[[[dictValue valueForKey:@"media$group"] valueForKey:@"media$thumbnail"] valueForKey:@"url"] objectAtIndex:0];
    values.videoUrl =[[[[dictValue valueForKey:@"media$group"] valueForKey:@"media$content"] valueForKey:@"url"] objectAtIndex:0];
//    values.views = [values convetNumToDecimalSystem:[[[dictValue valueForKey:@"yt$statistics"]valueForKey:@"viewCount"]intValue]];
//    values.duration =[values getFormattedTime:[[[[dictValue valueForKey:@"media$group"]valueForKey:@"yt$duration"] valueForKey:@"seconds"]intValue]];
//    values.description = [[[dictValue valueForKey:@"media$group"] valueForKey:@"media$description"] valueForKey:@"$t"];
//    values.likes = [values getlikes:[[[dictValue valueForKey:@"gd$rating"] valueForKey:@"average"]floatValue]];
//    values.publisheddate=[[dictValue valueForKey:@"published"] valueForKey:@"$t"];
    //NSLog(@"values.publisheddate :%@",values.publisheddate);
    return values;
}
-(void)dealloc{
    
    [title release];
    [imageUrl release];
    [likes release];
    [views release];
    [duration release];
    [description release];
    [publisheddate release];
    [videoUrl release];
    [super dealloc];
    
}
@end
