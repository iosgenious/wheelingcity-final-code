//
//  ResourceViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Modified by Denis Santxez 
//  Copyright 2010 W. All rights reserved.
//

#import "ResourceViewController.h"
#import "Resource.h"
#import "JITHAppDelegate.h"
#import "FirstViewController.h"
#import "Macro.h"

@interface ResCell : UITableViewCell
@end

@implementation ResCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(5,6, 65, 65)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
}

@end

@implementation ResourceViewController
@synthesize myResourceInfoViewController, resourcesDict, resourceTableView, images, categoryButton, categoryArray;

- (void)viewDidLoad {
	
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	resourcesDict = [[NSDictionary alloc ] initWithDictionary:dataBaseSingleton.resourcesDict];
    categoryArray = [[NSMutableArray alloc]initWithArray:dataBaseSingleton.resourceCategory];
	resourceTableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	self.title = @"Resources";
    self.images = [[NSMutableDictionary alloc] init];
    //SlideShow Starter
    slideShowController = [[PagerViewController alloc] init];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    slideShowController.view.autoresizesSubviews = YES;
    [slideShowController setImages:[NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"WheelingClockSmall.png"],
                                    nil]];
    
    //[UIImage imageNamed:@"WJU_LOGO_2C_Stacked.jpg"],

    CGRect slideShowFrame=CGRectMake(0, 0, 320, 189);
    slideShowController.view.frame=slideShowFrame;
    [resourceTableView setTableHeaderView:slideShowController.view];
    categoryButton = [[UIBarButtonItem alloc] initWithTitle:@"Categories" style:UIBarButtonItemStylePlain target:self action:@selector(showCategories:)];
    [[self navigationItem] setRightBarButtonItem:categoryButton];
    [resourceTableView.tableHeaderView setFrame:CGRectMake(0, 0, 320, 120)];
 }

- (void)showCategories:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Jump to category" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    int idx = 0;
    for (NSString *category in categoryArray) {
        [actionSheet addButtonWithTitle:category];
        idx++;
    }
    [actionSheet addButtonWithTitle:@"Cancel"];

    actionSheet.cancelButtonIndex = idx++;    
    [actionSheet showInView:self.view];
    [actionSheet release];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.cancelButtonIndex!=buttonIndex)
    {
        [resourceTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:(buttonIndex)] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        CGPoint point = resourceTableView.contentOffset;
        point .y -= 44.0;
        [resourceTableView setContentOffset:point];
    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	[self.navigationController setNavigationBarHidden:FALSE animated:NO];
    myResourceInfoViewController = [[ResourceInfoViewController alloc] init];
}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();	
    return resizedImage;
}

- (void)dealloc {
	[myResourceInfoViewController release];
	[resourceTableView release];
    [slideShowController release];
	[resourcesDict release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table view data source methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [resourcesDict count];
}

- (NSString *)getNameFromSection:(NSInteger)section {
    
    return [categoryArray objectAtIndex:section];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self getNameFromSection:section];
}


- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
    return [[resourcesDict objectForKey:[self getNameFromSection:section]] count];    
}


- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"Cell";
	
	ResCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (ResCell*)[[[ResCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15];// systemFontOfSize:16];
        cell.textLabel.textColor = [UIColor whiteColor];
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
        cell.textLabel.numberOfLines=0;

	}
	
	Resource *the_pResource = [[resourcesDict objectForKey:[self getNameFromSection:indexPath.section]] objectAtIndex:indexPath.row];
    cell.textLabel.text = the_pResource.organization;

	
    [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *image = [self.images objectForKey:the_pResource.image];
        
        if(!image)
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString *documentsFolder = [paths objectAtIndex:0];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:the_pResource.image]]) {
                UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pResource.image]];     
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pResource.image];            
            }
            else {
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[@"http://wheeling.yourmobicity.com/English/Resources/" stringByAppendingString:the_pResource.image]]];
                UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pResource.image];
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                
                [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:the_pResource.image] atomically:YES];
                [unresizedImage release];
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[cell imageView] setImage:image];
            [cell setNeedsLayout];
        });
    });
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Navigation logic may go here ‚Äî for example, create and push another view controller.

	Resource *the_pResource = [[resourcesDict objectForKey:[self getNameFromSection:indexPath.section]] objectAtIndex:indexPath.row];

    [[self navigationController] pushViewController:myResourceInfoViewController animated:YES];
	
	//Setting title of the artist view
	myResourceInfoViewController.title = the_pResource.organization;
	myResourceInfoViewController.descriptionLabel.numberOfLines = 0;
	[myResourceInfoViewController.descriptionLabel setText:[the_pResource description]];
    [myResourceInfoViewController.descriptionLabel sizeToFit];

	myResourceInfoViewController.contentScroll.contentSize = CGSizeMake(319,	myResourceInfoViewController.descriptionLabel.frame.size.height + 280);

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *documentsFolder = [paths objectAtIndex:0];
    
    UIImage *the_pResourceImage = [UIImage imageWithContentsOfFile:[documentsFolder stringByAppendingPathComponent:the_pResource.image]];     
    
    
    NSLog(@"The the_pResourceImage is **%@ ** %@", [documentsFolder stringByAppendingPathComponent:the_pResource.image],the_pResource.image);

	myResourceInfoViewController.resourceImage.image = the_pResourceImage;
	if (the_pResource.website) {
        if ([the_pResource.website hasPrefix:@"http://"]) {
            myResourceInfoViewController.resourceURL = the_pResource.website;
        } else {
            myResourceInfoViewController.resourceURL =[@"http://" stringByAppendingString:the_pResource.website];
        }
    }
    else 
        the_pResource.website = @"http://www.wheelingcvb.com";
    myResourceInfoViewController.resourceCall = the_pResource.phone;
    myResourceInfoViewController.resourceYoutube = the_pResource.video;
    myResourceInfoViewController.resourceLatitude = the_pResource.latitude;
    myResourceInfoViewController.resourceLongitude = the_pResource.longitude;
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	previousResourceId = currentResourceId; 
	currentResourceId = the_pResource.resourceId;
	
	if (previousResourceId != currentResourceId){
		myResourceInfoViewController.contentScroll.contentOffset = CGPointMake(0, 0);
		myResourceInfoViewController.sameResource = FALSE;
	}
	else {
		myResourceInfoViewController.sameResource = TRUE;
	}
    myResourceInfoViewController.descriptionBubble.backgroundColor=[UIColor whiteColor];
    CGRect frame=myResourceInfoViewController.descriptionLabel.frame;
    frame.size.width+=18;
    frame.size.height+=18;
    frame.origin.x-=9;
    frame.origin.y-=9;
    [myResourceInfoViewController.descriptionBubble setFrame:frame];
    myResourceInfoViewController.descriptionBubble.layer.cornerRadius=10;
}

- (void) flushCache {

    [images release];
    images = [[NSMutableDictionary alloc] init];
    
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    
    [self flushCache];
    [super didReceiveMemoryWarning];
    // Will it create a new empty infoview controller? Apparently it does
    [myResourceInfoViewController release];
    myResourceInfoViewController = [[ResourceInfoViewController alloc] init];
}
@end

