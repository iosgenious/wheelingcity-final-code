//
//  ResourceViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "ResourceInfoViewController.h"

@interface ResourceViewController : UIViewController 
<UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate>  
{
	DataBaseSingleton *dataBaseSingleton;
    ResourceInfoViewController *myResourceInfoViewController;
	IBOutlet UITableView *resourceTableView;
	NSDictionary *resourcesDict;
	int previousResourceId;
	int currentResourceId;
    NSMutableDictionary *images;
    PagerViewController* slideShowController;
    UIBarButtonItem *categoryButton;
    NSMutableArray *categoryArray;

}

@property (nonatomic, retain) ResourceInfoViewController *myResourceInfoViewController;
@property (nonatomic, retain) NSDictionary *resourcesDict;
@property (nonatomic, retain) IBOutlet UITableView *resourceTableView;
@property (nonatomic, retain) NSDictionary *images;
@property (nonatomic, retain) UIBarButtonItem *categoryButton;
@property (nonatomic, retain) NSMutableArray *categoryArray;



- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height;

@end
