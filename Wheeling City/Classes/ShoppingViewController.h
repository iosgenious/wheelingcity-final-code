//
//  ShoppingViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "StoreViewController.h"

@interface ShoppingViewController : UIViewController 
<UITableViewDelegate, UITableViewDataSource>  
{
	DataBaseSingleton *dataBaseSingleton;
    StoreViewController *myStoreViewController;
    UITableView *shoppingTableView;
	NSDictionary *shoppingDict;
    NSArray *areaArray;
	int previousAreaId;
	int currentAreaId;
    NSMutableDictionary *images;
    PagerViewController* slideShowController;

}

@property (nonatomic, retain) StoreViewController *myStoreViewController;
@property (nonatomic, retain) NSDictionary *shoppingDict;
@property (nonatomic, retain) NSArray *areaArray;
@property (nonatomic, retain) IBOutlet UITableView *shoppingTableView;
@property (nonatomic, retain) NSDictionary *images;

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height;

@end
