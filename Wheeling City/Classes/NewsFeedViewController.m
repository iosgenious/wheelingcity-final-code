//
//  NewsFeedViewController.m
//  JITH
//
//  Created by Santosh Singh on 08/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import "NewsFeedViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Macro.h"
#import "MBProgressHUD.h"
#import "JITHAppDelegate.h"
#import "NewsWithNoLoginViewController.h"
#import "JITHFeedsViewController.h"
#import "IlikeItViewController.h"

@interface NewsFeedViewController ()<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>{
    NSMutableArray * newFeedArray;
    UIActivityIndicatorView *spinner;
    MBProgressHUD *mbSpinner;
    BOOL isFeedCall;
	NSMutableArray *arryData;
    NSMutableArray* list;
    NSMutableArray *idsComments;
    NSString *token;
    NSMutableArray *likesCount;
    NSMutableArray *likesFirstCount;
    int prime;
    IBOutlet UIBarButtonItem  *loginButton;
    UIImage * imageUpload;
    
    BOOL isEventShare;
    NSString * shareTitleString;

}
@property(nonatomic,retain)IBOutlet UITableView  *newsTbView;
@property(nonatomic,retain)NSArray               *fbpermissions;
@property (nonatomic, retain) UIImageView *imageView;
@property (retain)     NSMutableData* responseData;
@property (retain)     NSMutableArray* list;
@property(nonatomic,retain)NSURLConnection *connection;


@end

@implementation NewsFeedViewController
@synthesize list;
void networkalertWithMessage (NSString *errorTitle, NSString *message){
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:errorTitle message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertView show];
    [alertView release];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - Helper methods
/*
 * Helper method to show alert results or errors
 */
- (NSString *)checkErrorMessage:(NSError *)error {
    NSString *errorMessage = @"";
    if (error.fberrorUserMessage) {
        errorMessage = error.fberrorUserMessage;
    } else {
        errorMessage = @"Operation failed due to a connection problem, retry later.";
    }
    return errorMessage;
}
/*
 * Helper method to show an alert
 */
- (void)showAlert:(NSString *) alertMsg alertTitle:(NSString *) title{
    if (![alertMsg isEqualToString:@""]) {
        UIAlertView *alertView= [[UIAlertView alloc] initWithTitle:title
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
}

#pragma mark - view life cycle Method

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_newsTbView setHidden:YES];
    prime=0;
    list=[[ NSMutableArray alloc]init];
    
	self.title = @"News";
    
    [self getAllWallPost];
    
    if (![[FBSession activeSession] isOpen]) {
        // create a fresh session object
        FBSession *session = [[FBSession alloc] init];
        [FBSession setActiveSession:session];
    }
    dispatch_async(dispatch_get_current_queue(), ^{
    [self updateUI];
    });

}
-(void)updateUI{
    
    if (![[FBSession activeSession] isOpen]) {
        [loginButton setTitle:@"Login"];
    }else{
        [loginButton setTitle:@"Logout"];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [mbSpinner hide:true];
    [spinner release];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


#pragma mark - Class Instance Method


-(void)showShareDialogWithMessage{
    NSMutableDictionary* parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       kfbAppID, @"app_id",
                                       facebookPageUrl, @"link",
                                       pageWallCommentDescription, @"description",
                                       pageWallCommentTittle, @"name",
                                       pageWallComentPictureUrl, @"picture",
                                       shareTitleString,@"message",
                                       nil];
    
    [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:parameters handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
        if (result==FBWebDialogResultDialogCompleted) {
            [self showAlert:@"Posted on your facebook wall" alertTitle:@"WheelingCity"];
        }else if(result==FBWebDialogResultDialogNotCompleted){
        }else{
            [self showAlert:[error localizedDescription] alertTitle:@"WheelingCity"];
        }
    }];
    
}

/*
 * Method to shoe the FBShare Dialog
 */

-(void)showShareDialog{
    NSMutableDictionary* parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       kfbAppID, @"app_id",
                                       facebookPageUrl, @"link",
                                       pageWallCommentDescription, @"description",
                                       pageWallCommentTittle, @"name",
                                       pageWallComentPictureUrl, @"picture",
                                       nil];
    [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:parameters handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
        if (result==FBWebDialogResultDialogCompleted) {
            [self showAlert:@"Posted on your facebook wall" alertTitle:@"WheelingCity"];
        }else if(result==FBWebDialogResultDialogNotCompleted){
        }else{
            [self showAlert:[error localizedDescription] alertTitle:@"WheelingCity"];
        }
    }];

}
-(void)shareOnFB:(NSString*)titleContent{
    isEventShare=YES;
    shareTitleString=titleContent ;
    if (![[FBSession activeSession] isOpen]) {
        [self loginOutBtnAction:nil];
        return;
    }
    [self showShareDialogWithMessage];
}

-(IBAction)commentBtnAction:(id)sender{
    
    if (![[FBSession activeSession] isOpen]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Connect with Facebook"
                              message:@"Log on to your Facebook account to post a comment on your wall"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
	}
    [self showShareDialog];
	
}
-(IBAction)loginOutBtnAction:(id)sender{
    if (![kAppDel isReachable]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Network Error!"
                              message:@"Internet connection appears to be offline,try again."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return;
    }


  
        if (FBSession.activeSession  != FBSessionStateCreated) {
            // Create a new, logged out session.
            NSArray * fbpermission=[NSArray arrayWithObjects:@"offline_access",@"manage_pages",@"user_likes",@"publish_checkins",@"publish_stream",@"email",@"read_stream",@"publish_actions",nil];
             FBSession * fbSession = [[FBSession alloc] initWithPermissions:fbpermission];
            [FBSession setActiveSession:fbSession];
        
        
        // if the session isn't open, let's open it now and present the login UX to the user
        [FBSession.activeSession openWithBehavior:FBSessionLoginBehaviorForcingWebView completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            switch (status) {
                case FBSessionStateOpen:
                    [loginButton setTitle:@"Logout"];
                    if (isEventShare) {
                        isEventShare=NO;
                        [self showShareDialogWithMessage];
                    }
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

-(IBAction)likeBtnAction:(id)sender{
   
    if (![kAppDel isReachable]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Network Error!"
                              message:@"Internet connection appears to be offline,try again."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];

        return;
    }
    
    if (![FBSession.activeSession isOpen]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Connect with Facebook"
                              message:@"Log on to your Facebook account to like Wheeling City page"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
	}
    IlikeItViewController *dvController = [[IlikeItViewController alloc]initWithNibName:@"IlikeItViewController" bundle:nil];
    [self.navigationController pushViewController:dvController animated:YES];
    [dvController release];
    dvController = nil;
	
}
-(IBAction)cameraBtnAction:(id)sender{
    dispatch_async(dispatch_get_current_queue(), ^{
        [self showPickPhotosActionSheet];
    });
}
/**
 This method select a photo for the galery and pick it. when it is picker the imager picker delegate is called. If there is not a photo in the librry the system give an alert
 
 */

- (IBAction)selectExistingPicture {
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *picker =
        [[UIImagePickerController alloc] init];
        picker.delegate = self;
		picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentModalViewController:picker animated:YES];
        [picker release];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Error accessing photo library"
                              message:@"Device does not support a photo library"
                              delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
/**
 This method take a photo with you camera and pick it. when it is picker the imager picker delegate is called. If you are not login the system give you an alert.
 
 */
- (IBAction)getCameraPicture{

    if (![FBSession.activeSession isOpen]) {
		if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
            //[myPhotosViewController.navigationController setNavigationBarHidden:TRUE animated:YES];
			[self.navigationController setNavigationBarHidden:TRUE animated:YES];
			UIImagePickerController *picker =
			[[UIImagePickerController alloc] init];
			picker.delegate = self;
			picker.allowsEditing = YES;
			picker.sourceType = UIImagePickerControllerSourceTypeCamera;
			[self presentModalViewController:picker animated:YES];
			[picker release];
			
		}
		else {
			UIAlertView *alert = [[UIAlertView alloc]
								  initWithTitle:@"Error accessing camera"
								  message:@"Device has no built-in camera"
								  delegate:nil
								  cancelButtonTitle:@"OK"
								  otherButtonTitles:nil];
			[alert show];
			[alert release];
		}
	}
	else {
		UIAlertView *alert = [[UIAlertView alloc]
							  initWithTitle:@"Connect with Facebook"
							  message:@"Log on to your Facebook account to upload pictures"
							  delegate:nil
							  cancelButtonTitle:@"OK"
							  otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
    
}

/**
 With showPickPhotosActionSheet method you can choose to select in a small menu if yoy want to take a photo or if you want to upload the photo of your photo gallery. You can cancell this action if you want.
 changed _session.isConnected por _facebook.isSessionValid
 */
- (void) showPickPhotosActionSheet {
    
    if ([FBSession.activeSession isOpen]) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Photo from" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Take photo" otherButtonTitles:@"Library",nil];
		[actionSheet showInView:self.view];
		[actionSheet release];
	}
	else {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Connect with Facebook"
                              message:@"Log on to your Facebook account to upload pictures"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
	
}


/**
 ActionSheet is the  method selector for the last method. here is selected to take a photo with the camera or to select one picture of the library
 
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
        [[self navigationController]setNavigationBarHidden:TRUE animated:YES];
		[self getCameraPicture];
	} else if (buttonIndex == 1) {
        [[self navigationController]setNavigationBarHidden:TRUE animated:YES];
		[self selectExistingPicture];
	}
    
}


#pragma mark - Facenook Photo Upload Method

- (void) performPublishAction:(void (^)(void)) action {
    // we defer request for permission to post to the moment of post, then we check for the permission

    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        // if we don't already have the permission, then we request it now
        [FBSession.activeSession requestNewPublishPermissions:@[@"publish_actions"]
        defaultAudience:FBSessionDefaultAudienceFriends
        completionHandler:^(FBSession *session, NSError *error) {
            if (!error) {
                        action();
                    } else if (error.fberrorCategory != FBErrorCategoryUserCancelled){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Permission denied" message:@"Unable to get permission to post" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                [alertView release];
            }
            
        }];
    } else {
        action();
    }
}

// Post Photo button handler

- (void)postPhotoClick:(UIImage *)image {
    
    [self performPublishAction:^{
        FBRequestConnection *connection = [[FBRequestConnection alloc] init];
        connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession
        | FBRequestConnectionErrorBehaviorAlertUser
        | FBRequestConnectionErrorBehaviorRetry;
        
        [connection addRequest:[FBRequest requestForUploadPhoto:imageUpload]
             completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"WheelingCity" message:@"Your photo posted successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alertView show];
                     [alertView release];
                 }else{
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to post the image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alertView show];
                     [alertView release];
                 }
             }];
        [connection start];
        
    }];
}


#pragma mark  - UIImagePickerControllern Delegatde Method

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissModalViewControllerAnimated:YES];
    NSData * imageData=UIImageJPEGRepresentation(image, 0.5);
    
    imageUpload=[UIImage imageWithData:imageData];
        [self postPhotoClick:imageUpload];
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
}

-(void)getAllWallPost{
    if (![kAppDel isReachable]) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Network Error!"
                              message:@"Internet connection appears to be offline,try again."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return;
    }

    mbSpinner=[[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:mbSpinner];
    mbSpinner.delegate=self;
    mbSpinner.labelText=@"Updating";
    [mbSpinner show:true];
    NSString *urlString =[NSString stringWithFormat:@"https://graph.facebook.com/oauth/access_token?type=client_cred&client_id=114215548618946&client_secret=dff0b474f9eb7a39df2406bcbbcecb58"];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept-Type"];
    [request setHTTPMethod:@"GET"];
    isFeedCall=YES;
    
   self.connection=[NSURLConnection connectionWithRequest:request delegate:self];
    [request release];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arryData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[[UITableViewCell alloc]
                 initWithStyle:UITableViewCellStyleDefault
                 reuseIdentifier:CellIdentifier] autorelease];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(14)];
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
        [[cell detailTextLabel] setLineBreakMode:UILineBreakModeWordWrap];
        cell.textLabel.numberOfLines=0;
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;

        
    }
        
    // Set up the cell...
	cell.textLabel.text = [arryData objectAtIndex:indexPath.row];
	    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([FBSession.activeSession isOpen])
    {
        NSString *selectedCell = [arryData objectAtIndex:indexPath.row];
        NSString *selectedId=[idsComments objectAtIndex:indexPath.row];
        
        //Initialize the detail view controller and display it.
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        JITHFeedsViewController *dvController = [[JITHFeedsViewController alloc]initWithNibName:@"JITHFeedsViewController" bundle:[NSBundle mainBundle]];
        dvController.selectedCell = selectedCell;
        dvController.selectedId = selectedId;
        [dvController setToken:[[FBSession.activeSession accessTokenData] accessToken]];
        dvController.totalLikes=[likesCount objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:dvController animated:YES];
        [dvController release];
        
        dvController = nil;
    }
    else {
    NSString *feedContent = [arryData objectAtIndex:indexPath.row];
    NewsWithNoLoginViewController *newsVC = [[NewsWithNoLoginViewController alloc]initWithNibName:@"NewsWithNoLoginViewController" bundle:nil];
    [newsVC setSelectedCellTableView:feedContent];feedContent=nil;
    [newsVC.navigationController setNavigationBarHidden:NO];
    [self.navigationController pushViewController:newsVC animated:YES];
    [newsVC release];
    newsVC = nil;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    UIView* backgroundView = [ [ [ UIView alloc ] initWithFrame:CGRectZero ] autorelease ];
    if(indexPath.row % 2 ==0)
        backgroundView.backgroundColor = [UIColor colorWithRed:0.19 green:0.5 blue:0.81 alpha:0.5];
    else
        backgroundView.backgroundColor = [UIColor colorWithRed:0.18 green:0.15 blue:1.0 alpha:0.5];
    cell.backgroundView = backgroundView;
    for ( UIView* view in cell.contentView.subviews )
    {
        view.backgroundColor = [ UIColor clearColor ];
    }
    
}
#pragma mark - Memoery Management Method

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)dealloc{
    [super dealloc];
}


#pragma mark---------------- NSURLConnection delegate methods-------------

- (NSURLRequest *)connection:(NSURLConnection *)connection
 			 willSendRequest:(NSURLRequest *)request
 			redirectResponse:(NSURLResponse *)redirectResponse {
	return request;
}


- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int code = [httpResponse statusCode];
    //NSLog(@"the response code is %d",code);
    if (code >=200 && code <=299) {
        self.responseData = [NSMutableData data];
    }
    else if (code >=400 && code <=499) {
        [theConnection cancel];
        
    }
    else {
        [theConnection cancel];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
	
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error {
    [mbSpinner hide:true];
    NSString *message = [NSString stringWithFormat:@"Connection is terminated because %@",[error localizedDescription]];
    networkalertWithMessage(@"Error!", message);
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {    
    if(prime==0)
    {
        NSString *json1 = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
        
        NSString *urlString1 =[NSString stringWithFormat:@"https://graph.facebook.com/%@%@%@",facebookPageId,@"/posts?",json1];
        urlString1 = [urlString1 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *url1 = [NSURL URLWithString:urlString1];
        NSMutableURLRequest *request1 = [NSMutableURLRequest requestWithURL:url1 cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
        [NSURLConnection connectionWithRequest:request1 delegate:self ];
        [json1 release];
        prime=1;
    }
    else
    {
        likesFirstCount=[[NSMutableArray alloc]init];
        likesCount=[[NSMutableArray alloc]init];
        //if one post has 0 likes
        NSMutableString *nullmesagge=[[NSMutableString alloc] initWithString:@"0"];
        int indice=0;
        NSMutableDictionary *dictionary =[[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:NULL]];

        
        NSMutableArray *arr = [dictionary mutableArrayValueForKey:@"data"];
        int len = [arr count];
        for(int i = 0; i < len; ++i) {
            NSDictionary *d = [arr objectAtIndex:i];
            [list addObject:d];
        }
        
        for(NSDictionary *message in arr){
            NSDictionary *user = [message objectForKey:@"likes"];
            NSMutableString *facebookLikes=[user objectForKey:@"count"];
            
            if(facebookLikes!=nil){
                [likesFirstCount insertObject:facebookLikes atIndex:indice];
            }
            else{
                
                [likesFirstCount insertObject:nullmesagge atIndex:indice];
            }
            indice++;
        }
        
        [nullmesagge release];
        arryData=[[NSMutableArray alloc]init];
        idsComments=[[NSMutableArray alloc]init];
        
        NSUInteger index = 0;
        NSUInteger internal=0;
        for ( id key in list)
        {
            NSMutableString *message= [[list objectAtIndex:index] objectForKey:@"message"];
            NSMutableString *comments= [[list objectAtIndex:index] objectForKey:@"id"];
            NSMutableString *likes= [likesFirstCount objectAtIndex:index];
            if(message!=nil){
                [arryData insertObject:message atIndex:internal];
                [idsComments insertObject:comments atIndex:internal];
                [likesCount insertObject:likes atIndex:internal];
                index++;
                internal++;
            }
            else
            {index++;
                
            }
        }
       
        [_newsTbView setHidden:NO];
        [_newsTbView reloadData];
        [likesFirstCount release];
        [list release];
        [mbSpinner hide:true];
        [spinner release];
    }


}


- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [mbSpinner removeFromSuperview];
    [mbSpinner release];
	mbSpinner = nil;
}

@end
