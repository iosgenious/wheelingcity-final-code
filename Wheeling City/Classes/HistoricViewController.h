//
//  HistoricViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "HistoricInfoViewController.h"

@interface HistoricViewController : UIViewController 
<UITableViewDelegate, UITableViewDataSource>  
{
	DataBaseSingleton *dataBaseSingleton;
    HistoricInfoViewController *myHistoricInfoViewController;
	IBOutlet UITableView *historicTableView;
	NSArray *historicArray;
	int previousHistoricId;
	int currentHistoricId;
    NSMutableDictionary *images;
    PagerViewController* slideShowController;
}

@property (nonatomic, retain) HistoricInfoViewController *myHistoricInfoViewController;
@property (nonatomic, retain) NSArray *historicArray;
@property (nonatomic, retain) IBOutlet UITableView *historicTableView;
@property (nonatomic, retain) NSDictionary *images;

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height;

@end
