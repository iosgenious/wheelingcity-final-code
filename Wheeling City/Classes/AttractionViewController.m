    //
//  AttractionViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Modified by Denis Santxez 
//  Copyright 2010 W. All rights reserved.
//

#import "AttractionViewController.h"
#import "Attraction.h"
#import "JITHAppDelegate.h"
#import "FirstViewController.h"
#import "Macro.h"
#import "UIImageView+WebCache.h"

@interface AttractionCell : UITableViewCell
@end

@implementation AttractionCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(5,7, 60, 60)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
    
}

@end

@implementation AttractionViewController
@synthesize myAttractionInfoViewController, attractionsDict, attractionTableView, images, categoryArray;

- (void)viewDidLoad {
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	attractionsDict = [[NSDictionary alloc ] initWithDictionary:dataBaseSingleton.attractionsDict];
    categoryArray = [[NSMutableArray alloc]initWithArray:dataBaseSingleton.attractionCategory];
	attractionTableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	self.title = @"Attractions";
    self.images = [[NSMutableDictionary alloc] init];
    
    //Start scrollview
    slideShowController = [[PagerViewController alloc] init];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    slideShowController.view.autoresizesSubviews = YES;
    [slideShowController setImages:[NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"CapitolTheater3.png"],
                                    [UIImage imageNamed:@"dacfeec0e5bedad69ab379e49be8b07e.png"],
                                    [UIImage imageNamed:@"oglebay_park_pictures_011.jpg"],
                                    [UIImage imageNamed:@"oglebay_park_pictures_026.jpg"],
                                    [UIImage imageNamed:@"oglebay_park_pictures_080.jpg"],
                                    nil]];
    
    //  imagen 2 [UIImage imageNamed:@"oglebay_park_pictures_005.jpg"],
    // imagen 4  [UIImage imageNamed:@"oglebay_park_pictures_023.jpg"],


    
    CGRect slideShowFrame=CGRectMake(0, 0, 320, 189);
    slideShowController.view.frame=slideShowFrame;
    [attractionTableView setTableHeaderView:slideShowController.view];
    categoryButton = [[UIBarButtonItem alloc] initWithTitle:@"Categories" style:UIBarButtonItemStylePlain target:self action:@selector(showCategories:)];
    [[self navigationItem] setRightBarButtonItem:categoryButton];
    [attractionTableView.tableHeaderView setFrame:CGRectMake(0, 0, 320, 120)];
 

}

- (void)showCategories:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Jump to category" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    int idx = 0;
    for (NSString *category in categoryArray) {
        [actionSheet addButtonWithTitle:category];
        idx++;
    }
    [actionSheet addButtonWithTitle:@"Cancel"];
    
    actionSheet.cancelButtonIndex = idx++;    
    [actionSheet showInView:self.view];
    [actionSheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.cancelButtonIndex!=buttonIndex)
    {
        [attractionTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:buttonIndex] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        CGPoint point = attractionTableView.contentOffset;
        point .y -= 44.0;
        [attractionTableView setContentOffset:point];
    }
}


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
	[self.navigationController setNavigationBarHidden:FALSE animated:NO];
    myAttractionInfoViewController = [[AttractionInfoViewController alloc] init];

}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [myAttractionInfoViewController release];self.myAttractionInfoViewController=nil;
    [slideShowController release];slideShowController=nil;
    [attractionTableView setDelegate:nil];
    [attractionTableView setDataSource:nil];
	[attractionTableView release];attractionTableView=nil;
	[attractionsDict release];self.attractionsDict=nil;
}

- (void)dealloc {
    self.images=nil;
	[myAttractionInfoViewController release];self.myAttractionInfoViewController=nil;
    [slideShowController release];slideShowController=nil;
	[attractionTableView release];self.attractionTableView=nil;
	[attractionsDict release];self.attractionsDict=nil;
   [super dealloc];
}

#pragma mark -
#pragma mark Table view data source methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [attractionsDict count];
}

- (NSString *)getNameFromSection:(NSInteger)section {
    return [categoryArray objectAtIndex:section];
}
  

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self getNameFromSection:section];
}


- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
    return [[attractionsDict objectForKey:[self getNameFromSection:section]] count];    
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	
	AttractionCell *cell = (AttractionCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (AttractionCell*)[[[AttractionCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15];// systemFontOfSize:16];
        cell.textLabel.textColor = [UIColor whiteColor];
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
        cell.textLabel.numberOfLines=0;
	}
	
    Attraction *the_pAttraction = [[attractionsDict objectForKey:[self getNameFromSection:indexPath.section]] objectAtIndex:indexPath.row];
    
	cell.textLabel.text = the_pAttraction.name;
    [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *image = nil;
        if ([self.images objectForKey:the_pAttraction.image]) {
            image=[self.images objectForKey:the_pAttraction.image];
        }
        
        if(!image)
        {
            // Image is not on the cached NSMUtableDictionary. Check if image is available on the documents folder and copy from there
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString *documentsFolder = [paths objectAtIndex:0];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:the_pAttraction.image]]) {
                UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pAttraction.image]];
                
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                if (pngFile) {
                    image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                    [self.images setValue:image forKey:the_pAttraction.image];
                }
                
            }
            
            else {
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[@"http://wheeling.yourmobicity.com/English/Attractions/" stringByAppendingString:the_pAttraction.image]]];
                UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
               
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                if (pngFile) {
                    image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                    [self.images setValue:image forKey:the_pAttraction.image];
                }
                
                [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:the_pAttraction.image] atomically:YES];
                [unresizedImage release];
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (UIImagePNGRepresentation(image)) {
                [[cell imageView] setImage:image];
            }
            [cell setNeedsLayout];
        });
    });
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];

	Attraction *the_pAttraction = [[attractionsDict objectForKey:[self getNameFromSection:indexPath.section]] objectAtIndex:indexPath.row];

    [[self navigationController] pushViewController:myAttractionInfoViewController animated:YES];
	
	//Setting title of the artist view
	myAttractionInfoViewController.title = the_pAttraction.name;
	myAttractionInfoViewController.descriptionLabel.numberOfLines = 0;
	[myAttractionInfoViewController.descriptionLabel setText:[the_pAttraction description]];
    [myAttractionInfoViewController.descriptionLabel sizeToFit];

	myAttractionInfoViewController.contentScroll.contentSize = CGSizeMake(319,	myAttractionInfoViewController.descriptionLabel.frame.size.height + 280);

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *documentsFolder = [paths objectAtIndex:0];
    
    UIImage *the_pAttractionImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pAttraction.image]];     
	
	myAttractionInfoViewController.attractionImage.image = the_pAttractionImage;	
    myAttractionInfoViewController.attractionCall = the_pAttraction.phone;
    myAttractionInfoViewController.attractionURL = the_pAttraction.website;
    myAttractionInfoViewController.attractionYoutube = the_pAttraction.video;
    myAttractionInfoViewController.attractionLatitude=the_pAttraction.latitude;
    myAttractionInfoViewController.attractionLongitude=the_pAttraction.longitude;
	
	previousAttractionId = currentAttractionId; 
	currentAttractionId = the_pAttraction.attractionId;
	
	if (previousAttractionId != currentAttractionId){
		myAttractionInfoViewController.contentScroll.contentOffset = CGPointMake(0, 0);
		myAttractionInfoViewController.sameAttraction = FALSE;
	}
	else {
		myAttractionInfoViewController.sameAttraction = TRUE;
	}
    myAttractionInfoViewController.bubble.backgroundColor=[UIColor whiteColor];
    CGRect frame=myAttractionInfoViewController.descriptionLabel.frame;
    frame.size.width+=18;
    frame.size.height+=18;
    frame.origin.x-=9;
    frame.origin.y-=9;
    [myAttractionInfoViewController.bubble setFrame:frame];
    myAttractionInfoViewController.bubble.layer.cornerRadius=10;
}

- (void) flushCache {

    [images release];
    images = [[NSMutableDictionary alloc] init];
    
}
- (void)didReceiveMemoryWarning{
    [self flushCache];
    [super didReceiveMemoryWarning];
    // Will it create a new empty infoview controller? Apparently it does
    [myAttractionInfoViewController release];
    myAttractionInfoViewController = [[AttractionInfoViewController alloc] init];
}
@end

