//
//  NSString+Null.h
//  StreetBid
//
//  Created by Santosh on 09/03/13.
//  Copyright (c) 2013 Manoj Yadav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Null)

-(NSString*)notNullString;
@end
