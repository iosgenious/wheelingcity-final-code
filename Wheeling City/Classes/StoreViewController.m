//
//  StoreViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Modified by Denis Santxez 
//  Copyright 2010 W. All rights reserved.
//

#import "StoreViewController.h"
#import "Shopping.h"
#import "JITHAppDelegate.h"
#import "FirstViewController.h"
#import "Macro.h"

@interface StoreCell : UITableViewCell
@end

@implementation StoreCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(5,7, 60, 60)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
}

@end



@implementation StoreViewController
@synthesize myShoppingInfoViewController, shoppingArray, shoppingTableView, areaImage, storeCall,descriptionArea,storeLatitude,storeLongitude,images;

BOOL first;

UIWebView  *webview;
CLLocationManager *GPSlocation;
double userLongitude;
double userLatitude;


- (void)viewDidLoad {
	
    [super viewDidLoad];
    storeLongitude=@"";
    storeLatitude=@"";
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	shoppingTableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    //myShoppingInfoViewController = [[ShoppingInfoViewController alloc] init];
    //SlideShow Starter
  
    flipButton = [[UIBarButtonItem alloc] initWithTitle:@"Description" style:UIBarButtonItemStylePlain target:self action:@selector(flipDesc:)];
    [[self navigationItem] setRightBarButtonItem:flipButton];
    [flipButton release];
    
    slideShowController = [[PagerViewController alloc] init];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    slideShowController.view.autoresizesSubviews = YES;
    if([areaImage isEqualToString:@"Shopping.png"])
    {
        areaImage=@"Tammy Stein Wheeling Pictures 029.png";
        [slideShowController setImages:[NSArray arrayWithObjects:
                                        [UIImage imageNamed:@"Tammy Stein Wheeling Pictures 029.png"],
                                        nil]];
        
    }
    else
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentsFolder = [paths objectAtIndex:0];
        [slideShowController setImages:[NSArray arrayWithObjects:
                                        [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:areaImage]],
                                        nil]];
    }
    
    CGRect slideShowFrame=CGRectMake(0, 0, 320, 189);
    slideShowController.view.frame=slideShowFrame;
    [shoppingTableView setTableHeaderView:slideShowController.view];
    [shoppingTableView.tableHeaderView  setAutoresizesSubviews:YES];
    [shoppingTableView.tableHeaderView setFrame:CGRectMake(0, 0, 320, 120)];
  


}


- (void)flipDesc:(id)sender {
    if ([descriptionArea isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle:@"Area Description" 
                              message:@"No description available"  
                              delegate:nil 
                              cancelButtonTitle:@"OK" 
                              otherButtonTitles:nil];
        [alert show];
        [alert release];        
        
        
        
        
    }
    else{
    
    UIAlertView *alert = [[UIAlertView alloc] 
                          initWithTitle:@"Area Description" 
                          message:descriptionArea 
                          delegate:nil 
                          cancelButtonTitle:@"OK" 
                          otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
 

}



- (void)viewDidUnload {
    [super viewDidUnload];
    [self release];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    }

- (void) viewDidAppear:(BOOL)animated {
	[self.navigationController setNavigationBarHidden:FALSE animated:YES];
    [self infoArea:nil];

    
    
}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();	
    return resizedImage;
}

- (void)dealloc {
	[myShoppingInfoViewController release];
    [slideShowController release];
	[shoppingTableView release];
	[shoppingArray release];
    [webview release];
    //[super dealloc];
}

#pragma mark -
#pragma mark Table view data source methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

    

- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
    return [shoppingArray count];    
}



- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"Cell";
	
	StoreCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (StoreCell*)[[[StoreCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	Shopping *the_pShopping = [shoppingArray objectAtIndex:indexPath.row];
	
	cell.textLabel.font = [UIFont boldSystemFontOfSize:15];// systemFontOfSize:16];
	cell.textLabel.textColor = [UIColor whiteColor];
	cell.textLabel.text = the_pShopping.name;
	[[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
    cell.textLabel.numberOfLines=0;
    if ([the_pShopping.image isEqualToString:@"Shopping.png"]) {
        [[cell imageView] setImage:[UIImage imageNamed:@"Shopping.png"]];
        
    }
    else {
        [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
    }  
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *image = [self.images objectForKey:the_pShopping.image];
        if(!image) // && [the_pArea.image isEqualToString:@"Shopping.png"])
        {
            // Image is not on the cached NSMUtableDictionary. Check if image is available on the documents folder and copy from there
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString *documentsFolder = [paths objectAtIndex:0];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:the_pShopping.image]]) {
                UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pShopping.image]];     
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pShopping.image];            
            }
            
            // NSFileManager *the_pFileManager = [NSFileManager defaultManager];
            // NSLog(@" Full path of file to be created %@", [documentsFolder stringByAppendingPathComponent:the_pArtist.image]);
            else {
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[@"http://wheeling.yourmobicity.com/English/Shopping/" stringByAppendingString:the_pShopping.image]]];
                UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pShopping.image];
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                
                [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:the_pShopping.image] atomically:YES];
                [unresizedImage release];
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[cell imageView] setImage:image];
            [cell setNeedsLayout];
        });
    });
    return cell;
   // Shopping.png
}

   
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Navigation logic may go here ‚Äî for example, create and push another view controller.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call", @"Get me there!", nil];
    
    Shopping *the_pShopping = [shoppingArray objectAtIndex:indexPath.row];
    storeCall = the_pShopping.phone;
    storeLatitude = the_pShopping.latitude;
    storeLongitude = the_pShopping.longitude;
    
    [actionSheet showInView:self.view];
    [actionSheet release];

}


- (void)callStore {
    
    if (!storeCall){
        UIAlertView *errorView;
        errorView = [[UIAlertView alloc] 
					 initWithTitle: @"No phone number" 
					 message: @"Contact number not available" 
					 delegate: self 
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];  
		[errorView show];
		[errorView autorelease];
        
    }
    else{
        if(webview==nil)
            webview = [[UIWebView alloc] initWithFrame:self.view.frame];
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:storeCall]]]];
    }
}

-(void)bringGoogleMaps
{
   
    if(storeLatitude!=nil || storeLongitude!=nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Go to that location" message:@"This will open the Maps App" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No location" message:@"There is no location information" delegate:self 
                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    userLatitude=newLocation.coordinate.latitude;
    userLongitude=newLocation.coordinate.longitude;
    [GPSlocation stopUpdatingLocation];
    [GPSlocation release];
    [self openMap:[NSString stringWithFormat:@"saddr=%f,%f&daddr=%@,%@&output=dragdir",userLatitude,userLongitude,storeLatitude,storeLongitude]];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex!=0)
    {
        GPSlocation=[[CLLocationManager alloc]init];
        GPSlocation.delegate=self;
        [GPSlocation startUpdatingLocation];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
        [self callStore];
    } 
    else if (buttonIndex == 1) {
        // ToDo: GoogleMaps
        [self bringGoogleMaps];
    } 
   }

- (void) flushCache {
    
    [images release];
    images = [[NSMutableDictionary alloc] init];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [self flushCache];
  
}

- (void)infoArea:(id)sender {
    if ([descriptionArea isEqualToString:@""]){
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] 
                              initWithTitle:@"Area Description" 
                              message:descriptionArea 
                              delegate:nil 
                              cancelButtonTitle:@"OK" 
                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
    
}

#pragma mark - Map Method

-(void)openMap:(NSString*)urlString{
    NSString *url;
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
    {
        url = [NSString stringWithFormat:@"http://maps.google.com/maps?%@",urlString];
    }
    else
    {
        url = [NSString stringWithFormat:@"http://maps.apple.com/maps?%@",urlString];
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

@end

