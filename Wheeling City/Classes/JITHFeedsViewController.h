//
//  JITHFeeds.h
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 09/03/11.
//  Copyright 2011 W. All rights reserved.
//



#import <UIKit/UIKit.h>
//#import "DetailViewController.h"
#import "MBProgressHUD.h"

@interface JITHFeedsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,MBProgressHUDDelegate> {
    
    /**
     IBOUtlets that appears in the view
     */
    IBOutlet UITextView *lblText;
    IBOutlet UITableView *postCommentsTable;
    IBOutlet UITextField *addCommentText;

    /*
     Variables that are passed for the other view controller and we stored in that ones
     */
    NSString *totalLikes;
    NSString *selectedCell;
    NSString *selectedId;
    NSString *token;
    
    /**
     Some variables defined to save data structures
     */
    NSMutableData* responseData;
    NSMutableArray* list;
    NSMutableArray *namesComments;
    NSMutableArray *namesIds;
    NSMutableArray *arryData;
    NSMutableArray *commentsData;
    
    /**
     other variables
     */
    
    CGFloat animatedDistance; 
    NSString *selectedLikes;
    UIActivityIndicatorView *spinner;
    MBProgressHUD *mbSpinner;
    



    
}
@property (nonatomic, retain)NSString *totalLikes;
@property (nonatomic, retain)NSString *selectedCell;
@property (nonatomic,retain)NSString *selectedLikes;
@property (nonatomic, retain)NSString *selectedId;
@property (retain)     NSMutableData* responseData;
@property (retain)     NSMutableArray* list;
@property (nonatomic, retain)NSString *token;

/**
 With this method we post a comment in JITH feed. We post in the selected post. First we have to get what is the id of the post. To obtain that we have to manage the selectedId. It has two parts, and we have to selected the second part of that. Then when we have de id of the selected id and the user press the send button the comment is send to Facebook. This is done calling the Facebook Delegate
 */
-(IBAction)addCommentInJITHSomeComment:(id)sender;

@end
