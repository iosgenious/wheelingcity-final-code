//
//  FirstViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/24/10.
//  Copyright 2010 W. All rights reserved.
//

#import "FirstViewController.h"
#import "NetworkReachability.h"
#import <QuartzCore/CALayer.h>
#import "Macro.h"
#import "WhatsHappeningViewController.h"

@implementation FirstViewController


@synthesize myLodgingViewController;
@synthesize myShoppingViewController;
@synthesize myHistoricViewController;
@synthesize myAttractionViewController;
@synthesize mySiteMapViewController;
@synthesize myYoutubeChannelViewController;
@synthesize myNewsTicker;
@synthesize NewsLabel;
@synthesize myDiningViewController;
@synthesize myResourceViewController;
@synthesize myInfoViewController;
@synthesize mySponsorLoadViewController;
@synthesize arrow;
@synthesize weatherBubble;
@synthesize weatherLabel;
@synthesize lodgingButton;


CGRect windowFrame;
NSTimer* myTimer;
int from,to;
int updatetimeseg=5;


- (void)updateNewsTicker:(BOOL)firstTime
{
    if(!firstTime)
    {
        if([dataBaseSingleton isDBConnected])
        {
            [dataBaseSingleton setDatabaseVars];
            [dataBaseSingleton readNewsTicker];
            [myNewsTicker setText:dataBaseSingleton.newsTicker];
        }
        else
            [myNewsTicker setText:@"Check your Internet Connection. Some connection failures were detected"];
        //calculate size
        updatetimeseg=60;
        
        CGSize textSize = [dataBaseSingleton.newsTicker sizeWithFont:[myNewsTicker font]];
        CGRect frame = myNewsTicker.frame;
        frame.size.width=textSize.width;
        frame.origin.x=from;
        [myNewsTicker setFrame:frame];
        to=NewsLabel.frame.size.width - myNewsTicker.frame.size.width;
        
        
    }
}

- (void)getWeather
{
    //  using http resource file
    CXMLDocument *doc = [[[CXMLDocument alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://www.weather.gov/xml/current_obs/KHLG.xml"] options:0 error:nil] autorelease];
    
    //  searching for temperature nodes
    int temp_f = [[[doc nodeForXPath:@"/current_observation/temp_f" error:nil] stringValue] intValue];
    int temp_c = [[[doc nodeForXPath:@"/current_observation/temp_c" error:nil] stringValue] intValue];
    NSString *temps = [[NSString alloc] initWithFormat:@"%d°F\n%d°C",temp_f,temp_c];
    [weatherLabel setText:temps];
}

- (void)moveNewsTicker
{
    static int number=0;
    static BOOL directionUP=TRUE;
    number++;
    CGFloat arrowAlpha=[arrow alpha];
    CGRect frame=myNewsTicker.frame;
    if(frame.origin.x < to)
    {
        if(number >= updatetimeseg*100)
        {
            number=0;
            [self updateNewsTicker:false];
        }
        else
        {
            CGRect frame=myNewsTicker.frame;
            frame.origin.x=from;
            [myNewsTicker setFrame:frame];
        }
    }
    else
    {
        frame.origin.x-=1;
        [myNewsTicker setFrame:frame];
    }
    
    if(arrowAlpha<=0.1)
        directionUP=true;
    else if (arrowAlpha>=0.9)
        directionUP=false;
    if(directionUP)
        arrowAlpha+=0.01;
    else
        arrowAlpha-=0.01;
    [arrow setAlpha:arrowAlpha];
    [self.view bringSubviewToFront:arrow];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    dataBaseSingleton = [[DataBaseSingleton alloc] init];
	[dataBaseSingleton setDatabaseVars];
	self.title = @"Main";
	self.navigationController.navigationBarHidden = TRUE;
    self.view.hidden = TRUE;
    [self.newsFeedView setFrame:CGRectMake(0, IS_IPHONE5?206:156, 320, 30)];
    
    //initialize news ticker
    myNewsTicker.adjustsFontSizeToFitWidth=false;
    [myNewsTicker sizeToFit];
    //News ticker
    windowFrame=self.view.frame;
    from=windowFrame.size.width;
    to=NewsLabel.frame.size.width - myNewsTicker.frame.size.width;
    //Move News ticker to begin the animation
    CGRect frame=myNewsTicker.frame;
    frame.origin.x=from;
    [myNewsTicker setFrame:frame];
    [self updateNewsTicker:true];
    
    //Start scrollview
    slideShowController = [[HomePagerViewController alloc] init];
    [slideShowController.view setFrame:CGRectMake(0, 0, 320, 220)];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    slideShowController.view.autoresizesSubviews = YES;
    [slideShowController setImages:[NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"WheelingClockSmall.png"],
                                    [UIImage imageNamed:@"DSC_0195.png"],
                                    [UIImage imageNamed:@"Tammy Stein Wheeling Pictures 009.png"],
                                    [UIImage imageNamed:@"184dbc22290e0fcafcc2b2cc63df7f17.png"],
                                    [UIImage imageNamed:@"weedhawks3.png"],
                                    [UIImage imageNamed:@"CapitolTheater3.png"],
                                    [UIImage imageNamed:@"oglebay_park_pictures_025.jpg"],
                                    nil]];
    slideShowController.dissapear=arrow;
    slideShowController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    slideShowController.view.layer.shadowOffset = CGSizeMake(0, 1);
    slideShowController.view.layer.shadowOpacity = 1;
    slideShowController.view.layer.shadowRadius = 6.0;
    [self.view insertSubview:slideShowController.view atIndex:2];
    [self bringLoadingView];
    [self.view bringSubviewToFront:arrow];
    if(myTimer==nil)
        myTimer = [NSTimer scheduledTimerWithTimeInterval:.012 target:self selector:@selector(moveNewsTicker) userInfo:nil repeats:YES];
    weatherBubble.hidden = true;
    weatherBubble.layer.cornerRadius = 10;
    [weatherBubble.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [weatherBubble.layer setBorderWidth: 1.0];
    weatherBubble.layer.shadowColor = [UIColor blackColor].CGColor;
    weatherBubble.layer.shadowOffset = CGSizeMake(5, 5);
    weatherBubble.layer.shadowOpacity = 1;
    weatherBubble.layer.shadowRadius = 6.0;
    
    [UIView animateWithDuration:0.5 animations:^{
        weatherBubble.alpha = 0.6;
    }];
    
    [self getWeather];
    weatherBubble.hidden = false;
    
}



- (IBAction)bringSpotLightView:(id)sender
{
    [self flushCache];
    [self checkNetwork];
    if (myYoutubeChannelViewController == nil)
        myYoutubeChannelViewController = [[youtube_channelViewController alloc] init];
    [[self navigationController] pushViewController:myYoutubeChannelViewController animated:YES];
}

- (void)bringLoadingView {
    if (mySponsorLoadViewController==nil)
        mySponsorLoadViewController = [[SponsorLoadViewController alloc] init];
    mySponsorLoadViewController.delegate = self;
    mySponsorLoadViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentModalViewController:mySponsorLoadViewController animated:NO];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	[self.navigationController setNavigationBarHidden:TRUE animated:YES];
    self.view.hidden = FALSE;
}

-(void)bringShoppingView:(id)sender
{
    [self flushCache];
    if(myShoppingViewController==nil)
        myShoppingViewController = [[ShoppingViewController alloc]init];
    //TODO For some reason, the database vars dissapear causing bugs all over the appok
    [dataBaseSingleton setDatabaseVars];
	[dataBaseSingleton readShoppingFromDatabase];
	//[dataBaseSingleton checkAndCreateDatabase];
	[[self navigationController] pushViewController:myShoppingViewController animated:YES];
}

- (IBAction)bringWhatsHappening:(id)sender
{
    [self flushCache];
    WhatsHappeningViewController * objVC=[[WhatsHappeningViewController alloc] initWithNibName:@"WhatsHappeningViewController" bundle:nil];
       [[self navigationController] pushViewController:objVC animated:YES];
    [objVC release];objVC=nil;
}

-(void)bringHistoricView:(id)sender
{
    [self flushCache];
    if(myHistoricViewController==nil)
        myHistoricViewController = [[HistoricViewController alloc]init];
 	//TODO For some reason, the database vars dissapear causing bugs all over the appok
    [dataBaseSingleton setDatabaseVars];
	[dataBaseSingleton readHistoricFromDatabase];
	//[dataBaseSingleton checkAndCreateDatabase];
	[[self navigationController] pushViewController:myHistoricViewController animated:YES];
}

- (IBAction)bringLodgingView:(id)sender {
    [self flushCache];
    if(myLodgingViewController==nil)
        myLodgingViewController = [[LodgingViewController alloc] init];
	//TODO For some reason, the database vars dissapear causing bugs all over the appok
    [dataBaseSingleton setDatabaseVars];
	[dataBaseSingleton readLodgingFromDatabase];
	//[dataBaseSingleton checkAndCreateDatabase];
	[[self navigationController] pushViewController:myLodgingViewController animated:YES];
}

- (IBAction)bringAttractionsView:(id)sender{
    [self flushCache];
    if(myAttractionViewController==nil)
        myAttractionViewController = [[AttractionViewController alloc] init];
    [dataBaseSingleton setDatabaseVars];
	[dataBaseSingleton readAttractionsFromDatabase];
	[[self navigationController] pushViewController:myAttractionViewController animated:YES];
}


- (IBAction)bringDiningView:(id)sender
{
    [self flushCache];
    if(myDiningViewController==nil)
        myDiningViewController = [[DiningViewController alloc]init];
    [dataBaseSingleton setDatabaseVars];
	[dataBaseSingleton readDiningFromDatabase];
	//[dataBaseSingleton checkAndCreateDatabase];
	[[self navigationController] pushViewController:myDiningViewController animated:YES];
}

- (IBAction)bringResourcesView:(id)sender{
    [self flushCache];
    if(myResourceViewController==nil)
        myResourceViewController = [[ResourceViewController alloc] init];
    [dataBaseSingleton setDatabaseVars];
	[dataBaseSingleton readResourcesFromDatabase];
	[[self navigationController] pushViewController:myResourceViewController animated:YES];
}

- (void)checkNetwork{
	
	UIAlertView *errorView;
	NetworkReachability* internetReach;
	internetReach = [[NetworkReachability reachabilityForInternetConnection] retain];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	[internetReach release];
	if (netStatus == NotReachable) {
		// Could be ReachableViaWWAN and ReachableViaWiFi
		
		//NSLog(@"NotReachable");
		
		errorView = [[UIAlertView alloc]
					 initWithTitle: @"Network Error"
					 message: @"There is no internet conection!"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
	}
}


- (IBAction)bringSiteMapView:(id)sender{
    [self flushCache];
    if(mySiteMapViewController==nil)
        mySiteMapViewController = [[SiteMapViewController alloc] init];
    /*[dataBaseSingleton setDatabaseVars];
     [dataBaseSingleton readCoordenatesDinning];[dataBaseSingleton readCoordenatesAttractions];
     [dataBaseSingleton readCoordenatesHistorical];[dataBaseSingleton readCoordenatesLodging];
     [dataBaseSingleton readCoordenatesShopping];[dataBaseSingleton readCoordenatesResources];*/
	[[self navigationController] pushViewController:mySiteMapViewController animated:YES];
}

- (IBAction)bringMyPhotosView:(id)sender{
    [self didReceiveMemoryWarning];
	//[[self navigationController] pushViewController:myPhotosViewController animated:YES];
}

- (IBAction)bringTwiterView:(id)sender{
	[self checkNetwork];
    
	[[self navigationController] pushViewController:myYoutubeChannelViewController animated:YES];
    
    //DO nothing
}


- (IBAction)callJITH4Tickets:(id)sender {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Call JITH for Tickets"
												   delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"CANCEL",nil];
	[alert show];
	[alert release];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex  {
	if (buttonIndex == 0)
	{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://8006245456"]];
	}
}

- (void)infoViewControllerDidFinish:(InfoViewController *)controller
{
    [self dismissModalViewControllerAnimated:YES];
}

// SponsorLoadViewController Delegate Method
- (void) sponsorLoadViewControllerDidFinish:(SponsorLoadViewController *)controller{
    [self dismissModalViewControllerAnimated:YES];

}




- (IBAction)bringInfoView:(id)sender {
    if(myInfoViewController==nil)
        myInfoViewController = [[InfoViewController alloc] init];
    myInfoViewController.delegate = self;
    myInfoViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:myInfoViewController animated:YES];
}
//- (void)photosViewControllerDidFinish:(PhotosViewController *)controller
//{
//    [self dismissModalViewControllerAnimated:YES];
//}
//
//
- (IBAction)bringInFacebook:(id)sender {
    [self flushCache];
    NewsFeedViewController *newsVC=[[NewsFeedViewController alloc] initWithNibName:@"NewsFeedViewController" bundle:nil];

	[[self navigationController] pushViewController:newsVC animated:YES];
    [newsVC release];
    newsVC=nil;

}

-(void)flushCache
{
    if(myLodgingViewController!=nil)
    {
        [myLodgingViewController release];
        myLodgingViewController=nil;
    }
    
    if(myResourceViewController!=nil)
    {
        [myResourceViewController release];
        myResourceViewController=nil;
    }
    if(myShoppingViewController!=nil)
    {
        [myShoppingViewController release];
        myShoppingViewController=nil;
    }
    if(mySiteMapViewController!=nil)
    {
        [mySiteMapViewController release];
        mySiteMapViewController=nil;
    }
    
    if(myYoutubeChannelViewController!=nil)
    {
        [myYoutubeChannelViewController release];
        myYoutubeChannelViewController=nil;
    }
    if(myHistoricViewController!=nil)
    {
        [myHistoricViewController release];
        myHistoricViewController=nil;
    }
    if(myDiningViewController!=nil)
    {
        [myDiningViewController release];
        myDiningViewController=nil;
    }
}

-(void)didReceiveMemoryWarning
{
    [self flushCache];
    [super didReceiveMemoryWarning];
}


//If you kill it when low memory, it makes all very unstable

/*
 - (void)dealloc {
 [myLodgingViewController release];
 [mySponsorViewController release];
 [mySiteMapViewController release];
 [myPhotosViewController release];
 [youtube_channelViewController release];
 [myInfoViewController release];
 [super dealloc];
 }
 
 
 
 */


@end

