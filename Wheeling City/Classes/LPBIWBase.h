//
//  LPBIWBase.h
//  layarplayer
//
//  Created by Lawrence Lee on 10/23/10.
//  Copyright (c) 2010 Layar B.V. All rights reserved.
//

//System imports
#import <UIKit/UIKit.h>

#define LPBIW_HEIGHT					100.0f
#define LPBIW_BANNER_ICON_VIEW_HEIGHT	26.0f
#define LPBIW_BANNER_ICON_VIEW_WIDTH	60.0f
#define LPBIW_BANNER_LABEL_HEIGHT		26.0f
#define BANNERFONT						[UIFont fontWithName:@"Helvetica-Bold" size:15.0f]
#define LINEFONT						[UIFont fontWithName:@"Helvetica" size:12.0f]

@class LSPOI;

@class LPImageRequestManager;
@class LPImageRequest;

@interface LPBIWBase : UIView
{
	LSPOI *poi;
	UIView *container;
	UIView *poiContainer;
	UIView *bannerView;
	UIImageView *bannerIconView;
	UILabel *bannerLabel;
	UIImageView *imageView;
	UILabel *line2;
	UILabel *line3;
	UILabel *line4;
	UILabel *attribution;
	UIImage *placeholder;
	
	LPImageRequestManager *imageRequestManager;
	LPImageRequest *bannerIconRequest;
	LPImageRequest *imageRequest;
}

@property (nonatomic, retain) LSPOI *poi;
@property (nonatomic, retain) UIView *container;
@property (nonatomic, retain) UIView *poiContainer;
@property (nonatomic, retain) UIView *bannerView;
@property (nonatomic, retain) UIImageView *bannerIconView;
@property (nonatomic, retain) UILabel *bannerLabel;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UILabel *line2;
@property (nonatomic, retain) UILabel *line3;
@property (nonatomic, retain) UILabel *line4;
@property (nonatomic, retain) UILabel *attribution;
@property (nonatomic, retain) UIImage *placeholder;
@property (nonatomic, retain) LPImageRequestManager *imageRequestManager;

@end
