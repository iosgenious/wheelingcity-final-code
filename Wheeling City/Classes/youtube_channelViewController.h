//
//  youtube_channelViewController.h
//  youtube channel
//
//  Created by Ryan Wall on 3/28/11.
//  Copyright 2011 Wall2Wall LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


@interface youtube_channelViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,MBProgressHUDDelegate>
{
    IBOutlet UITableView* table;
    NSMutableDictionary *images;
    NSMutableArray *youTubeContentArray;
}
@property (nonatomic,retain) IBOutlet UITableView* table;
@property (nonatomic, retain) NSDictionary *images;
@property (nonatomic,retain) NSMutableData *responseData;
@property (nonatomic,retain) NSURLConnection *connection;


@end
