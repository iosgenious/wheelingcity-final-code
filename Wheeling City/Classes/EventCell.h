//
//  EventCell.h
//  JITH
//
//  Created by Santosh on 15/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCell : UITableViewCell

@property(nonatomic,retain)IBOutlet UILabel * lblTime;
@property(nonatomic,retain)IBOutlet UILabel * lblName;
@property(nonatomic,retain)IBOutlet UILabel * lblLocation;

@end
