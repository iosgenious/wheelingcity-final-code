//
//  HistoricViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Modified by Denis Santxez 
//  Copyright 2010 W. All rights reserved.
//

#import "HistoricViewController.h"
#import "Historic.h"
#import "JITHAppDelegate.h"
#import "FirstViewController.h"
#import "Macro.h"

@interface HistoricCell : UITableViewCell
@end

@implementation HistoricCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(5,7, 60, 60)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
    
}

@end

@implementation HistoricViewController
@synthesize myHistoricInfoViewController, historicArray, historicTableView, images;

- (void)viewDidLoad {
	
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	historicArray = [[NSArray alloc ] initWithArray:dataBaseSingleton.historicArray];
	historicTableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	self.title = @"Historic";
    self.images = [[NSMutableDictionary alloc] init];
    //SlideShow Starter
    slideShowController = [[PagerViewController alloc] init];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    slideShowController.view.autoresizesSubviews = YES;
    [slideShowController setImages:[NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"624ec657f60a01b72dbdf3bb6d320b6b.png"],
                                    nil]];
    CGRect slideShowFrame=CGRectMake(0, 0, 320, 189);
    slideShowController.view.frame=slideShowFrame;
    [historicTableView setTableHeaderView:slideShowController.view];
    [historicTableView.tableHeaderView setAutoresizesSubviews:YES];
    [historicTableView.tableHeaderView setFrame:CGRectMake(0, 0, 320, 120)];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated {
	[self.navigationController setNavigationBarHidden:FALSE animated:NO];
    myHistoricInfoViewController = [[HistoricInfoViewController alloc] init];
}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();	
    return resizedImage;
}

- (void)dealloc {
	[myHistoricInfoViewController release];myHistoricInfoViewController=nil;
    [slideShowController release];slideShowController=nil;
	[historicTableView release];historicTableView=nil;
	[historicArray release];historicArray=nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Table view data source methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	//return [historicArray count];
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
	return [historicArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"Cell";
	
	HistoricCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (HistoricCell*)[[[HistoricCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	
	Historic *the_pHistoric = [historicArray objectAtIndex:indexPath.row];
	
	cell.textLabel.font = [UIFont boldSystemFontOfSize:15];// systemFontOfSize:16];
	cell.textLabel.textColor = [UIColor whiteColor];
	cell.textLabel.text = the_pHistoric.name;
	[[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
    cell.textLabel.numberOfLines=0;


    [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *image = [self.images objectForKey:the_pHistoric.image];
        
        if(!image)
        {
            // Image is not on the cached NSMUtableDictionary. Check if image is available on the documents folder and copy from there
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString *documentsFolder = [paths objectAtIndex:0];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:the_pHistoric.image]]) {
                UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pHistoric.image]];     
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pHistoric.image];            
            }
            
            // NSFileManager *the_pFileManager = [NSFileManager defaultManager];
            // NSLog(@" Full path of file to be created %@", [documentsFolder stringByAppendingPathComponent:the_pArtist.image]);
            else {
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[@"http://wheeling.yourmobicity.com/English/Historical/" stringByAppendingString:the_pHistoric.image]]];
                UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pHistoric.image];
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                //NSLog(@"the_pArtist.image is %@", the_pHistoric.image);
                
                //NSLog(@"to be copied to %@", documentsFolder);
                [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:the_pHistoric.image] atomically:YES];
                [unresizedImage release];
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[cell imageView] setImage:image];
            [cell setNeedsLayout];
        });
    });
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Navigation logic may go here ‚Äî for example, create and push another view controller.

	Historic *the_pHistoric = [historicArray objectAtIndex:indexPath.row];

    [[self navigationController] pushViewController:myHistoricInfoViewController animated:YES];
	
	//Setting title of the artist view
	myHistoricInfoViewController.title = the_pHistoric.name;
	myHistoricInfoViewController.descriptionLabel.numberOfLines = 0;
	[myHistoricInfoViewController.descriptionLabel setText:[the_pHistoric description]];
    [myHistoricInfoViewController.descriptionLabel sizeToFit];
    myHistoricInfoViewController.historicLatitude = the_pHistoric.latitude;
    myHistoricInfoViewController.historicLongitude = the_pHistoric.longitude;

	myHistoricInfoViewController.contentScroll.contentSize = CGSizeMake(319,	myHistoricInfoViewController.descriptionLabel.frame.size.height + 280);

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *documentsFolder = [paths objectAtIndex:0];
    
    UIImage *the_pHistoricImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pHistoric.image]];     
	
	myHistoricInfoViewController.historicImage.image = the_pHistoricImage;	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	previousHistoricId = currentHistoricId; 
	currentHistoricId = the_pHistoric.historicId;
	
	if (previousHistoricId != currentHistoricId){
		myHistoricInfoViewController.contentScroll.contentOffset = CGPointMake(0, 0);
		myHistoricInfoViewController.sameHistoric = FALSE;
	}
	else {
		myHistoricInfoViewController.sameHistoric = TRUE;
	}
    myHistoricInfoViewController.descriptionBubble.backgroundColor=[UIColor whiteColor];
    CGRect frame=myHistoricInfoViewController.descriptionLabel.frame;
    frame.size.width+=18;
    frame.size.height+=18;
    frame.origin.x-=9;
    frame.origin.y-=9;
    [myHistoricInfoViewController.descriptionBubble setFrame:frame];
    myHistoricInfoViewController.descriptionBubble.layer.cornerRadius=10;
}

- (void) flushCache {

    [images release];
    images = [[NSMutableDictionary alloc] init];
    
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    
    [self flushCache];
    [super didReceiveMemoryWarning];
    // Will it create a new empty infoview controller? Apparently it does
    [myHistoricInfoViewController release];
    myHistoricInfoViewController = [[HistoricInfoViewController alloc] init];
}
@end

