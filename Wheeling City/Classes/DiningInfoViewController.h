//
//  DiningInfoViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DataBaseSingleton.h"
#import "PagerViewController.h"
#import "DiningWebsiteViewController.h"


@interface DiningInfoViewController : UIViewController <UIActionSheetDelegate,CLLocationManagerDelegate>{

	DataBaseSingleton *dataBaseSingleton;
    UILabel *descriptionLabel;
    UIImageView *descriptionBubble;
    UIImageView *diningImage;
    UIScrollView *contentScroll;
    DiningWebsiteViewController *myDiningWebsiteViewController;
    NSString *diningUrl;
    NSString *diningYoutube;
    NSString *diningCall;
	BOOL sameDining;
    UIBarButtonItem *optionsButton;
    UIImageView *backgroundImage;
    NSString *diningLongitude;
    NSString *diningLatitude;
}

@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UIImageView *descriptionBubble;
@property (nonatomic, retain) IBOutlet UIImageView *diningImage;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScroll;
@property (nonatomic, assign) BOOL sameDining;
@property (nonatomic, retain) IBOutlet DiningWebsiteViewController *myDiningWebsiteViewController;
@property (nonatomic, retain) NSString *diningUrl;
@property (nonatomic, retain) NSString *diningYoutube;
@property (nonatomic, retain) NSString *diningCall;
@property (nonatomic, retain) NSString *diningLongitude;
@property (nonatomic, retain) NSString *diningLatitude;
@property (nonatomic, retain) UIBarButtonItem *optionsButton;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImage;

- (void)callDining;
- (void)bringDiningWebsiteView;
- (void)bringDiningVideoView;
- (void)bringGoogleMaps;
@end
