//
//  UIPickerViewAppDelegate.h
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 11/04/11.
//  Copyright 2011 W. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>



@class SiteMapViewController;



@interface UIPickerViewAppDelegate : NSObject <UIApplicationDelegate>

{
    
    SiteMapViewController *mviewController;
    
    UIWindow *window;
    
}



@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) SiteMapViewController *mviewController;





@end
