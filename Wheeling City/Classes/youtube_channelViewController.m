//
//  youtube_channelViewController.m
//  youtube channel
//
//  Created by Denis Santxez on 3/28/11.
//  Modified by Urko Fernandez
//  Copyright 2011 Wall2Wall LLC. All rights reserved.
//

#import "youtube_channelViewController.h"
#import <CommonCrypto/CommonDigest.h>
#import "Macro.h"
#import "YouTubeModel.h"
#import "YouTubePlayerController.h"

@interface YouTubeCell : UITableViewCell
@end

@implementation YouTubeCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(4,7, 50, 50)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
    [self.textLabel setFrame:CGRectMake(60, 18, 210, 30)];
}

@end

@implementation youtube_channelViewController

@synthesize table;
@synthesize images;


BOOL gotVideos;

UIActivityIndicatorView *spinner;
MBProgressHUD *mbSpinner;


void alertWithMessage (NSString *errorTitle, NSString *message){
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:errorTitle message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertView show];
    [alertView release];
    
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Spotlight";
    self.images = [[NSMutableDictionary alloc] init];
    youTubeContentArray=[[NSMutableArray alloc] init];
    [self fetchYouTubeData];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self hudWasHidden];
}
- (void)dealloc
{
    [youTubeContentArray release];
     [super dealloc];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	[self.navigationController setNavigationBarHidden:FALSE animated:YES];
}

- (void) flushCache {
    for (id theKey in images)
        [images removeObjectForKey:theKey];
}

- (void)didReceiveMemoryWarning
{
    [self flushCache];
    [super didReceiveMemoryWarning];
}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}


/**
 Calculate a string's MD5 checksum
 \param text String to calculate md5 sum, usually is a URL
 */

- (NSString *) md5:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mak TableView delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	YouTubeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (YouTubeCell*)[[[YouTubeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15];
        cell.textLabel.textColor = [UIColor whiteColor];
        
        cell.detailTextLabel.textColor = [UIColor yellowColor];
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
	}
    
        YouTubeModel * yContent=[youTubeContentArray objectAtIndex:indexPath.row];
        cell.textLabel.text= [yContent title];
        
        UIView* backgroundView = [ [ [ UIView alloc ] initWithFrame:CGRectZero ] autorelease ];
        if(indexPath.row % 2 ==0)
            backgroundView.backgroundColor = [UIColor colorWithRed:0.19 green:0.5 blue:0.81 alpha:0.5];
        else
            backgroundView.backgroundColor = [UIColor colorWithRed:0.18 green:0.15 blue:1.0 alpha:0.5];
        cell.backgroundView = backgroundView;
        for ( UIView* view in cell.contentView.subviews )
        {
            view.backgroundColor = [ UIColor clearColor ];
        }
        
        [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSString *imageName = [self md5:[yContent imageUrl]];
            UIImage *image = [self.images objectForKey:imageName];
            
            if(!image)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                
                NSString *documentsFolder = [paths objectAtIndex:0];
                
                if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:imageName]]) {
                    UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:imageName]];
                    image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                    [self.images setValue:image forKey:imageName];
                }
                else {
                    // if we didn't find an image, create a placeholder image and
                    // put it in the "cache". Start the download of the actual image
                    //            image = [UIImage imageNamed:@"Placeholder.png"];
                    NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[yContent imageUrl]]];
                    UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
                    image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                    
                    [self.images setValue:image forKey:imageName];
                    NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                    [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:imageName] atomically:YES];
                    [unresizedImage release];
                }
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                [[cell imageView] setImage:image];
                [cell setNeedsLayout];
            });
        });
        table.hidden=false;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [youTubeContentArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    YouTubeModel * yContent=[youTubeContentArray objectAtIndex:indexPath.row];

    YouTubePlayerController * yPlayer=[[YouTubePlayerController alloc] initWithNibName:@"YouTubePlayerController" bundle:nil];
    [yPlayer setWeburl:[yContent videoUrl]];
    [yPlayer setToptitle:[yContent title]];
    [self.navigationController pushViewController:yPlayer animated:YES];
    [yPlayer release];
}

- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [mbSpinner removeFromSuperview];
    [mbSpinner release];
	mbSpinner = nil;
}



#pragma mark - YouTube Video Implementation Method

-(void)fetchYouTubeData{
    
    mbSpinner=[[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:mbSpinner];
    mbSpinner.delegate=self;
    mbSpinner.labelText=@"Updating";
    [mbSpinner show:true];

    NSMutableURLRequest *urlRequest=[[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:YouTube_URL]];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept-Type"];
    [urlRequest setHTTPMethod:@"GET"];
    assert(urlRequest!=nil);
    self.connection=[NSURLConnection connectionWithRequest:urlRequest delegate:self];
    assert(self.connection!=nil);
    [urlRequest release];

}



#pragma mark---------------- NSURLConnection delegate methods-------------

- (NSURLRequest *)connection:(NSURLConnection *)connection
 			 willSendRequest:(NSURLRequest *)request
 			redirectResponse:(NSURLResponse *)redirectResponse {
	return request;
}


- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int code = [httpResponse statusCode];
    //NSLog(@"the response code is %d",code);
    if (code >=200 && code <=299) {
        self.responseData = [NSMutableData data];
    }
    else if (code >=400 && code <=499) {
        [theConnection cancel];
        self.connection = nil;
       
    }
    else {
        [theConnection cancel];
        self.connection = nil;
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
	
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error {
    assert(theConnection == self.connection);
    [self.connection cancel];
    [mbSpinner hide:true];
    NSString *message = [NSString stringWithFormat:@"Connection is terminated because %@",[error localizedDescription]];
    alertWithMessage(@"Error!", message);

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
        NSMutableDictionary *responseDict =[[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:NULL]];
    
    
    if (![responseDict isEqual:[NSNull null]])
    {
        NSMutableArray *tempDataArry  = [[responseDict valueForKey:@"feed"] valueForKey:@"entry"];
        for (int i = 0;  i < [tempDataArry count]; i++) {
            [youTubeContentArray  addObject:[YouTubeModel youTubeTyPeWithParameter:[tempDataArry objectAtIndex:i]]];
        }
        gotVideos=true;
        [table reloadData];
        [mbSpinner hide:true];
        
    }
    else
    {
       alertWithMessage(@"", @"No youtube video found");
    }
        [responseDict release];
    
}

@end
