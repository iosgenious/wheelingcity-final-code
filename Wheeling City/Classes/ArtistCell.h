//
//  ArtistCell.h
//  JITH
//
//  Created by Urko Fernandez on 3/24/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ArtistCell : UITableViewCell {
	UILabel *primaryLabel;
	UILabel *secondaryLabel;
	UIImageView *myImageView;
}

@property(nonatomic,retain)UILabel *primaryLabel;
@property(nonatomic,retain)UILabel *secondaryLabel;
@property(nonatomic,retain)UIImageView *myImageView;

@end
