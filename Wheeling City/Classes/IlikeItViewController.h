//
//  IlikeItViewController.h
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 23/03/11.
//  Copyright 2011 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface IlikeItViewController : UIViewController<UIWebViewDelegate,MBProgressHUDDelegate>
{
    IBOutlet UITextView* infoTextView;
    UIActivityIndicatorView* spinner;
    MBProgressHUD *mbSpinner;
}

@property (retain)     NSMutableData* responseData;
@property(nonatomic,retain)NSURLConnection *connection;

@property (nonatomic, retain)IBOutlet UIWebView* facebookLike;

/**
 It did a request to obtain the facebook I like Button of JITH
 */
- (void)showLikeButton ;
-(void) likeTheFanPage;

@end
