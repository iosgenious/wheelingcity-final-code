//
//  Dining.h
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//  Modified by Denis Santxez
//


@interface Dining : NSObject {
	NSInteger diningId;
	NSString *name;
	NSString *description;	
    NSString *location;
    NSString *phone;
    NSString *image;
    NSString *website;    
    NSString *video;
    NSString *longitude;
    NSString *latitude;

}
  
@property (nonatomic, assign) NSInteger diningId;
@property (nonatomic, retain) NSString *name, *description, *location, *phone, *image, *website, *video, *longitude, *latitude; 

- (id) initWithName:(NSInteger)in_DiningId name:(NSString *)in_Name description:(NSString *)in_Description 
			  location:(NSString *)in_Location phone:(NSString *)in_Phone image:(NSString *)in_Image
            website:(NSString *)in_Website video:(NSString *)in_Video longitude:(NSString *)in_Longitude latitude:(NSString *)in_Latitude; 

@end