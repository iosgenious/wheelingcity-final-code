//
//  InfoViewController.h
//  JITH
//
//  Created by Urko Fernandez on 6/3/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InfoViewControllerDelegate;

@interface InfoViewController : UIViewController <UIWebViewDelegate> {
    UIWebView *infoWeb;

}
@property (nonatomic, retain) IBOutlet UIWebView *infoWeb;
@property (nonatomic, assign) id <InfoViewControllerDelegate> delegate;

- (void)copyFromBundle;
- (IBAction)done:(id)sender;

@end

@protocol InfoViewControllerDelegate

- (void)infoViewControllerDidFinish:(InfoViewController *)controller;

@end
