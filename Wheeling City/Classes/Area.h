//
//  Area.h
//  JITH
//
//  Created by Urko Fernandez on 4/13/11.
//  Copyright 2011 W. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Area : NSObject {
	NSInteger areaId;
	NSString *name;
	NSString *description;	
    NSString *image;  
}

@property (nonatomic, assign) NSInteger areaId;
@property (nonatomic, retain) NSString *name, *description, *image;
- (id) initWithName:(NSInteger)in_AreaId name:(NSString *)in_Name description:(NSString *)in_Description 
              image:(NSString *)in_Image;
@end
