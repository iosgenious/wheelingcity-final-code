//
//  Attraction.h
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//


@interface Attraction : NSObject {
	NSInteger attractionId;
	NSString *name;
	NSString *description;	
    NSString *category;
    NSString *address;
    NSString *phone;
    NSString *image;
    NSString *video;
    NSString *website;
    NSString *longitude;
    NSString *latitude;

}
  
@property (nonatomic, assign) NSInteger attractionId;
@property (nonatomic, retain) NSString *name, *description, *category, *address, *phone, *image, *video, *website, *longitude, *latitude; 

- (id) initWithName:(NSInteger)in_AttractionId name:(NSString *)in_Name description:(NSString *)in_Description 
			  category:(NSString *)in_Category address:(NSString *)in_Address phone:(NSString *)in_Phone image:(NSString *)in_Image
              video:(NSString *)in_Video website:(NSString *)in_Website longitude:(NSString *)in_longitude latitude:(NSString *)in_latitude; 

@end