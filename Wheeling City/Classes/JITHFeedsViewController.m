//
//  JITHFeeds.m
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 09/03/11.
//  Copyright 2011 W. All rights reserved.
//

#import "JITHFeedsViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Macro.h"
#import "DetailViewController.h"
#import "JITHAppDelegate.h"

@implementation JITHFeedsViewController

/**
 Constant variables to appear the keyboard in the possition that i wanted, without quiting the comments
 */
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@synthesize selectedCell,selectedId,selectedLikes, totalLikes, responseData,list,token;

//////////////////////////////////////////////////////////////////////////////////////////////
// UIViewController
//////////////////////////////////////////////////////////////////////////////////////////////

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


#pragma mark- MBProgress Method

-(void)addActivity{
    [self removeHUD];
    mbSpinner=[[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:mbSpinner];
    mbSpinner.delegate=self;
    mbSpinner.labelText=@"Updating";
    [mbSpinner show:true];

}
- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [mbSpinner removeFromSuperview];
    [mbSpinner release];
	mbSpinner = nil;
}

-(void)removeHUD{
    //    [spinner stopAnimating];
    [mbSpinner hide:true];
    [spinner release];
}
#pragma mark FaceBook Intraction Method

-(void)getFeedInfo{
    [self addActivity];
    
    //nsurlrequest
    NSString *urlString =[NSString stringWithFormat:@"https://graph.facebook.com/%@%@%@%@",selectedId,@"/comments?access_token=",token,@"&limit=100"];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [NSURLConnection connectionWithRequest:request delegate:self ];
}
#pragma mark- View Life Cycle Method

/**
 It an important method, we load all the information in the view. First we put a correct name to the view. Then when the nsurl request is starting the spining starts.We ask for all the comments for a selected post. This selected post is selected previously. The we print in the view the all post(afeter there is not posted de all post, only a part of the message) i put all the message and the number of likes of this post. finaly we decided the height and the weight of the text and the info of the view.
 */
- (void)viewDidLoad
{
    arryData=[[NSMutableArray alloc]init];
    commentsData=[[NSMutableArray alloc]init];
    namesComments=[[NSMutableArray alloc]init];
    namesIds=[[NSMutableArray alloc]init];
    //Set the title of the navigation bar
    self.navigationItem.title = @"Facebook Comments";
    responseData=[[NSMutableData alloc]init];
    list=[[ NSMutableArray alloc]init];

    [self getFeedInfo];
    //manage the data to put in the text view
    NSString *intString = [NSString stringWithFormat:@"%@ Likes it",totalLikes];
    NSString *commentString = [NSString stringWithFormat:@"%@\r%@",selectedCell,intString];
    lblText.text=commentString;
    
    //change the size if we want
    lblText.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(16)];
    lblText.textColor=[UIColor yellowColor];
    CGRect frame = lblText.frame;
    frame.size.height = lblText.contentSize.height;
    lblText.frame = frame;

    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [super viewDidUnload];

}

/**
 in dealloc we release differents objetcs
 */
- (void)dealloc
{
    
    //the IBOutlets are released
    
    [lblText release];
    [postCommentsTable release];
    [addCommentText release];
    
    
    //delete the structures to manage data
    [responseData release];
    [list release];
    [namesComments release];
    [namesIds release];
    [arryData release];
    [commentsData release];
    
    [super dealloc];
     

    
}


#pragma mark - Facebook Method


- (void) performPublishAction:(void (^)(void)) action {
    // we defer request for permission to post to the moment of post, then we check for the permission
    
    if ([FBSession.activeSession.permissions indexOfObject:@"publish_stream"] == NSNotFound) {
        // if we don't already have the permission, then we request it now
        [FBSession.activeSession requestNewPublishPermissions:@[@"publish_stream"]
                                              defaultAudience:FBSessionDefaultAudienceFriends
                                            completionHandler:^(FBSession *session, NSError *error) {
                                                if (!error) {
                                                    action();
                                                } else if (error.fberrorCategory != FBErrorCategoryUserCancelled){
                                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Permission denied" message:@"Unable to get permission to post" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                    [alertView show];
                                                    [alertView release];
                                                }
                                                
                                            }];
    } else {
        action();
    }
}


-(IBAction)addCommentInJITHSomeComment:(id)sender{
    NSString *comentario=addCommentText.text;
    if ([comentario length]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please enter your comment" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        return;
    }
    [addCommentText resignFirstResponder];
    [self addActivity];

    NSString * gPath=[NSString stringWithFormat:@"%@/comments",selectedId];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   comentario,@"message",FBSession.activeSession.accessTokenData.accessToken,@"access_token",
                                   nil];
    
    [self performPublishAction:^{
        FBRequestConnection *connection = [[FBRequestConnection alloc] init];
        connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession
        | FBRequestConnectionErrorBehaviorAlertUser
        | FBRequestConnectionErrorBehaviorRetry;
        
        [connection addRequest:[FBRequest requestWithGraphPath:gPath parameters:params HTTPMethod:@"POST"]
             completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    [self removeHUD];
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"WheelingCity" message:@"Your photo posted successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alertView show];
                     [alertView release];
                     
                 }else{
                     [self removeHUD];
                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to post the image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alertView show];
                     [alertView release];
                 }
             }];
        [connection start];
        
    }];
    [addCommentText setText:@""];
}
/**
 With this method we set the lenght of the response received for de NSURLRequest
 */

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response {
    [responseData setLength:0];
}
/**
 With this method save the response received for de NSURLRequest
 */

- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data {
    [responseData appendData:data];
}
/**
 With this method we know is the data of the response received for de NSURLRequest has any error
 */
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error {
    NSLog(@"Connection failed: %@", [error description]);
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSMutableDictionary *dictionary =[[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:NULL]];

       NSMutableArray *arr = [dictionary mutableArrayValueForKey:@"data"];
    [arryData removeAllObjects];
    [commentsData removeAllObjects];
    [namesComments removeAllObjects];
    [namesIds removeAllObjects];
    
    int len = [arr count];
    for(int i=len-1; i>=0;i--){
        NSDictionary *d = [arr objectAtIndex:i];
        [list addObject:d];
    }
    
    int indice=0;

    for (int i=len-1;i>=0;i--){
        
        NSDictionary *message=[arr objectAtIndex:i];
        NSDictionary *user = [message objectForKey:@"from"];
        NSMutableString *facebookNames=[user objectForKey:@"name"];
        NSMutableString *facebookIdent=[user objectForKey:@"id"];
        [namesComments insertObject:facebookNames atIndex:indice];
        [namesIds insertObject:facebookIdent atIndex:indice];
        
        indice++;
        
    }
    
    NSUInteger index = 0;
    NSUInteger internal=0;
    for ( id key in list) 
    {
        NSMutableString *message= [[list objectAtIndex:index] objectForKey:@"message"];
        NSMutableString *createdTime= [[list objectAtIndex:index] objectForKey:@"created_time"];
        if(message!=nil){
            
            
            NSString *fbDate = [createdTime stringByReplacingOccurrencesOfString:@"T" withString:@" "];
            fbDate = [fbDate stringByReplacingOccurrencesOfString:@"+" withString:@" +"];
            NSDate *date = [[NSDate alloc] initWithString:fbDate];
            
            [commentsData insertObject:date atIndex:internal];
            [arryData insertObject:message atIndex:internal];
            index++;
            internal++;
            [date release];
        }
        else
        {index++;
            
        }
    }
    NSUInteger val=[arryData count];
   if (val == 0)
   {
       UIAlertView *alert = [[UIAlertView alloc] 
                             initWithTitle:@"Wheeling City App" 
                             message:@"There are no comments in this publication" 
                             delegate:nil 
                             cancelButtonTitle:@"OK" 
                             otherButtonTitles:nil];
       [alert show];
       [alert release];
   }
    
    [postCommentsTable reloadData];
    [self removeHUD];
    
}


#pragma mark Table view methods

/**
 This method return the number of section in one tableview, in our case we only have one sections, beause of that the function return 1
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

/**
 Customize the number of rows in the table view.
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arryData count];
}

/**
Customize the appearance of table view cells. We decided here the type of the letter and the font for the table view
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    static NSString *SimpleTableIdentifier = @"SimpleTableIdentifier";
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {        
        cell = [[[UITableViewCell alloc] 
                 initWithStyle:UITableViewCellStyleDefault 
                 reuseIdentifier:SimpleTableIdentifier] autorelease];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:14.0f];
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
        [[cell detailTextLabel] setLineBreakMode:UILineBreakModeWordWrap];
        //to decide how much lines for cell, if it is 0 is the number of lines that it could
        cell.textLabel.numberOfLines=0;
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;

        UIImageView *imageTableView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccDisclosure.png"]];
        cell.accessoryView = imageTableView;
        [imageTableView release];

    }
	cell.textLabel.text = [arryData objectAtIndex:indexPath.row];
       return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    DetailViewController *dvController = [[DetailViewController alloc]initWithNibName:@"DetailViewController" bundle:[NSBundle mainBundle]];
    
    dvController.comments = [arryData objectAtIndex:indexPath.row];
    dvController.names = [namesComments objectAtIndex:indexPath.row];
    dvController.ids=[namesIds objectAtIndex:indexPath.row];
    dvController.commentData=[commentsData objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:dvController animated:YES];
    [dvController release];
    dvController = nil;
    
}
/**
 With this method we know with cell is pushed for the user and we van do different thigs. This method return de index of the cell pushed
 */
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    [self tableView:postCommentsTable didSelectRowAtIndexPath:indexPath];
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView new] autorelease];
}

/**
keyboard and the unitextfield, the management between boths, is to have the keyboard in a good position to see some messages and to write the message in te uitextfield.
 */

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    postCommentsTable.allowsSelection=NO;
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    postCommentsTable.allowsSelection=YES;

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
 

@end