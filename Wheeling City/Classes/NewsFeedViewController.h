//
//  NewsFeedViewController.h
//  JITH
//
//  Created by Santosh Singh on 08/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedViewController : UIViewController

-(IBAction)commentBtnAction:(id)sender;
-(IBAction)loginOutBtnAction:(id)sender;
-(IBAction)likeBtnAction:(id)sender;
-(IBAction)cameraBtnAction:(id)sender;


-(void)shareOnFB:(NSString*)titleContent;

@end
