//
//  ResourceInfoViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DataBaseSingleton.h"
#import "PagerViewController.h"
#import "ResourceWebsiteViewController.h"


@interface ResourceInfoViewController : UIViewController <UIActionSheetDelegate,CLLocationManagerDelegate> {

	DataBaseSingleton *dataBaseSingleton;
    UILabel *descriptionLabel;
    UIImageView *descriptionBubble;
    UIImageView *resourceImage;
    UIScrollView *contentScroll;
    ResourceWebsiteViewController *myResourceWebsiteViewController;
	BOOL sameResource;
    UIBarButtonItem *optionsButton;
    NSString *resourceCall;
    NSString *resourceURL;
    NSString *resourceYoutube;
    NSString *resourceLatitude;
    NSString *resourceLongitude;


}

@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UIImageView *descriptionBubble;
@property (nonatomic, retain) IBOutlet UIImageView *resourceImage;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScroll;
@property (nonatomic, assign) BOOL sameResource;
@property (nonatomic, retain) ResourceWebsiteViewController *myResourceWebsiteViewController;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *optionsButton;
@property (nonatomic, retain) NSString *resourceCall;
@property (nonatomic, retain) NSString *resourceURL;
@property (nonatomic, retain) NSString *resourceYoutube;
@property (nonatomic, retain) NSString *resourceLatitude;
@property (nonatomic, retain) NSString *resourceLongitude;

- (void)callResource;
- (void)bringResourceWebsiteView;
- (void)bringResourceVideoView;
- (void)bringGoogleMaps;
@end
