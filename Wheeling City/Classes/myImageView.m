    //
//  myImageView.m
//  JITH
//
//  Created by Urko Fernandez on 6/7/10.
//  Copyright 2010 W. All rights reserved.
//

#import "myImageView.h"
#import "JITHAppDelegate.h"


@implementation myImageView


-(void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event
{
	UITouch *touch = [[touches allObjects] objectAtIndex:0];
	JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
	mySiteMapViewController = [the_pAppDelegate.myNavController.viewControllers objectAtIndex:1];
	[mySiteMapViewController.navigationController setNavigationBarHidden:FALSE animated:YES];
	switch ([touch tapCount]) {
		case 2: 
				[mySiteMapViewController zoomInOrOut];
			break;
	}
}


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/



- (void)dealloc {
	//TODO:
	//[mySiteMapViewController release];
    [super dealloc];
}


@end
