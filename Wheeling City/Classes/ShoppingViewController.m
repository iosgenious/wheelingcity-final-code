//
//  ShoppingViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Modified by Denis Santxez 
//  Copyright 2010 W. All rights reserved.
//

#import "ShoppingViewController.h"
#import "Shopping.h"
#import "Area.h"
#import "JITHAppDelegate.h"
#import "FirstViewController.h"
#import "Macro.h"

@interface ShoppingCell : UITableViewCell
@end

@implementation ShoppingCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(5,7, 60, 60)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
}

@end


@implementation ShoppingViewController
@synthesize myStoreViewController, shoppingDict, shoppingTableView, images, areaArray;



- (void)viewDidLoad {
	
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO animated:YES];

	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	shoppingDict = [[NSDictionary alloc ] initWithDictionary:dataBaseSingleton.shoppingDict];
    areaArray = [[NSArray alloc ] initWithArray:dataBaseSingleton.areaArray];
	shoppingTableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	self.title = @"Shopping Areas";
    self.images = [[NSMutableDictionary alloc] init];
    slideShowController = [[PagerViewController alloc] init];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    slideShowController.view.autoresizesSubviews = YES;
    [slideShowController setImages:[NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"Tammy Stein Wheeling Pictures 029.png"],
                                    nil]];
    [slideShowController.view setBackgroundColor:[UIColor redColor]];
    CGRect slideShowFrame=CGRectMake(0, 0, 320, 189);
    slideShowController.view.frame=slideShowFrame;
    [shoppingTableView setTableHeaderView:slideShowController.view];
    shoppingTableView.tableHeaderView.autoresizesSubviews = YES;
    [shoppingTableView.tableHeaderView setFrame:CGRectMake(0, 0, 320, 120)];
    
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();	
    return resizedImage;
}

- (void)dealloc {
	[myStoreViewController release];myStoreViewController=nil;
    [slideShowController release];slideShowController=nil;
	[shoppingTableView release];shoppingTableView=nil;
	[shoppingDict release];shoppingDict=nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Table view data source methods

- (NSString *)getKeyFromRow:(NSInteger)row {
    return [areaArray objectAtIndex:row];
}

- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
    return [areaArray count];    
}


- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"Cell";
	
	ShoppingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (ShoppingCell*)[[[ShoppingCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
	}
	Area *the_pArea = [areaArray objectAtIndex:indexPath.row];
	cell.textLabel.font = [UIFont boldSystemFontOfSize:15];// systemFontOfSize:16];
	cell.textLabel.textColor = [UIColor whiteColor];
	cell.textLabel.text =  the_pArea.name;
	cell.detailTextLabel.textColor = [UIColor yellowColor];
	[[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
	[[cell detailTextLabel] setLineBreakMode:UILineBreakModeWordWrap];
    
    //Temporary solution
    

    if ([the_pArea.image isEqualToString:@"Shopping.png"]) {
        [[cell imageView] setImage:[UIImage imageNamed:@"Shopping.png"]];
        
    }
    else {
        [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
    }  
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        
        
         UIImage *image = [self.images objectForKey:the_pArea.image];
         if(!image) // && [the_pArea.image isEqualToString:@"Shopping.png"])
        {
            // Image is not on the cached NSMUtableDictionary. Check if image is available on the documents folder and copy from there
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString *documentsFolder = [paths objectAtIndex:0];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:the_pArea.image]]) {
                UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pArea.image]];     
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pArea.image];            
            }
            
            // NSFileManager *the_pFileManager = [NSFileManager defaultManager];
            // NSLog(@" Full path of file to be created %@", [documentsFolder stringByAppendingPathComponent:the_pArtist.image]);
            else {
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[@"http://wheeling.yourmobicity.com/English/Area/" stringByAppendingString:the_pArea.image]]];
                UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pArea.image];
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                NSLog(@"the_pArea.image is %@", the_pArea.image);
                
                NSLog(@"to be copied to %@", documentsFolder);
                [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:the_pArea.image] atomically:YES];
                [unresizedImage release];
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[cell imageView] setImage:image];
            [cell setNeedsLayout];
        });
    });
         
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Navigation logic may go here ‚Äî for example, create and push another view controller.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Area *the_pArea = [areaArray objectAtIndex:indexPath.row];
    if (myStoreViewController) {
    [myStoreViewController release];
    }
    myStoreViewController = [[StoreViewController alloc] init];
	myStoreViewController.areaImage = the_pArea.image;	
    

    UITableViewCell *cell = [shoppingTableView cellForRowAtIndexPath:indexPath];
    myStoreViewController.shoppingArray = [shoppingDict objectForKey:cell.textLabel.text];
    
    //Setting title of the artist view
	myStoreViewController.title = the_pArea.name;
    myStoreViewController.descriptionArea=the_pArea.description;
	
	previousAreaId = currentAreaId; 
	currentAreaId = the_pArea.areaId;
    
    //send image
    [[self navigationController] pushViewController:myStoreViewController animated:YES];

    
}



- (void) flushCache {

    [images release];
    images = [[NSMutableDictionary alloc] init];
    
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    
    [self flushCache];
    [super didReceiveMemoryWarning];
    // Will it create a new empty infoview controller? Apparently it does
    [myStoreViewController release];
    myStoreViewController = [[StoreViewController alloc] init];
}
@end

