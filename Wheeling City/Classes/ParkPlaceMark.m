#import "ParkPlaceMark.h"

@implementation ParkPlaceMark

@synthesize coordinate, mTitle, mSubTitle;

- (NSString *)subtitle{
    return mSubTitle;
}

- (NSString *)title{
    return mTitle;
}


-(id)initWithCoordinate:(CLLocationCoordinate2D) c{
	coordinate=c;
	NSLog(@"%f,%f",c.latitude,c.longitude);
	return self;
}
@end
