//
//  DiningViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "DiningInfoViewController.h"

@interface DiningViewController : UIViewController 
<UITableViewDelegate, UITableViewDataSource>  
{
	DataBaseSingleton *dataBaseSingleton;
    DiningInfoViewController *myDiningInfoViewController;
	IBOutlet UITableView *diningTableView;
	NSDictionary *diningDict;
	int previousDiningId;
	int currentDiningId;
    NSMutableDictionary *images;
    PagerViewController* slideShowController;
    UIBarButtonItem *flipButton;
}

@property (nonatomic, retain) DiningInfoViewController *myDiningInfoViewController;
@property (nonatomic, retain) NSDictionary *diningDict;
@property (nonatomic, retain) IBOutlet UITableView *diningTableView;
@property (nonatomic, retain) NSDictionary *images;

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height;

@end
