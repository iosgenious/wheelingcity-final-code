//
//  WCEvent.h
//  JITH
//
//  Created by Santosh Singh on 14/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WCEvent : NSObject

@property(nonatomic,retain)NSString * eventTitle;
@property(nonatomic,retain)NSString * eventDescription;
@property(nonatomic,retain)NSString * eventTime;
@property(nonatomic,retain)NSDate * eventStartTime;
@property(nonatomic,retain)NSDate * eventFinishTime;
@property(nonatomic,retain)NSString * eventLocation;


@end

