//
//  AttractionInfoViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import "AttractionInfoViewController.h"
#import "NetworkReachability.h"
#import "Macro.h"

@implementation AttractionInfoViewController

@synthesize descriptionLabel, attractionImage, contentScroll, sameAttraction, actionButton;
@synthesize attractionURL,attractionYoutube, attractionCall, attractionLatitude, attractionLongitude;
@synthesize myAttractionWebsiteViewController,bubble;

//MKMapView *map;
CLLocationManager *GPSlocation;
UIWebView  *webview;
double userLongitude;
double userLatitude;


- (void)viewDidLoad {
    [super viewDidLoad];
    //map = [[MKMapView alloc]init];
    //map.showsUserLocation=YES;
    //map.delegate=self;
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	contentScroll.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	descriptionLabel.userInteractionEnabled = TRUE;
    actionButton = [[UIBarButtonItem alloc] initWithTitle:@"Options" style:UIBarButtonItemStylePlain target:self action:@selector(showCategories:)];
    [[self navigationItem] setRightBarButtonItem:actionButton];
    [actionButton release];
}

-(void) viewDidAppear:(BOOL)animated {
	UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
    /*
     if ([attractionCall length] != 0) {
     actionButton = [[UIBarButtonItem alloc] initWithTitle:@"Call" style:UIBarButtonItemStylePlain target:self action:@selector(showCategories:)];
     [[self navigationItem] setRightBarButtonItem:actionButton];
     [actionButton release];
     }
     
     else if (actionButton) {
     self.navigationItem.rightBarButtonItem = nil;
     }
     */
}

-(void) viewDidDisappear:(BOOL)animated {
    [self release];
}

- (void)showCategories:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call", @"Website", @"Video", @"Get me there!", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
        [self callAttraction];
    }
    else if (buttonIndex == 1) {
        [self bringAttractoinWebsiteView];
    }
    else if (buttonIndex == 2) {
        [self bringAttractionVideoView];
	}
    else if (buttonIndex == 3) {
        // ToDo: GoogleMaps
        [self bringGoogleMaps];
    }
}

-(void)bringGoogleMaps
{
    if(attractionLatitude!=nil || attractionLongitude!=nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Go to that location" message:@"This will open the Maps App" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No location" message:@"There is no location information" delegate:self
                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

#pragma mark - CLLocation Manager Delegate Method

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    userLatitude=newLocation.coordinate.latitude;
    userLongitude=newLocation.coordinate.longitude;
    [GPSlocation stopUpdatingLocation];
    [GPSlocation release];
    [self openMap:[NSString stringWithFormat:@"saddr=%f,%f&daddr=%@,%@&output=dragdir",userLatitude,userLongitude,attractionLatitude,attractionLongitude]];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex!=0)
    {
        GPSlocation=[[CLLocationManager alloc]init];
        GPSlocation.delegate=self;
        [GPSlocation startUpdatingLocation];
    }
}

- (void)callAttraction {
    if (!attractionCall){
        UIAlertView *errorView;
        errorView = [[UIAlertView alloc]
					 initWithTitle: @"No phone number"
					 message: @"Contact number not available"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
        
    }
    else{
        
        if(webview==nil)
            webview = [[UIWebView alloc] initWithFrame:self.view.frame];
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:attractionCall]]]];
    }
}
- (void)checkNetwork{
	
	UIAlertView *errorView;
	NetworkReachability* internetReach;
	internetReach = [[NetworkReachability reachabilityForInternetConnection] retain];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	[internetReach release];
	if (netStatus == NotReachable) {
		// Could be ReachableViaWWAN and ReachableViaWiFi
		
		//NSLog(@"NotReachable");
		
		errorView = [[UIAlertView alloc]
					 initWithTitle: @"Network Error"
					 message: @"There is no internet conection!"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
        return;
	}
}

- (void)bringAttractoinWebsiteView {
	
    if (!attractionURL){
        [self checkNetwork];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        av.frame=CGRectMake(145, 230, 25, 25);
        av.tag  = 1;
        myAttractionWebsiteViewController = [[AttractionWebsiteViewController alloc] init];
        [myAttractionWebsiteViewController.view addSubview:av];
        [av startAnimating];
        [[self navigationController] pushViewController:myAttractionWebsiteViewController animated:YES];
        
        NSURL *url = [NSURL URLWithString:@"http://www.wheelingcvb.com/"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [myAttractionWebsiteViewController.attractionWeb loadRequest:request];
    }
    else{
        [self checkNetwork];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        av.frame=CGRectMake(145, 230, 25, 25);
        av.tag  = 1;
        myAttractionWebsiteViewController = [[AttractionWebsiteViewController alloc] init];
        [myAttractionWebsiteViewController.view addSubview:av];
        [av startAnimating];
        [[self navigationController] pushViewController:myAttractionWebsiteViewController animated:YES];
        
        NSURL *url = [NSURL URLWithString:attractionURL];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [myAttractionWebsiteViewController.attractionWeb loadRequest:request];
    }
	
}

- (void)bringAttractionVideoView {
    
    if (![attractionYoutube length])
    {
        UIAlertView *errorView;
        errorView = [[UIAlertView alloc]
					 initWithTitle: @"No Video"
					 message: @"There is no promotional video available"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
        
    }
    else{
        [self checkNetwork];
        YouTubePlayerController * yPlayer=[[YouTubePlayerController alloc] initWithNibName:@"YouTubePlayerController" bundle:nil];
        [yPlayer setWeburl:attractionYoutube];
        [self.navigationController pushViewController:yPlayer animated:YES];
        [yPlayer release];
        
    }
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    //[map release];
    [super didReceiveMemoryWarning];
	// Release any cached data, images, etc that aren't in use.
}

- (void)dealloc {
	[descriptionLabel release];self.descriptionLabel=nil;
	[attractionImage release];self.attractionImage=nil;
    [webview release];webview=nil;
	[contentScroll release];self.contentScroll=nil;
	//[super dealloc];
}


#pragma mark - Map Method

-(void)openMap:(NSString*)urlString{
    NSString *url;
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
    {
        url = [NSString stringWithFormat:@"http://maps.google.com/maps?%@",urlString];
    }
    else
    {
        url = [NSString stringWithFormat:@"http://maps.apple.com/maps?%@",urlString];
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}


@end

