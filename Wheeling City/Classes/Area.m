//
//  Area.m
//  JITH
//
//  Created by Urko Fernandez on 4/13/11.
//  Copyright 2011 W. All rights reserved.
//

#import "Area.h"


@implementation Area

@synthesize areaId, name, description, image;

- (id) initWithName:(NSInteger)in_AreaId name:(NSString *)in_Name description:(NSString *)in_Description 
           image:(NSString *)in_Image {
	
	self = [super init];
	if(self)
	{
		self.areaId = in_AreaId;
		self.name = in_Name;
		self.description = in_Description;
		self.image = in_Image;
	}
	return self;
}


- (void) dealloc {
	
	[name release];
	[image release];
	[description release];
	[super dealloc];
}

@end