//
//  LodgingViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import "LodgingInfoViewController.h"

@interface LodgingViewController : UIViewController 
<UITableViewDelegate, UITableViewDataSource>  
{
	DataBaseSingleton *dataBaseSingleton;
    LodgingInfoViewController *myLodgingInfoViewController;
    UITableView *lodgingTableView;
	NSArray *lodgingArray;
	int previousLodgingId;
	int currentLodgingId;
    NSMutableDictionary *images;
    PagerViewController* slideShowController;
}

@property (nonatomic, retain) LodgingInfoViewController *myLodgingInfoViewController;
@property (nonatomic, retain) NSArray *lodgingArray;
@property (nonatomic, retain) IBOutlet UITableView *lodgingTableView;
@property (nonatomic, retain) NSDictionary *images;

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height;
- (UIImage*)imageWithBorderFromImage:(UIImage*)source;

@end
