//
//  Resource.h
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//  Modified by Denis Santxez
//


@interface Resource : NSObject {
	NSInteger resourceId;
	NSString *type;
	NSString *organization;	
    NSString *location;
    NSString *phone;
    NSString *website;    
    NSString *image;
    NSString *description;
    NSString *video;
    NSString *longitude;
    NSString *latitude;
}
  
@property (nonatomic, assign) NSInteger resourceId;
@property (nonatomic, retain) NSString *type, *organization, *location, *phone, *website, *image, *description, *video, *longitude, *latitude; 

- (id) initWithName:(NSInteger)in_ResourceId type:(NSString *)in_Type organization:(NSString *)in_Organization
			  location:(NSString *)in_Location phone:(NSString *)in_Phone website:(NSString *)in_Website
              image:(NSString *)in_Image description:(NSString *)in_Description  video:(NSString *)in_Video longitude:(NSString *)in_Longitude latitude:(NSString *)in_Latitude; 

@end