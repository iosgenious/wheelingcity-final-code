//
//  MyAnnotation.m
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 11/04/11.
//  Copyright 2011 W. All rights reserved.
//

#import "MyAnnotation.h"


@implementation MyAnnotation
@synthesize coordinate, title, subtitle,category,description;

- (id) initWithDictionary:(NSDictionary *) dict
{
	self = [super init];
	if (self != nil) {
        self.title = [dict objectForKey:@"name"];
		self.subtitle = [dict objectForKey:@"address"];
        self.category=[dict objectForKey:@"category"];
		coordinate.latitude = [[dict objectForKey:@"latitude"] doubleValue];
		coordinate.longitude = [[dict objectForKey:@"longitude"] doubleValue];
        self.description=[dict objectForKey:@"description"];
	}
	return self;
}


@end

/**
 #import "MyAnnotation.h"
 
 
 @implementation MyAnnotation
 @synthesize coordinate, title, subtitle,category;
 
 - (id) initWithDictionary:(NSDictionary *) dict
 {
 self = [super init];
 if (self != nil) {
 coordinate.latitude = [[dict objectForKey:@"latitude"] doubleValue];
 coordinate.longitude = [[dict objectForKey:@"longitude"] doubleValue];
 self.title = [dict objectForKey:@"name"];
 self.subtitle = [dict objectForKey:@"address"];
 category=[dict objectForKey:@"category"];
 }
 return self;
 }
 
 
 @end
 */