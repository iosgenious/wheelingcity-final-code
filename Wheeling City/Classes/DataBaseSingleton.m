//
//  DataBaseSingleton.m
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//

#import "JITHAppDelegate.h"
#import "DataBaseSingleton.h"
#import <CoreLocation/CoreLocation.h>
#import "Lodging.h"
#import "Attraction.h"
#import "Historic.h"
#import "Dining.h"
#import "Resource.h"
#import "Shopping.h"
#import "Area.h"
#import <CommonCrypto/CommonDigest.h>
#import <zlib.h>
#import "Map.h"

#define CHUNK 16384


@implementation DataBaseSingleton

@synthesize lodgingArray;
@synthesize shoppingDict;
@synthesize areaArray;
@synthesize historicArray;
@synthesize attractionsDict;
@synthesize diningDict;
@synthesize resourcesDict;
@synthesize databaseName;
@synthesize databasePath;
@synthesize localMd5;
@synthesize md5Conn, sqlFileConn;
@synthesize newsTicker;
@synthesize coorArray;
@synthesize coorAttractionsArray;
@synthesize coorHistoricalArray;
@synthesize coorLodgingArray;
@synthesize coorShoppingArray;
@synthesize coorResourcesArray;
@synthesize categoryArray;
@synthesize resourceCategory;
@synthesize attractionCategory;
@synthesize layarName,layarConsumerKey,layarConsumerSecret;



#pragma mark -
#pragma mark class instance methods

#pragma mark -
#pragma mark Singleton methods
static DataBaseSingleton *sharedInstance = nil;

+ (DataBaseSingleton*)sharedManager
{
    if (sharedInstance == nil)
        sharedInstance = [[super allocWithZone:NULL] init];
    
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [[self sharedManager] retain];
}

//- (id)init {
//    if (sharedInstance == nil)
//        connectionToInfoMapping = CFDictionaryCreateMutable(
//														kCFAllocatorDefault,
//														0,
//														&kCFTypeDictionaryKeyCallBacks,
//														&kCFTypeDictionaryValueCallBacks);
//}

- (void)setDatabaseVars {
	//NSLog(@"init:We are initializing the singleton");
	//Set up global varibles
    if (connectionToInfoMapping == nil)
        connectionToInfoMapping = CFDictionaryCreateMutable(
                                                            kCFAllocatorDefault,
    														0,
    														&kCFTypeDictionaryKeyCallBacks,
    														&kCFTypeDictionaryValueCallBacks);
    
	databaseName = @"WheelingWV.sqlite";
	
	//Get the path to documents directory and append the database name
	NSArray *the_pDocumentPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	the_pDocumentsDir = [the_pDocumentPaths objectAtIndex:0];
	databasePath = [the_pDocumentsDir stringByAppendingPathComponent:databaseName];
}



- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain {
    return self;
}

- (unsigned)retainCount {
	return NSUIntegerMax;// denotes an object that cannot be released
}

- (void)release {
    //do nothing
}

- (id)autorelease {
    return self;
}

- (BOOL)isDBConnected
{
    return finishedLoading;
}


- (void)checkAndCreateDatabase
{
    //start the boolean value as false
    finishedLoading = false;
	//Check if the SQL database is available on the Iphone, if not the copy it over
	BOOL the_bSuccess;
	
    
	//Crete filemaker object and we will use to check if the database is available on the user‚Äôs iPhone, if not the copy it over
	NSFileManager *the_pFileManager = [NSFileManager defaultManager];
    
    the_bSuccess = [the_pFileManager fileExistsAtPath:databasePath];
    NSString *the_pDatabasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];

    if(!the_bSuccess) 
	{
        //This should happen only first time app is opened.
        //If it doesn't exist then proceed to copy the database from the application to user‚Äôs filesystem
        //Get the path to the database in the application package
        
        //Copy the database from the package to the user‚Äôs filesystem
        [the_pFileManager copyItemAtPath:the_pDatabasePathFromApp toPath:databasePath error:nil];
    }
    else {
        
        
        //Check the date of the file in the bundle and in documents folder, to compare databases and move the newest one
        // For first time only, while upgrading the app
        
        NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:databasePath error:nil];
        NSString *resultDocuments = [fileAttribs valueForKey:NSFileModificationDate];
        //NSLog(@"Database in Documents folder: %@",resultDocuments);
        
        
        fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:the_pDatabasePathFromApp error:nil];
        NSString *resultBundle = [fileAttribs valueForKey:NSFileModificationDate];
        //NSLog(@"Database in Bundle folder: %@",resultBundle);
        
        //Check if the database is already exists in the user‚Äôs filesystem i.e documents directory of the application
        //NSLog(@"Database name is %@ and the path is %@", databaseName, databasePath);
        //If database is already exist then return without doing anything
        
        if([resultDocuments compare:resultBundle] == NSOrderedAscending) {
            //NSLog(@"Document's database is older, copy from %@ to %@", the_pDatabasePathFromApp, databasePath);
            [the_pFileManager removeItemAtPath:databasePath error:nil];
            [the_pFileManager copyItemAtPath:the_pDatabasePathFromApp toPath:databasePath error:nil];

        }
        else
            NSLog(@"Document's database is newer");
    }

    
	
    //Calculate file's MD5 checksum
    
    NSFileHandle *handle = [NSFileHandle fileHandleForReadingAtPath:databasePath];
    if( handle == nil ) NSLog(@"ERROR GETTING FILE MD5"); // file didnt exist
    CC_MD5_CTX md5;
    
	CC_MD5_Init(&md5);
	
	BOOL done = NO;
    NSData *fileData;
	while(!done)
	{
        
        fileData = [[[NSData alloc] initWithData:[handle readDataOfLength:4096]]autorelease];
        
        CC_MD5_Update(&md5, [fileData bytes], [fileData length]);
        
        if( [fileData length] == 0 )
        {
            done = YES;
        }
        
    }
    
    
    // [fileData release];
    
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final(digest, &md5);
    localMd5 = [[NSString alloc] initWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                digest[0], digest[1], 
                digest[2], digest[3],
                digest[4], digest[5],
                digest[6], digest[7],
                digest[8], digest[9],
                digest[10], digest[11],
                digest[12], digest[13],
                digest[14], digest[15]];
    //NSLog(@"MD5 is %@", localMd5);
    
    //responseData = [[NSMutableData data] retain];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://wheeling.yourmobicity.com/English/WheelingWV.sqlite.md5"] cachePolicy:NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval:15.0];
	md5Conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    CFDictionaryAddValue(
                         connectionToInfoMapping,
                         md5Conn,
                         [NSMutableDictionary
                          dictionaryWithObject:[NSMutableData data]
                          forKey:@"md5Data"]);
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    //add the data to the corresponding NSURLConnection object
	const NSMutableDictionary *connectionInfo = CFDictionaryGetValue(connectionToInfoMapping, connection);
    [self setDatabaseVars];
    
    //  NSLog(@"Database path is %@", databasePath);
	if ([connectionInfo objectForKey:@"md5Data"] != nil) {
        
		[[connectionInfo objectForKey:@"md5Data"] appendData:data];
        NSString *responseString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        NSLog(@"MD5 on the Server is %@, is it equal to local MD5 (%@)? %@", responseString, localMd5, ([localMd5 isEqualToString:responseString]? @"YES" : @"NO"));
        //TODO: Use regular expression to ensure it is not an md5
        if ([responseString length] != [localMd5 length])
        {
            //NSLog(@"Not a valid MD5, use the old database");
            JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
            [the_pAppDelegate finishedUpdatingDatabase];
            finishedLoading = TRUE;
        }
        else if ([localMd5 isEqualToString:responseString]) {
            //NSLog(@"It is equal, don't do anything");
            JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
            [the_pAppDelegate finishedUpdatingDatabase];
            finishedLoading = TRUE;

        }
        else {
            //NSLog(@"It is different, download new SQLite");
            NSFileManager *the_pFileManager = [NSFileManager defaultManager];
            [the_pFileManager removeItemAtPath:databasePath error:NULL];
            
            
            NSString *zippedDBPath = [databasePath stringByAppendingString:@".gz"];
            [the_pFileManager createFileAtPath:zippedDBPath contents:nil attributes:nil];
            file = [[NSFileHandle fileHandleForUpdatingAtPath:zippedDBPath] retain];
            if (file)   {
                
                [file seekToEndOfFile];
            }
            //           [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://wheeling.yourmobicity.com/English/WheelingWV.sqlite"]];
            sqlFileConn = [[NSURLConnection alloc] initWithRequest:request delegate:self];           
            //JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
            //[the_pAppDelegate updatingDatabase];
            CFDictionaryAddValue(
                                 connectionToInfoMapping,
                                 sqlFileConn,
                                 [NSMutableDictionary
                                  dictionaryWithObject:[NSMutableData data]
                                  forKey:@"sqlFileData"]);
        }    
    }
	else if ([connectionInfo objectForKey:@"sqlFileData"] != nil) {
		[[connectionInfo objectForKey:@"sqlFileData"] appendData:data];
        //NSLog(@"Received SQLite File, we are about to write a file in %@", databasePath);
        //NSFileManager *the_pFileManager = [NSFileManager defaultManager];
        //[the_pFileManager createFileAtPath:databasePath  contents:data attributes:nil];
        //[data writeToFile:databasePath atomically:YES];
        if (file)  { 
            // NSLog(@"Downloading compressed SQLite");
            [file seekToEndOfFile];
        } 
        [file writeData:data]; 
        // [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
    [the_pAppDelegate finishedUpdatingDatabase];
    //NSLog(@"Connection failed: %@", [error description]);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    finishedLoading=true;
    /*JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
    [the_pAppDelegate finishedUpdatingDatabase];*/
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    //   [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    
    const NSMutableDictionary *connectionInfo = CFDictionaryGetValue(connectionToInfoMapping, connection);
    
    if ([connectionInfo objectForKey:@"sqlFileData"] != nil) {
        [self setDatabaseVars];  
        NSString *zippedDBPath = [databasePath stringByAppendingString:@".gz"];
        NSFileManager *the_pFileManager = [NSFileManager defaultManager];
        
        [the_pFileManager moveItemAtPath:zippedDBPath toPath:databasePath error:nil];
        
        
        //NSLog(@"(Auto Decompressed and renamed)Finished Download DataBase at: %@", databasePath);
        
        NSDictionary *fileAttributes = [the_pFileManager attributesOfItemAtPath:databasePath error:nil];
        
        NSLog(@"(DidFinishLoading) Size of unzipped file is %@", [fileAttributes objectForKey:@"NSFileSize"]);
        JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
        [the_pAppDelegate finishedUpdatingDatabase];
        
        
        [connection release];
        finishedLoading=true;
    }
    else if ([connectionInfo objectForKey:@"md5Data"] != nil) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }


//    [del.navigationController   
}

/**
 It checks whether the image exists in the Documents folder
 \param imageName name of the jpg/png file to check
 \return TRUE if the image is already copied/cached on the Documents folder
 
 */

- (BOOL) checkImage:(NSString *)imageName
{
	//Images coming from bundle are loaded the first time they are used, and copied to the documents directory of the appliaction
	
	//Crete filemaker object and we will use to check if the image is available on the user‚Äôs iPhone, if not the copy it over
	NSFileManager *the_pFileManager = [NSFileManager defaultManager];
	//Check if the image is already exists in the user‚Äôs filesystem i.e documents directory of the application
	return [the_pFileManager fileExistsAtPath:[the_pDocumentsDir stringByAppendingPathComponent:imageName]];
	[the_pFileManager release];
    
}	
/**
 Copies an image that comes bundled to application's Documents dir
 \param imageName name of the image that should come bundled
 \return FALSE is the file is not found on the bundle
 */
/**
 If the file exists on the Bundle, copy it to the Documents folder
 \param imageName name of the image from the Database that we want to copy from Bundle to Documents
 */
- (BOOL) CopyImage:(NSString *)imageName
{
	NSString *imagePath = [the_pDocumentsDir stringByAppendingPathComponent:imageName];	
	//If image is already exist then return without doing anything
	if ([self checkImage:imageName]) return TRUE;
	NSFileManager *the_pFileManager = [NSFileManager defaultManager];
    
	//Get the path to the image in the application package
	NSString *the_pImagePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:imageName];
	if ([the_pFileManager fileExistsAtPath:the_pImagePathFromApp]) {
        [the_pFileManager copyItemAtPath:the_pImagePathFromApp toPath:imagePath error:nil];
        return TRUE;
    } else {
        return FALSE;
    }
}


- (void)saveImage:(UIImage *)image withName:(NSString *)name {
	
	//save image
	NSData *data = UIImagePNGRepresentation(image);
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:name];
	[fileManager createFileAtPath:fullPath contents:data attributes:nil];
	
}

- (void)readNewsTicker
{
    sqlite3* the_pDatabase;
    NSMutableString* tmpNewsTicker=[[NSMutableString alloc]initWithString:@""];
    if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
    {
        const char *the_pSqlStatement = "select Text from news";
        sqlite3_stmt* the_pCompiledStatement;
        
        if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL)==SQLITE_OK)
        {
            while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
            {
                [tmpNewsTicker appendFormat:@"%@                        ",[NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 0)]];
            }
        }
        sqlite3_finalize(the_pCompiledStatement);
    }
	sqlite3_close(the_pDatabase);
    newsTicker=[tmpNewsTicker copy];
    //NSLog(@"%@", newsTicker);
    [tmpNewsTicker release];
    
}
- (void)readLodgingFromDatabase
{
	sqlite3 *the_pDatabase;
	//Init the artist array
	if (lodgingArray != nil)
		[lodgingArray removeAllObjects];
	else {
		lodgingArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Lodging Order by Priority";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSInteger the_pLodgingId = sqlite3_column_int(the_pCompiledStatement, 0);
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                NSString *the_pDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                NSString *the_pLocation = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 3)];
                NSString *the_pPhone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 4)];

                char *theImage = (char *)sqlite3_column_text(the_pCompiledStatement, 5);
                NSString *the_pImage;
                if (theImage !=NULL) {
                     the_pImage = [NSString stringWithUTF8String: theImage];
                }
                else 
                    the_pImage= @"Lodging.png";
                
                char *theWebsite = (char *)sqlite3_column_text(the_pCompiledStatement, 6);
                NSString *the_pWebsite;
                if (theWebsite !=NULL) {
                    the_pWebsite = [NSString stringWithUTF8String: theWebsite];
                }
                else 
                    the_pWebsite= nil;

                char *theVideo = (char *)sqlite3_column_text(the_pCompiledStatement, 7);
                NSString *the_pVideo;
                if (theVideo !=NULL) {
                    the_pVideo = [NSString stringWithUTF8String: theVideo];
                }
                else 
                    the_pVideo= nil;
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;


				//NSLog(@"We are adding a new Lodging named %@", the_pName);
				//Create new Lodging object with the data from the database
                
				Lodging *the_pLodging = [[Lodging alloc] initWithName:the_pLodgingId name:the_pName 
													   description:the_pDescription location:the_pLocation
                                                                phone:the_pPhone image:the_pImage website:the_pWebsite video:the_pVideo longitude:the_pLongitude latitude:the_pLatitude];
				
				[lodgingArray addObject:the_pLodging];
				
				//Copy the image to iPhone's documents directory
                // NSLog(@"Image %@ is in Documents? %@",the_pImage, [self checkImage:the_pImage]? @"YES":@"NO");
				if (![self checkImage:the_pImage]) {
                    if(![self CopyImage:the_pImage]) {
                        NSLog(@"Image was not in the Bundle, it will be downloaded and copied to Documents later while browsing the list");
                    }
                }
				[the_pLodging release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}

- (void)readShoppingFromDatabase
{
	sqlite3 *the_pDatabase;
	//Init the artist dict
	if (shoppingDict != nil)
		[shoppingDict removeAllObjects];
	else {
		shoppingDict = [[NSMutableDictionary alloc] init];
	}
    if (areaArray != nil)
		[areaArray removeAllObjects];
	else {
		areaArray = [[NSMutableArray alloc] init];
	}
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "SELECT * FROM Shopping, Area WHERE Shopping.AreaId = Area.Id Order by Area.Priority,Shopping.Priority, Shopping.name";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSInteger the_pShoppingId = sqlite3_column_int(the_pCompiledStatement, 0);
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                //Description can be null
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 3);
                NSString *the_pDescription;
                if (theDescription !=NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];
                }
                else 
                    the_pDescription= @"No description available";
                NSInteger the_pAreaId = sqlite3_column_int(the_pCompiledStatement, 2);
            
                char *theImage = (char *)sqlite3_column_text(the_pCompiledStatement, 4);
                NSString *the_pImage;
                
                if (theImage !=NULL && strcmp(theImage, "")) {
                    the_pImage = [NSString stringWithUTF8String: theImage];
                }
                else 
                    the_pImage= @"Shopping.png";
                
                char *thePhone = (char *)sqlite3_column_text(the_pCompiledStatement, 5);
                NSString *the_pPhone;
                if (thePhone !=NULL) {
                    the_pPhone = [NSString stringWithUTF8String: thePhone];
                }
                else 
                    the_pPhone= nil;
                
                char *theVideo = (char *)sqlite3_column_text(the_pCompiledStatement, 6);
                NSString *the_pVideo;
                if (theVideo !=NULL) {
                    the_pVideo = [NSString stringWithUTF8String: theVideo];
                }
                else 
                    the_pVideo= nil;
                
                NSString *the_pAreaName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 11)];
            
                char *theAreaDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 12);
                NSString *the_pAreaDescription;
                if (theAreaDescription !=NULL) {
                    the_pAreaDescription = [NSString stringWithUTF8String: theAreaDescription];
                }
                else
                        the_pAreaDescription= nil;
                    
                char *theAreaImage = (char *)sqlite3_column_text(the_pCompiledStatement, 13);
                NSString *the_pAreaImage;
                if (theAreaImage !=NULL) {
                    the_pAreaImage = [NSString stringWithUTF8String: theAreaImage];
                }
                else 
                    the_pAreaImage= @"Shopping.png";
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 8);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
            
            
				//NSLog(@"We are adding a new Shopping named %@", the_pName);
				//Create new Shopping object with the data from the database
                
				Shopping *the_pShopping = [[Shopping alloc] initWithName:the_pShoppingId name:the_pName 
                                                             description:the_pDescription location:the_pAreaId
                                                                   phone:the_pPhone image:the_pImage video:the_pVideo
                                                                    area:the_pAreaName longitude:the_pLongitude latitude:the_pLatitude];
                Area *the_pArea = [[Area alloc] initWithName:the_pAreaId name:the_pAreaName description:the_pAreaDescription image:the_pAreaImage];
                
                if (![shoppingDict objectForKey:the_pAreaName]) {
                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                    [shoppingDict setObject:tempArray forKey:the_pAreaName];
                    [tempArray release];
                    [areaArray addObject:the_pArea];
                }
                [[shoppingDict objectForKey:the_pAreaName] addObject:the_pShopping];
                if (![self checkImage:the_pAreaImage]) {
                    if(![self CopyImage:the_pAreaImage]) {
                        //   NSLog(@"Image was not in the Bundle, it will be downloaded and copied to Documents later while browsing the list");
                    }
                }
				[the_pShopping release];
                [the_pArea release];
            }    
        }
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}

- (void)readHistoricFromDatabase
{
	sqlite3 *the_pDatabase;
	//Init the artist array
	if (historicArray != nil)
		[historicArray removeAllObjects];
	else {
		historicArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Historical Order by Priority, Site";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSInteger the_pHistoricId = sqlite3_column_int(the_pCompiledStatement, 0);
				//NSString *the_pCategory = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                //Description can be null
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 3);
                NSString *the_pDescription;
                if (theDescription !=NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];
                }
                else 
                    the_pDescription= @"No description available";

                char *theLocation = (char *)sqlite3_column_text(the_pCompiledStatement, 2);
                NSString *the_pLocation;
                if (theLocation != NULL) {
                    the_pLocation = [NSString stringWithUTF8String:theLocation];
                }
                else
                    the_pLocation = nil;
                NSString *the_pIsThere = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 5)];
                
                char *theImage = (char *)sqlite3_column_text(the_pCompiledStatement, 6);
                NSString *the_pImage;
                if (theImage !=NULL) {
                    the_pImage = [NSString stringWithUTF8String: theImage];
                }
                else 
                    the_pImage= @"Historical.png";
                
                char *theWebsite = (char *)sqlite3_column_text(the_pCompiledStatement, 4);
                NSString *the_pWebsite;
                if (theWebsite !=NULL) {
                    the_pWebsite = [NSString stringWithUTF8String: theWebsite];
                }
                else 
                    the_pWebsite= nil;
                
                char *theVideo = (char *)sqlite3_column_text(the_pCompiledStatement, 7);
                NSString *the_pVideo;
                if (theVideo !=NULL) {
                    the_pVideo = [NSString stringWithUTF8String: theVideo];
                }
                else 
                    the_pVideo= nil;
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                
                
				//NSLog(@"We are adding a new Historic named %@", the_pName);
				//Create new Historic object with the data from the database
                
				Historic *the_pHistoric = [[Historic alloc] initWithName:the_pHistoricId name:the_pWebsite 
                                                          description:the_pDescription location:the_pLocation
                                                                phone:the_pIsThere image:the_pImage website:the_pWebsite video:the_pVideo longitude:the_pLongitude latitude:the_pLatitude];
				
				[historicArray addObject:the_pHistoric];
				
				//Copy the image to iPhone's documents directory
                // NSLog(@"Image %@ is in Documents? %@",the_pImage, [self checkImage:the_pImage]? @"YES":@"NO");
				if (![self checkImage:the_pImage]) {
                    if(![self CopyImage:the_pImage]) {
                        NSLog(@"Image was not in the Bundle, it will be downloaded and copied to Documents later while browsing the list");
                    }
                }
				[the_pHistoric release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}

- (void)readAttractionsFromDatabase
{
    sqlite3 *the_pDatabase;
    //Init the artist array
    if (attractionsDict != nil)
        [attractionsDict removeAllObjects];
    else {
        attractionsDict = [[NSMutableDictionary alloc] init];
    }
    attractionCategory = [[NSMutableArray alloc]init];
    //Create four categories
    //NSMutableArray *entertainmentArray = [[NSMutableArray alloc] init];
	//NSMutableArray *sportsArray = [[NSMutableArray alloc] init];
	//NSMutableArray *youthArray = [[NSMutableArray alloc] init];
	//NSMutableArray *eventsArray = [[NSMutableArray alloc] init];
    //NSMutableArray *golfArray = [[NSMutableArray alloc] init];
    
    //Open the database from the user[s filesystem
    if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
    {
        //Setting up SQL statements
        // No performance times known
        const char *the_pSqlStatement = "select * from Attractions Order by Priority,Name";
        sqlite3_stmt *the_pCompiledStatement;
        if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
        {
            //Loop through the results and add them to array
            while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
            {
                //Read the data from the result row
                NSInteger the_pAttractionId = sqlite3_column_int(the_pCompiledStatement, 0);
                NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                NSString *the_pDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                NSString *the_pCategory = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 3)];              
                NSString *the_pAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 4)];
                
                //NSString *the_pPhone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 5)];
                char *thePhone = (char *)sqlite3_column_text(the_pCompiledStatement, 5);
                NSString *the_pPhone;
                if (thePhone !=NULL) {
                    the_pPhone = [NSString stringWithUTF8String: thePhone];
                }
                else 
                    the_pPhone= nil;
            
                
                char *theImage = (char *)sqlite3_column_text(the_pCompiledStatement, 6);
                NSString *the_pImage;
                if (theImage !=NULL) {
                    the_pImage = [NSString stringWithUTF8String: theImage];
                }
                else 
                    the_pImage= @"Attractions.png";
                
                char *theVideo = (char *)sqlite3_column_text(the_pCompiledStatement, 7);
                NSString *the_pVideo;
                if (theVideo !=NULL) {
                    the_pVideo = [NSString stringWithUTF8String: theVideo];
                }
                else 
                    the_pVideo= nil;
                
                char *theWebsite = (char *)sqlite3_column_text(the_pCompiledStatement, 8);
                NSString *the_pWebsite;
                if (theWebsite !=NULL) {
                    the_pWebsite = [NSString stringWithUTF8String: theWebsite];
                }
                else 
                    the_pWebsite= nil;
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 11);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                
                
                Attraction *the_pAttraction = [[Attraction alloc] initWithName:the_pAttractionId name:the_pName description:the_pDescription category:the_pCategory address:the_pAddress phone:the_pPhone image:the_pImage video:the_pVideo website:the_pWebsite longitude:the_pLongitude latitude:the_pLatitude];
                
                //Add the attraction object to the attractions dict
                if (![attractionsDict objectForKey:the_pCategory]) {
                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                    [attractionCategory addObject:the_pCategory];
                    [attractionsDict setObject:tempArray forKey:the_pCategory];
                    [tempArray release];
                }
                [[attractionsDict objectForKey:the_pCategory] addObject:the_pAttraction];
				
                /* if ([the_pCategory isEqualToString:@"Entertainment"]) {
                 [entertainmentArray addObject:the_pAttraction];
                 }
                 else if ([the_pCategory isEqualToString:@"Sports and Rec."]) {
                 [sportsArray addObject:the_pAttraction];
                 }
                 else if ([the_pCategory isEqualToString:@"Youth and kids"]) {
                 [youthArray addObject:the_pAttraction];
                 }
                 else if ([the_pCategory isEqualToString:@"Golf"]) {
                     [golfArray addObject:the_pAttraction];
                 }
                 else{	//([the_pCategory isEqualToString:@"Annual Events"]) {
                 [eventsArray addObject:the_pAttraction];
                 }*/
                //[attractionsArray addObject:the_pAttraction];
                
                //Copy the image to iPhone's documents directory
                // NSLog(@"Image %@ is in Documents? %@",the_pImage, [self checkImage:the_pImage]? @"YES":@"NO");
                if (![self checkImage:the_pImage]) {
                    if(![self CopyImage:the_pImage]) {
                        NSLog(@"Image was not in the Bundle, it will be downloaded and copied to Documents later while browsing the list");
                    }
                }
                [the_pAttraction release];
            }
           /* [attractionsDict setObject:entertainmentArray forKey:@"Entertainment"];
            [attractionsDict setObject:sportsArray forKey:@"Sports and Rec."];
            [attractionsDict setObject:youthArray forKey:@"Youth and kids"];
            [attractionsDict setObject:eventsArray forKey:@"Annual Events"];
            [attractionsDict setObject:golfArray forKey:@"Golf"];*/

        }
        //Release the compiled statement from the memory
        sqlite3_finalize(the_pCompiledStatement);
    }
    /*[entertainmentArray release];
    [sportsArray release];
    [youthArray release];
    [eventsArray release];
    [golfArray release];*/
    sqlite3_close(the_pDatabase);
}

- (void)readDiningFromDatabase
{
	sqlite3 *the_pDatabase;
	//Init the artist array
	if (diningDict != nil)
		[diningDict removeAllObjects];
	else {
		diningDict = [[NSMutableDictionary alloc] init];
	}
	
    //Create two categories
    NSMutableArray *localArray = [[NSMutableArray alloc] init];
	NSMutableArray *nationalArray = [[NSMutableArray alloc] init];
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Dinning Order by Priority,Name";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSInteger the_pDiningId = sqlite3_column_int(the_pCompiledStatement, 0);
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                //Address can be null
                char *theAddress = (char *)sqlite3_column_text(the_pCompiledStatement,2);
                NSString *the_pAddress;
                if (theAddress !=NULL) {
                    the_pAddress = [NSString stringWithUTF8String: theAddress];
                }
                else 
                    the_pAddress = @"No address available";
                
                NSString *the_pIsLocal = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 3)];
                //Phone can be null
                char *thePhone = (char *)sqlite3_column_text(the_pCompiledStatement,5);
                NSString *the_pPhone;
                if (thePhone !=NULL) {
                    the_pPhone = [NSString stringWithUTF8String: thePhone];
                }
                else 
                    the_pPhone = @"No phone available";
                
                char *theImage = (char *)sqlite3_column_text(the_pCompiledStatement, 4);
                NSString *the_pImage;
                if (theImage !=NULL) {
                    the_pImage = [NSString stringWithUTF8String: theImage];
                }
                else 
                    the_pImage= @"Dining.png";
                
                char *theWebsite = (char *)sqlite3_column_text(the_pCompiledStatement, 7);
                NSString *the_pWebsite;
                if (theWebsite !=NULL) {
                    the_pWebsite = [NSString stringWithUTF8String: theWebsite];
                }
                else 
                    the_pWebsite= nil;
                
                char *theVideo = (char *)sqlite3_column_text(the_pCompiledStatement, 6);
                NSString *the_pVideo;
                if (theVideo !=NULL) {
                    the_pVideo = [NSString stringWithUTF8String: theVideo];
                }
                else 
                    the_pVideo= nil;
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                
                
                
                
				//Create new Dining object with the data from the database
                
				Dining *the_pDining = [[Dining alloc] initWithName:the_pDiningId name:the_pName 
                                                          description:the_pAddress location:the_pIsLocal
                                                                phone:the_pPhone image:the_pImage website:the_pWebsite video:the_pVideo longitude:the_pLongitude latitude:the_pLatitude];
				
                //Add the dining object to the dinning dict
				
                if ([the_pIsLocal isEqualToString:@"YES"]) {
                    [localArray addObject:the_pDining];
                }
                else{	//([the_pIsLocal isEqualToString:@"NO"]) {
                    [nationalArray addObject:the_pDining];
                }
                //[diningArray addObject:the_pDining];
				
				//Copy the image to iPhone's documents directory
                // NSLog(@"Image %@ is in Documents? %@",the_pImage, [self checkImage:the_pImage]? @"YES":@"NO");
				if (![self checkImage:the_pImage]) {
                    if(![self CopyImage:the_pImage]) {
                        NSLog(@"Image was not in the Bundle, it will be downloaded and copied to Documents later while browsing the list");
                    }
                }
				[the_pDining release];
			}
            [diningDict setObject:localArray forKey:@"YES"];
            [diningDict setObject:nationalArray forKey:@"NO"];

		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
    [localArray release];
    [nationalArray release];
	sqlite3_close(the_pDatabase);
}


- (void)readResourcesFromDatabase
{
    sqlite3 *the_pDatabase;
    //Init the artist array
    if (resourcesDict != nil)
        [resourcesDict removeAllObjects];
    else {
        resourcesDict = [[NSMutableDictionary alloc] init];
    }
    resourceCategory = [[NSMutableArray alloc]init];
    
    //Open the database from the user[s filesystem
    if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
    {
        //Setting up SQL statements
        // No performance times known
        const char *the_pSqlStatement = "select * from Resources Order by Priority, Organization";
        sqlite3_stmt *the_pCompiledStatement;
        if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
        {
            //Loop through the results and add them to array
            while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
            {
                //Read the data from the result row
                NSInteger the_pResourceId = sqlite3_column_int(the_pCompiledStatement, 0);
                NSString *the_pType = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                NSString *the_pOrganization = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                NSString *the_pLocation = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 3)];              
                NSString *the_pPhone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 4)];
                
                char *theWebsite = (char *)sqlite3_column_text(the_pCompiledStatement, 5);
                NSString *the_pWebsite;
                if (theWebsite !=NULL) {
                    the_pWebsite = [NSString stringWithUTF8String: theWebsite];
                }
                else 
                    the_pWebsite= nil;
                
                char *theImage = (char *)sqlite3_column_text(the_pCompiledStatement, 6);
                NSString *the_pImage;
                if (theImage !=NULL) {
                    the_pImage = [NSString stringWithUTF8String: theImage];
                }
                else 
                    the_pImage= @"Resources.png";
                
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 7);
                NSString *the_pDescription; 
                if (theDescription != NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];

                }

                char *theVideo = (char *)sqlite3_column_text(the_pCompiledStatement, 8);
                NSString *the_pVideo;
                if (theVideo !=NULL) {
                    the_pVideo = [NSString stringWithUTF8String: theVideo];
                }
                else 
                    the_pVideo= nil;
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 11);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                
                Resource *the_pResource = [[Resource alloc] initWithName:the_pResourceId type:the_pType organization:the_pOrganization location:the_pLocation phone:the_pPhone website:the_pWebsite image:the_pImage description:the_pDescription video:the_pVideo longitude:the_pLongitude latitude:the_pLatitude];
                
                if (![resourcesDict objectForKey:the_pType]) {
                    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                    [resourcesDict setObject:tempArray forKey:the_pType];
                    [resourceCategory addObject:the_pType];
                    [tempArray release];
                }
                [[resourcesDict objectForKey:the_pType] addObject:the_pResource];
                
                if (![self checkImage:the_pImage]) {
                    if(![self CopyImage:the_pImage]) {
                        NSLog(@"Image was not in the Bundle, it will be downloaded and copied to Documents later while browsing the list");
                    }
                }
                [the_pResource release];
            }

        }
        //Release the compiled statement from the memory
        sqlite3_finalize(the_pCompiledStatement);
    }
    sqlite3_close(the_pDatabase);
}
- (void)readCoordenatesDinning
{
	sqlite3 *the_pDatabase;
    NSString *the_pCategory=@"Dining";
	//Init the artist array
	if (coorArray != nil)
		[coorArray removeAllObjects];
	else {
		coorArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Dinning";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                
                /*
                 NSString *the_pAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                 */
                
                char *theAddress = (char *)sqlite3_column_text(the_pCompiledStatement, 2);
                NSString *the_pAddress;
                if (theAddress !=NULL) {
                    the_pAddress = [NSString stringWithUTF8String: theAddress];
                }
                else 
                    the_pAddress= nil;
                
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                
                
				//NSLog(@"We are adding a new Historic named %@", the_pName);
				//Create new Historic object with the data from the database
                
				Map *the_pMap = [[Map alloc] initWithName:the_pName address:the_pAddress latitude:the_pLatitude longitude:the_pLongitude category:the_pCategory description:nil ];
				
				[coorArray addObject:the_pMap];
				
                
				[the_pMap release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}


- (void)readCoordenatesAttractions
{
	sqlite3 *the_pDatabase;
    NSString *the_pCategory=@"Attractions";
    
	//Init the artist array
	if (coorAttractionsArray != nil)
		[coorAttractionsArray removeAllObjects];
	else {
		coorAttractionsArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Attractions";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                
                /*
                 NSString *the_pAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                 */
                
                char *theAddress = (char *)sqlite3_column_text(the_pCompiledStatement, 4);
                NSString *the_pAddress;
                if (theAddress !=NULL) {
                    the_pAddress = [NSString stringWithUTF8String: theAddress];
                }
                else 
                    the_pAddress= nil;
                
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 11);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 2);
                NSString *the_pDescription;
                if (theDescription !=NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];
                }
                else 
                    the_pDescription= nil;
                
                
				//NSLog(@"We are adding a new Historic named %@", the_pName);
				//Create new Historic object with the data from the database
				Map *the_pMap = [[Map alloc] initWithName:the_pName address:the_pAddress latitude:the_pLatitude longitude:the_pLongitude category:the_pCategory description:the_pDescription];
				
				[coorAttractionsArray addObject:the_pMap];
				
                
				[the_pMap release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}
- (void)readCoordenatesHistorical
{
	sqlite3 *the_pDatabase;
    NSString *the_pCategory=@"Historical";
    
	//Init the artist array
	if (coorHistoricalArray != nil)
		[coorHistoricalArray removeAllObjects];
	else {
		coorHistoricalArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Historical";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 4)];
                
                /*
                 NSString *the_pAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                 */
                
                char *theAddress = (char *)sqlite3_column_text(the_pCompiledStatement, 2);
                NSString *the_pAddress;
                if (theAddress !=NULL) {
                    the_pAddress = [NSString stringWithUTF8String: theAddress];
                }
                else 
                    the_pAddress= nil;
                
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 3);
                NSString *the_pDescription;
                if (theDescription !=NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];
                }
                else 
                    the_pDescription= nil;
                
                
                
				//NSLog(@"We are adding a new Historic named %@", the_pName);
				//Create new Historic object with the data from the database
                
				Map *the_pMap = [[Map alloc] initWithName:the_pName address:the_pAddress latitude:the_pLatitude longitude:the_pLongitude category:the_pCategory description:the_pDescription];
				
				[coorHistoricalArray addObject:the_pMap];
				
                
				[the_pMap release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}
- (void)readCoordenatesLodging
{
	sqlite3 *the_pDatabase;
    NSString *the_pCategory=@"Lodging";
    
	//Init the artist array
	if (coorLodgingArray != nil)
		[coorLodgingArray removeAllObjects];
	else {
		coorLodgingArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Lodging";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                
                /*
                 NSString *the_pAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                 */
                
                char *theAddress = (char *)sqlite3_column_text(the_pCompiledStatement, 3);
                NSString *the_pAddress;
                if (theAddress !=NULL) {
                    the_pAddress = [NSString stringWithUTF8String: theAddress];
                }
                else 
                    the_pAddress= nil;
                
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 2);
                NSString *the_pDescription;
                if (theDescription !=NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];
                }
                else 
                    the_pDescription= nil;
                
                
				//NSLog(@"We are adding a new Historic named %@", the_pName);
				//Create new Historic object with the data from the database
                
				Map *the_pMap = [[Map alloc] initWithName:the_pName address:the_pAddress latitude:the_pLatitude longitude:the_pLongitude category:the_pCategory description:the_pDescription];
				
				[coorLodgingArray addObject:the_pMap];
				
                
				[the_pMap release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}

- (void)readCoordenatesShopping
{
	sqlite3 *the_pDatabase;
    NSString *the_pCategory=@"Shopping";
    
	//Init the artist array
	if (coorShoppingArray != nil)
		[coorShoppingArray removeAllObjects];
	else {
		coorShoppingArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Shopping";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                
                /*
                 NSString *the_pAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                 */
                
                char *theAddress = (char *)sqlite3_column_text(the_pCompiledStatement, 2);
                theAddress=nil;
                NSString *the_pAddress;
                if (theAddress !=NULL) {
                    the_pAddress = [NSString stringWithUTF8String: theAddress];
                }
                else 
                    the_pAddress= nil;
                
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 8);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 9);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 3);
                NSString *the_pDescription;
                if (theDescription !=NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];
                }
                else 
                    the_pDescription= nil;
                
                
				//NSLog(@"We are adding a new Historic named %@", the_pName);
				//Create new Historic object with the data from the database
                
				Map *the_pMap = [[Map alloc] initWithName:the_pName address:the_pAddress latitude:the_pLatitude longitude:the_pLongitude category:the_pCategory description:the_pDescription];
				
				[coorShoppingArray addObject:the_pMap];
				
                
				[the_pMap release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}

- (void)readCoordenatesResources
{
	sqlite3 *the_pDatabase;
    NSString *the_pCategory=@"Resources";
    
	//Init the artist array
	if (coorResourcesArray != nil)
		[coorResourcesArray removeAllObjects];
	else {
		coorResourcesArray = [[NSMutableArray alloc] init];
	}
	
	
	//Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
		//Setting up SQL statements
		// No performance times known
		const char *the_pSqlStatement = "select * from Resources";
		sqlite3_stmt *the_pCompiledStatement;
		if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
			//Loop through the results and add them to array
			while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
			{
				//Read the data from the result row
				NSString *the_pName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                
                /*
                 NSString *the_pAddress = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                 */
                
                char *theAddress = (char *)sqlite3_column_text(the_pCompiledStatement, 3);
                theAddress=nil;
                NSString *the_pAddress;
                if (theAddress !=NULL) {
                    the_pAddress = [NSString stringWithUTF8String: theAddress];
                }
                else 
                    the_pAddress= nil;
                
                
                char *theLatitude = (char *)sqlite3_column_text(the_pCompiledStatement, 10);
                NSString *the_pLatitude;
                if (theLatitude !=NULL) {
                    the_pLatitude = [NSString stringWithUTF8String: theLatitude];
                }
                else 
                    the_pLatitude= nil;
                
                char *theLongitude = (char *)sqlite3_column_text(the_pCompiledStatement, 11);
                NSString *the_pLongitude;
                if (theLongitude !=NULL) {
                    the_pLongitude = [NSString stringWithUTF8String: theLongitude];
                }
                else 
                    the_pLongitude= nil;
                char *theDescription = (char *)sqlite3_column_text(the_pCompiledStatement, 7);
                NSString *the_pDescription;
                if (theDescription !=NULL) {
                    the_pDescription = [NSString stringWithUTF8String: theDescription];
                }
                else 
                    the_pDescription= nil;
                
                
				//NSLog(@"We are adding a new Historic named %@", the_pName);
				//Create new Historic object with the data from the database
                
				Map *the_pMap = [[Map alloc] initWithName:the_pName address:the_pAddress latitude:the_pLatitude longitude:the_pLongitude category:the_pCategory description:the_pDescription];
				
				[coorResourcesArray addObject:the_pMap];
				
                
				[the_pMap release];
			}
		}
		//Release the compiled statement from the memory
		sqlite3_finalize(the_pCompiledStatement);
	}
	sqlite3_close(the_pDatabase);
}

-(void)readLayarConfiguration
{
    sqlite3 *the_pDatabase;
    //Open the database from the user[s filesystem
	if(sqlite3_open([databasePath UTF8String], &the_pDatabase) == SQLITE_OK)
	{
        const char *the_pSqlStatement = "select * from Layar";
		sqlite3_stmt *the_pCompiledStatement;
        //Loop through the results and add them to array
        if(sqlite3_prepare_v2(the_pDatabase, the_pSqlStatement, -1, &the_pCompiledStatement, NULL) == SQLITE_OK)
		{
            while(sqlite3_step(the_pCompiledStatement) == SQLITE_ROW)
            {
                layarName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 1)];
                layarConsumerKey = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 2)];
                layarConsumerSecret = [NSString stringWithUTF8String:(char *)sqlite3_column_text(the_pCompiledStatement, 3)];
                char *isavailable = (char *)sqlite3_column_text(the_pCompiledStatement, 4);
                if (strcmp(isavailable,"TRUE")==0)
                    layarIsAvailable=true;
                else
                    layarIsAvailable=false;
                    
                NSLog(@"Name:%@, Key:%@ and Secret: %@",layarName,layarConsumerKey,layarConsumerSecret);
            }
        }
    }
}

-(BOOL)isLayarAvailable
{
    return layarIsAvailable;
}



- (void) didReceiveMemoryWarning {
	NSLog(@"Ignoring memory warning in singleton");
}

/*
 - (void)dealloc {
 //This object should not destroy some objects when memory warnings happen. It's a singleton
 //that should keep values at all times
 
 [lodgingArray release];
 [partnersArray release];
 [sharedInstance release];
 [databaseName release];
 [databasePath release];
 [md5Conn release];
 [sqlFileConn release];
 
 [super dealloc];
 }
 */

@end
