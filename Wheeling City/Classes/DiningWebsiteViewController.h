//
//  DiningWebsiteViewController.h
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DiningWebsiteViewController : UIViewController <UIWebViewDelegate>{
    UIWebView *diningWeb;
}
@property (nonatomic, retain) IBOutlet UIWebView *diningWeb;
- (void) blankPage;

@end
