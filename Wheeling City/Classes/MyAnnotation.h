//
//  MyAnnotation.h
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 11/04/11.
//  Copyright 2011 W. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject <MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	NSString *title;
	NSString *subtitle;
    NSString *category;
    NSString *description;
}

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *description;



@end