//
//  DiningInfoViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import "DiningInfoViewController.h"
#import "DiningWebsiteViewController.h"
#import "NetworkReachability.h"
#import "Macro.h"

@implementation DiningInfoViewController

@synthesize descriptionLabel, descriptionBubble, diningImage, contentScroll, sameDining;
@synthesize myDiningWebsiteViewController, diningUrl, diningYoutube, diningCall;
@synthesize optionsButton, backgroundImage, diningLatitude, diningLongitude;

CLLocationManager *GPSlocation;
UIWebView  *webview;
double userLongitude;
double userLatitude;

- (void)viewDidLoad {
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	contentScroll.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	descriptionLabel.userInteractionEnabled = TRUE;
    optionsButton = [[UIBarButtonItem alloc] initWithTitle:@"Options" style:UIBarButtonItemStylePlain target:self action:@selector(showOptions:)];
    [[self navigationItem] setRightBarButtonItem:optionsButton];
    [optionsButton release];
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
    
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}


- (void)checkNetwork{
	
	UIAlertView *errorView;
	NetworkReachability* internetReach;
	internetReach = [[NetworkReachability reachabilityForInternetConnection] retain];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	[internetReach release];
	if (netStatus == NotReachable) {
		// Could be ReachableViaWWAN and ReachableViaWiFi
		
		//NSLog(@"NotReachable");
		
		errorView = [[UIAlertView alloc]
					 initWithTitle: @"Network Error"
					 message: @"There is no internet conection!"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
        return;
	}
}

- (void)callDining {
    if (!diningCall){
        [self checkNetwork];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        av.frame=CGRectMake(145, 230, 25, 25);
        av.tag  = 1;
        myDiningWebsiteViewController = [[DiningInfoViewController alloc] init];
        [myDiningWebsiteViewController.view addSubview:av];
        [av startAnimating];
        [[self navigationController] pushViewController:myDiningWebsiteViewController animated:YES];
        
        NSURL *url = [NSURL URLWithString:@"http://www.wheelingcvb.com/"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [myDiningWebsiteViewController.diningWeb loadRequest:request];
    }
    else{
        if(webview==nil)
            webview = [[UIWebView alloc] initWithFrame:self.view.frame];
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:diningCall]]]];
    }
}

- (void)bringDiningWebsiteView {
	
    if (!diningUrl){
        [self checkNetwork];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        av.frame=CGRectMake(145, 230, 25, 25);
        av.tag  = 1;
        myDiningWebsiteViewController = [[DiningWebsiteViewController alloc] init];
        [myDiningWebsiteViewController.view addSubview:av];
        [av startAnimating];
        [[self navigationController] pushViewController:myDiningWebsiteViewController animated:YES];
        
        NSURL *url = [NSURL URLWithString:@"http://www.wheelingcvb.com/"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [myDiningWebsiteViewController.diningWeb loadRequest:request];
    }
    else{
        [self checkNetwork];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        av.frame=CGRectMake(145, 230, 25, 25);
        av.tag  = 1;
        myDiningWebsiteViewController = [[DiningWebsiteViewController alloc] init];
        [myDiningWebsiteViewController.view addSubview:av];
        [av startAnimating];
        [[self navigationController] pushViewController:myDiningWebsiteViewController animated:YES];
        
        NSURL *url = [NSURL URLWithString:[@"http://" stringByAppendingString:diningUrl]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [myDiningWebsiteViewController.diningWeb loadRequest:request];
    }
	
}

- (void)bringDiningVideoView {
    
    if (![diningYoutube length])
    {
        UIAlertView *errorView;
        errorView = [[UIAlertView alloc]
					 initWithTitle: @"No Video"
					 message: @"There is no promotional video available"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
        
    }
    else{
        [self checkNetwork];
        YouTubePlayerController * yPlayer=[[YouTubePlayerController alloc] initWithNibName:@"YouTubePlayerController" bundle:nil];
        [yPlayer setWeburl:diningYoutube];
        [self.navigationController pushViewController:yPlayer animated:YES];
        [yPlayer release];
        
    }
}

- (void)showOptions:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call", @"Website", @"Video", @"Get me there!", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
        [self callDining];
    }
    else if (buttonIndex == 1) {
        [self bringDiningWebsiteView];
    }
    else if (buttonIndex == 2) {
        [self bringDiningVideoView];
	}
    else if (buttonIndex == 3) {
        // ToDo: GoogleMaps
        [self bringGoogleMaps];
    }
}

-(void)bringGoogleMaps
{
    if(diningLatitude!=nil || diningLongitude!=nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Go to that location" message:@"This will open the Maps App" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No location" message:@"There is no location information" delegate:self
                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    userLatitude=newLocation.coordinate.latitude;
    userLongitude=newLocation.coordinate.longitude;
    [GPSlocation stopUpdatingLocation];
    [GPSlocation release];
    [self openMap:[NSString stringWithFormat:@"saddr=%f,%f&daddr=%@,%@&output=dragdir",userLatitude,userLongitude,diningLatitude,diningLongitude]];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex!=0)
    {
        GPSlocation=[[CLLocationManager alloc]init];
        GPSlocation.delegate=self;
        [GPSlocation startUpdatingLocation];
    }
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
	[descriptionLabel release];descriptionLabel=nil;
	[diningImage release];diningImage=nil;
    [webview release];webview=nil;
	//[contentScroll release];
	[super dealloc];
}
#pragma mark - Map Method

-(void)openMap:(NSString*)urlString{
    NSString *url;
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
    {
        url = [NSString stringWithFormat:@"http://maps.google.com/maps?%@",urlString];
    }
    else
    {
        url = [NSString stringWithFormat:@"http://maps.apple.com/maps?%@",urlString];
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

@end

