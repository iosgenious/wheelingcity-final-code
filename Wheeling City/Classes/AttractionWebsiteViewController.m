//
//  AttractionWebsiteViewController.m
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//

#import "AttractionWebsiteViewController.h"

//
@implementation AttractionWebsiteViewController

@synthesize attractionWeb;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	attractionWeb.delegate = self;
	self.title = @"Website";
    attractionWeb.scalesPageToFit = YES;
	UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithTitle:@"Options" style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
	[[self navigationItem] setRightBarButtonItem:reloadButton];
	[reloadButton release];
	//[super viewDidLoad];
}

- (void)goBack:(id) sender {
    [attractionWeb goBack];
}

-(void) viewDidAppear:(BOOL)animated {
	self.navigationController.navigationBarHidden = FALSE;
	UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}

- (void) viewDidDisappear:(BOOL)animated {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;	
    UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}
- (void)webViewDidStartLoad:(UIWebView *)webView 
{
	//NSLog(@"web starts");

}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	[self blankPage];
	// Release any cached data, images, etc that aren't in use.
}

- (void) blankPage {
	[attractionWeb loadHTMLString:@"<html><head></head><body></body></html>" baseURL:nil];
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[attractionWeb release];
    //[super dealloc];
}


@end
