//
//  Dining.m
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//

#import "Resource.h"



@implementation Resource

@synthesize resourceId, type, organization, location, phone, website, image, description, video,longitude,latitude;  

- (id) initWithName:(NSInteger)in_ResourceId type:(NSString *)in_Type organization:(NSString *)in_Organization
           location:(NSString *)in_Location phone:(NSString *)in_Phone website:(NSString *)in_Website
              image:(NSString *)in_Image description:(NSString *)in_Description  video:(NSString *)in_Video longitude:(NSString *)in_Longitude latitude:(NSString *)in_Latitude{
	
	self = [super init];
	if(self)
	{
		self.resourceId = in_ResourceId;
		self.type = in_Type;
		self.organization = in_Organization;
        self.location = in_Location;
		self.phone = in_Phone;
		self.website = in_Website;
        self.image = in_Image;
		self.description = in_Description;
		self.video = in_Video;
        self.longitude = in_Longitude;
        self.latitude = in_Latitude;

	}
	return self;
}


- (void) dealloc {
	
	[type release];
    [organization release];
	[image release];
	[description release];
    [location release];
    [phone release];
    [website release];
    [video release];
	[super dealloc];
}

@end
