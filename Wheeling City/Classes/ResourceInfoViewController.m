//
//  ResourceInfoViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import "ResourceInfoViewController.h"
#import "NetworkReachability.h"


@implementation ResourceInfoViewController

@synthesize descriptionLabel, descriptionBubble, resourceImage, contentScroll, sameResource;
@synthesize optionsButton, resourceCall, resourceURL, resourceYoutube, resourceLatitude,resourceLongitude;
@synthesize myResourceWebsiteViewController;

CLLocationManager *GPSlocation;
UIWebView  *webview;
double userLongitude;
double userLatitude;

- (void)viewDidLoad {
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	contentScroll.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	descriptionLabel.userInteractionEnabled = TRUE;
    optionsButton = [[UIBarButtonItem alloc] initWithTitle:@"Options" style:UIBarButtonItemStylePlain target:self action:@selector(showOptions:)];
    [[self navigationItem] setRightBarButtonItem:optionsButton];
    [optionsButton release];
}

-(void)viewDidDisappear:(BOOL)animated {
   // [self release];
}
	
- (void)showOptions:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call", @"Website", @"Video", @"Get me there!", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
        [self callResource];
    } 
    else if (buttonIndex == 1) {
        [self bringResourceWebsiteView];
    } 
    else if (buttonIndex == 2) {
        [self bringResourceVideoView];
	} 
    else if (buttonIndex == 3) {
        // ToDo: GoogleMaps
        [self bringGoogleMaps];
    }
}

- (void)callResource {
    if (!resourceCall){
        UIAlertView *errorView;
        errorView = [[UIAlertView alloc] 
					 initWithTitle: @"No phone number" 
					 message: @"Contact number not available" 
					 delegate: self 
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];  
		[errorView show];
		[errorView autorelease];
        
    }
    else{
        if(webview==nil)
            webview = [[UIWebView alloc] initWithFrame:self.view.frame];
        [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[@"tel:" stringByAppendingString:resourceCall]]]];
    }
}

- (void)checkNetwork{
	
	UIAlertView *errorView;
	NetworkReachability* internetReach;
	internetReach = [[NetworkReachability reachabilityForInternetConnection] retain];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	[internetReach release];
	if (netStatus == NotReachable) {
		// Could be ReachableViaWWAN and ReachableViaWiFi
		
		//NSLog(@"NotReachable");
		
		errorView = [[UIAlertView alloc] 
					 initWithTitle: @"Network Error" 
					 message: @"There is no internet conection!" 
					 delegate: self 
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];  
		[errorView show];
		[errorView autorelease];
	}
}

-(void)bringGoogleMaps
{
    if(resourceLatitude!=nil || resourceLongitude!=nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Go to that location" message:@"This will open the Maps App" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No location" message:@"There is no location information" delegate:self 
                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {

    userLatitude=newLocation.coordinate.latitude;
    userLongitude=newLocation.coordinate.longitude;
    [GPSlocation stopUpdatingLocation];
    [GPSlocation release];
    [self openMap:[NSString stringWithFormat:@"saddr=%f,%f&daddr=%@,%@&output=dragdir",userLatitude,userLongitude,resourceLatitude,resourceLongitude]];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex!=0)
    {
        GPSlocation=[[CLLocationManager alloc]init];
        GPSlocation.delegate=self;
        [GPSlocation startUpdatingLocation];
    }
}

- (void)bringResourceWebsiteView {
	
    if (!resourceURL){
        [self checkNetwork];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        av.frame=CGRectMake(145, 230, 25, 25);
        av.tag  = 1;
        myResourceWebsiteViewController = [[ResourceWebsiteViewController alloc] init];
        [myResourceWebsiteViewController.view addSubview:av];
        [av startAnimating];	
        [[self navigationController] pushViewController:myResourceWebsiteViewController animated:YES];
        
        NSURL *url = [NSURL URLWithString:@"http://www.wheelingcvb.com/"];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [myResourceWebsiteViewController.resourceWeb loadRequest:request];
    }
    else{
        [self checkNetwork];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
        av.frame=CGRectMake(145, 230, 25, 25);
        av.tag  = 1;
        myResourceWebsiteViewController = [[ResourceWebsiteViewController alloc] init];
        [myResourceWebsiteViewController.view addSubview:av];
        [av startAnimating];	
        [[self navigationController] pushViewController:myResourceWebsiteViewController animated:YES];
        
        NSURL *url = [NSURL URLWithString:resourceURL];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [myResourceWebsiteViewController.resourceWeb loadRequest:request];
    }
	
}

- (void)bringResourceVideoView {
    
    if (!resourceYoutube)
    {
        UIAlertView *errorView;
        errorView = [[UIAlertView alloc] 
					 initWithTitle: @"No Video" 
					 message: @"There is no promotional video available" 
					 delegate: self 
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];  
		[errorView show];
		[errorView autorelease];
        
    }
    else{
        [self checkNetwork];
    }
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


#pragma mark - Memeory Management Method

- (void)didReceiveMemoryWarning {
   [super didReceiveMemoryWarning];
}


-(void)viewDidUnload{
    [super viewDidUnload];
    [descriptionLabel release];self.descriptionLabel=nil;
	[resourceImage release];self.resourceImage=nil;
	[contentScroll release];self.contentScroll=nil;
	[super dealloc];

}
- (void)dealloc {
	[descriptionLabel release];self.descriptionLabel=nil;
	[resourceImage release];self.resourceImage=nil;
	[contentScroll release];self.contentScroll=nil;
	[super dealloc];
}

#pragma mark - Map Method

-(void)openMap:(NSString*)urlString{
    NSString *url;
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
    {
        url = [NSString stringWithFormat:@"http://maps.google.com/maps?%@",urlString];
    }
    else
    {
        url = [NSString stringWithFormat:@"http://maps.apple.com/maps?%@",urlString];
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}


@end

