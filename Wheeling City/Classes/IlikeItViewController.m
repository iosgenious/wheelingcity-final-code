//
//  IlikeItViewController.m
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 23/03/11.
//  Copyright 2011 W. All rights reserved.
//

#import "IlikeItViewController.h"
#import "Macro.h"
#import <FacebookSDK/FacebookSDK.h>

@implementation IlikeItViewController

NSString *facebookPageIdent=@"113425338672809";

void alertBoxWithMessage (NSString *errorTitle, NSString *message){
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:errorTitle message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertView show];
    [alertView release];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [_facebookLike release];
    [self.facebookLike setDelegate:nil];
    self.facebookLike=nil;
    
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.facebookLike setDelegate:self];
    [self showLikeButton];
    [self likeTheFanPage];
    
    
    
    NSString *jithurl=@"http://www.wheelingwv.gov";
    NSString *commentString = [NSString stringWithFormat:@"Wheeling City Council\rCity-County Building\r1500 Chapline Street, Suite 301\rWheeling, WV 26003\r(304) 234-3604\r%@",jithurl];
    infoTextView.text=commentString;
    infoTextView.textColor=[UIColor blueColor];
    infoTextView.font=[UIFont systemFontOfSize:19];
    self.navigationItem.title = @"I like Wheeling City";
    
    // Do any additional setup after loading the view from its nib.
}
/**
 It did a request to obtain the facebook I like Button of JITH
 */
- (void)showLikeButton {
    
    NSString *likeButtonIframe =@"<iframe src=\"http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2FWheelingWV&amp;layout=box_count&amp;show_faces=true&amp;width=450&amp;action=like&amp;font&amp;colorscheme=light&amp;height=65\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:450px; height:65px;\" allowTransparency=\"true\"></iframe>";
    

    [self.facebookLike  loadHTMLString:likeButtonIframe baseURL:[NSURL URLWithString:@""]];
    

}

#pragma mark- MBProgress Method

-(void)addActivity{
    [self removeHUD];
    mbSpinner=[[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:mbSpinner];
    mbSpinner.delegate=self;
    mbSpinner.labelText=@"Updating";
    [mbSpinner show:true];
    
}
- (void)hudWasHidden {
    // Remove HUD from screen when the HUD was hidded
    [mbSpinner removeFromSuperview];
    [mbSpinner release];
	mbSpinner = nil;
}

-(void)removeHUD{
    //    [spinner stopAnimating];
    [mbSpinner hide:true];
    [spinner release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void) likeTheFanPage {
    [self addActivity];
    
    //nsurlrequest
    NSString *urlString =[NSString stringWithFormat:@"https://graph.facebook.com/%@/likes?access_token=%@",facebookPageId,[[FBSession.activeSession accessTokenData] accessToken]];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [NSURLConnection connectionWithRequest:request delegate:self ];
    
}


#pragma maek UIWebView Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}


#pragma mark---------------- NSURLConnection delegate methods-------------

- (NSURLRequest *)connection:(NSURLConnection *)connection
 			 willSendRequest:(NSURLRequest *)request
 			redirectResponse:(NSURLResponse *)redirectResponse {
	return request;
}


- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response {
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int code = [httpResponse statusCode];
    //NSLog(@"the response code is %d",code);
    if (code >=200 && code <=299) {
        self.responseData = [NSMutableData data];
    }
    else if (code >=400 && code <=499) {
        [theConnection cancel];
        
    }
    else {
        [theConnection cancel];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)theConnection didFailWithError:(NSError *)error {
    [self removeHUD];
    NSString *message = [NSString stringWithFormat:@"Connection is terminated because %@",[error localizedDescription]];
    alertBoxWithMessage(@"Error!", message);
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self removeHUD];
//        NSString *json1 = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
//        
//    NSLog(@"json1 %@",json1);
//    [self removeHUD];
    
}




@end
