//
//  HomePagerViewController.h
//  JITH
//
//  Created by Santosh Singh on 05/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomePagerViewController : UIViewController <UIScrollViewDelegate>
{
	NSArray *imageSet;
	
	UIImageView *view1;
	UIImageView *view2;
	
	int view1Index;
	int view2Index;
	
	UIScrollView *scroll;
    UIView* dissapear;
}

@property (nonatomic,retain) UIView* dissapear;

- (void) setImages:(NSArray *) images;
@end
