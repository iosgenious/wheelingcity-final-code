//
//  UIPickerViewAppDelegate.m
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 11/04/11.
//  Copyright 2011 W. All rights reserved.
//

#import "UIPickerViewAppDelegate.h"
#import "SiteMapViewController.h"





@implementation UIPickerViewAppDelegate



@synthesize window;

@synthesize mviewController;



- (void)applicationDidFinishLaunching:(UIApplication *)application

{
    
    mviewController = [[SiteMapViewController alloc] initWithNibName:@"SiteMapViewController" bundle:[NSBundle mainBundle] ];
    
    [window addSubview:mviewController.view];
    
    
    [window makeKeyAndVisible];
    
}



- (void)dealloc

{
    
    [window release];
    
    [mviewController release];
    
    [super dealloc];
    
}





@end
