//
//  LodgingViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Modified by Denis Santxez
//  Copyright 2010 W. All rights reserved.
//

#import "LodgingViewController.h"
#import "Lodging.h"
#import "JITHAppDelegate.h"
#import "FirstViewController.h"
#import "Macro.h"

@interface LoadingCell : UITableViewCell
@end

@implementation LoadingCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(5,7, 60, 60)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
}

@end

@implementation LodgingViewController
@synthesize myLodgingInfoViewController, lodgingArray, lodgingTableView, images;

- (void)viewDidLoad {
	
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	lodgingArray = [[NSArray alloc ] initWithArray:dataBaseSingleton.lodgingArray];
	lodgingTableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	self.title = @"Lodging";
    self.images = [[NSMutableDictionary alloc] init];
    //SlideShow Starter
    slideShowController = [[PagerViewController alloc] init];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    slideShowController.view.autoresizesSubviews = YES;
    [slideShowController setImages:[NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"a252c49f40401bbe22feeb8e920e64cd.png"],
                                    nil]];
    //IMG_06042011_153114.png
    CGRect slideShowFrame=CGRectMake(0, 0, 320, 189);
    slideShowController.view.frame=slideShowFrame;
    [lodgingTableView setTableHeaderView:slideShowController.view];
    [lodgingTableView.tableHeaderView setFrame:CGRectMake(0, 0, 320, 120)];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	[self.navigationController setNavigationBarHidden:FALSE animated:NO];
    myLodgingInfoViewController = [[LodgingInfoViewController alloc] init];
}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

- (void)dealloc {
	[myLodgingInfoViewController release];myLodgingInfoViewController=nil;
	[lodgingTableView release];lodgingTableView=nil;
    [slideShowController release];slideShowController=nil;
	[lodgingArray release];lodgingArray=nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Table view data source methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	//return [lodgingArray count];
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
	return [lodgingArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	
	LoadingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (LoadingCell*)[[[LoadingCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15];// systemFontOfSize:16];
        cell.textLabel.textColor = [UIColor whiteColor];
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
        cell.detailTextLabel.textColor = [UIColor yellowColor];
        cell.textLabel.numberOfLines=0;
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        
	}
	
	Lodging *the_pLodging = [lodgingArray objectAtIndex:indexPath.row];
    cell.textLabel.text = the_pLodging.name;
    
    [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *image = [self.images objectForKey:the_pLodging.image];
        
        if(!image)
        {
            // Image is not on the cached NSMUtableDictionary. Check if image is available on the documents folder and copy from there
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString *documentsFolder = [paths objectAtIndex:0];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:the_pLodging.image]]) {
                UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pLodging.image]];
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pLodging.image];
            }
            
            else {
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[@"http://wheeling.yourmobicity.com/English/Lodging/" stringByAppendingString:the_pLodging.image]]];
                UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
                image = [self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pLodging.image];
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                
                [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:the_pLodging.image] atomically:YES];
                [unresizedImage release];
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[cell imageView] setImage:image];
            [cell setNeedsLayout];
        });
    });
    
	return cell;
}

- (UIImage*)imageWithBorderFromImage:(UIImage*)source
{
    CGSize size = [source size];
    UIGraphicsBeginImageContext(size);
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    [source drawInRect:rect blendMode:kCGBlendModeNormal alpha:1.0];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 1.0);
    CGContextStrokeRect(context, rect);
    UIImage *testImg =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return testImg;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Navigation logic may go here ‚Äî for example, create and push another view controller.
    
	Lodging *the_pLodging = [lodgingArray objectAtIndex:indexPath.row];
    
    [[self navigationController] pushViewController:myLodgingInfoViewController animated:YES];
	
	//Setting title of the artist view
	myLodgingInfoViewController.title = the_pLodging.name;
	myLodgingInfoViewController.descriptionLabel.numberOfLines = 0;
	[myLodgingInfoViewController.descriptionLabel setText:[the_pLodging description]];
    [myLodgingInfoViewController.descriptionLabel sizeToFit];
    
	myLodgingInfoViewController.contentScroll.contentSize = CGSizeMake(319,	myLodgingInfoViewController.descriptionLabel.frame.size.height + 280);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *documentsFolder = [paths objectAtIndex:0];
    
    UIImage *the_pLodgingImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pLodging.image]];
	
	//myLodgingInfoViewController.lodgingImage.image = the_pLodgingImage;
	myLodgingInfoViewController.lodgingImage.image = [self imageWithBorderFromImage:the_pLodgingImage];
    myLodgingInfoViewController.lodgingUrl = the_pLodging.website;
    myLodgingInfoViewController.lodgingCall = the_pLodging.phone;
    myLodgingInfoViewController.lodgingYoutube = the_pLodging.video;
    myLodgingInfoViewController.lodgingLatitude = the_pLodging.latitude;
    myLodgingInfoViewController.lodgingLongitude = the_pLodging.longitude;
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	previousLodgingId = currentLodgingId;
	currentLodgingId = the_pLodging.lodgingId;
	
	if (previousLodgingId != currentLodgingId){
		myLodgingInfoViewController.contentScroll.contentOffset = CGPointMake(0, 0);
		myLodgingInfoViewController.sameLodging = FALSE;
	}
	else {
		myLodgingInfoViewController.sameLodging = TRUE;
	}
    myLodgingInfoViewController.descriptionBubble.backgroundColor=[UIColor whiteColor];
    CGRect frame=myLodgingInfoViewController.descriptionLabel.frame;
    frame.size.width+=18;
    frame.size.height+=18;
    frame.origin.x-=9;
    frame.origin.y-=9;
    [myLodgingInfoViewController.descriptionBubble setFrame:frame];
    myLodgingInfoViewController.descriptionBubble.layer.cornerRadius=10;
    [myLodgingInfoViewController.descriptionBubble.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [myLodgingInfoViewController.descriptionBubble.layer setBorderWidth: 1.0];
}

- (void) flushCache {
    
    [images release];
    images = [[NSMutableDictionary alloc] init];
    
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    [self flushCache];
    // Will it create a new empty infoview controller? Apparently it does
    [myLodgingInfoViewController release];
    myLodgingInfoViewController = [[LodgingInfoViewController alloc] init];
}



@end

