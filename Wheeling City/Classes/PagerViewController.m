#import "PagerViewController.h"


@interface PagerViewController (Private)

- (void) update;

@end


@implementation PagerViewController

@synthesize dissapear;

- (void) setImages:(NSArray *) images
{
	if(imageSet) [imageSet release];
	
	imageSet = [images retain];
	
	view1.frame = CGRectMake(0, 0, 320, 189);
	view2.frame = CGRectMake(320, 0, 320, 189);
	
	view1.image = [imageSet objectAtIndex:0];
    if([imageSet count]>1)
        view2.image = [imageSet objectAtIndex:1];
	
	scroll.contentSize = CGSizeMake([imageSet count]*320, 189);
}

- (id) init
{
		scroll = [[UIScrollView alloc] init];
		scroll.scrollEnabled = YES;
		scroll.pagingEnabled = YES;
		scroll.directionalLockEnabled = NO;
		scroll.showsVerticalScrollIndicator = NO;
		scroll.showsHorizontalScrollIndicator = NO;
		scroll.delegate = self;
		scroll.backgroundColor = [UIColor colorWithRed:0.0 green:0.1 blue:0.0 alpha:0.7];
		scroll.autoresizesSubviews = YES;
		scroll.frame = CGRectMake(0, 0, 320, 189);
		[self.view addSubview:scroll];
		
		view1 = [[UIImageView alloc] init];
    
		[scroll addSubview:view1];
		
		view2 = [[UIImageView alloc] init];
		[scroll addSubview:view2];
	
	return self;
}

- (void) update
{
    if(dissapear!=nil)
        dissapear.hidden=TRUE;
	CGFloat pageWidth = 320;
	float currPos = scroll.contentOffset.x;
	
	int selectedPage = roundf(currPos / pageWidth);
	
	float truePosition = selectedPage*pageWidth;
	
	int zone = selectedPage % 2;
	
	BOOL view1Active = zone == 0;
	
	UIImageView *nextView = view1Active ? view2 : view1;
	
	int nextpage = truePosition > currPos ? selectedPage-1 : selectedPage+1;
    
	if(nextpage >= 0 && nextpage < [imageSet count])
	{
		if((view1Active && nextpage == view1Index) || (!view1Active && nextpage == view2Index)) return;
		
		
		nextView.frame = CGRectMake(nextpage*320, 0, 320, 189);
		nextView.image = [imageSet objectAtIndex:nextpage];
		
		if(view1Active) view1Index = nextpage;
		else view2Index = nextpage;
	}
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// UIScrollView Delegate ///////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

- (void) scrollViewDidScroll:(UIScrollView *) scrollView
{	
	[self update];
}

/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////// Default Methods //////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)dealloc
{
	if(imageSet) [imageSet release];
	[scroll release];
	[view1 release];
	[view2 release];
    [super dealloc];
}

@end
