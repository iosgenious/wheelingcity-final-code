#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface ParkPlaceMark : NSObject <MKAnnotation> {
	CLLocationCoordinate2D coordinate;
	NSString *mTitle;
	NSString *mSubTitle;
}

@property (nonatomic, copy) NSString *mTitle;
@property (nonatomic, copy) NSString *mSubTitle;


@end