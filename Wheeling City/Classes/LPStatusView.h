//
//  LPStatusView.h
//  layarplayer
//
//  Created by Lawrence Lee on 11/17/10.
//  Copyright (c) 2010 Layar B.V. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPStatusMessage : NSObject
{
    BOOL showActivity;
	NSString *message;
}

@property (nonatomic, assign) BOOL showActivity;
@property (nonatomic, copy) NSString *message;

+ (LPStatusMessage*)statusMessage:(NSString*)text showActivity:(BOOL)activity;

@end

@protocol LPStatusViewDelegate <NSObject>

- (int)numberOfResultsForStatusView;
- (float)accuracyForStatusView;
- (float)rangeForStatusView;

@end

typedef enum LPStatusViewStatus
{
	LPSTATUSVIEW_RESULTS = 0,
	LPSTATUSVIEW_ACCURACY,
	LPSTATUSVIEW_RANGE,
	LPSTATUSVIEW_GETTING_POI
} LPStatusViewStatus;

@interface LPStatusView : UIView
{
	id <LPStatusViewDelegate> delegate;
	NSString *errorMessage;
	LPStatusMessage *message;
	UIActivityIndicatorView *activityView;
	UILabel *messageLabel;
	NSTimeInterval timeOfLastMessageUpdate;
	NSTimer *timer;
	LPStatusViewStatus status;
	
	float minWidth;
	float minHeight;
	float maxWidth;
	float maxHeight;
}

@property (nonatomic, assign) id <LPStatusViewDelegate> delegate;
@property (nonatomic, copy) NSString *errorMessage;
@property (nonatomic, retain) LPStatusMessage *message;
@property (nonatomic, assign) float minWidth;
@property (nonatomic, assign) float minHeight;
@property (nonatomic, assign) float maxWidth;
@property (nonatomic, assign) float maxHeight;

- (void)setMessageTextFromMessage:(LPStatusMessage*)aMessage;
- (void)statusTimerFired:(NSTimer*)aTimer;
- (void)invalidateTimer;

@end
