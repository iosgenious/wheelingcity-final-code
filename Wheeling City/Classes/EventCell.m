//
//  EventCell.m
//  JITH
//
//  Created by Santosh on 15/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import "EventCell.h"

@implementation EventCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)dealloc{
    self.lblLocation=nil;
    self.lblName=nil;
    self.lblTime=nil;
    [super dealloc];
}
@end
