//
//  WhatsHappeningInfoViewController.m
//  JITH
//
//  Created by Ryan Wall on 4/18/11.
//  Copyright 2011 W. All rights reserved.
//

#import "WhatsHappeningInfoViewController.h"


@implementation WhatsHappeningInfoViewController

@synthesize text,currentEvent,currentDay,shareButton,cvbWebViewController,postInFacebook,contentScroll;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.navigationItem.title = @"Event Information";
    NSMutableString* myText=[[NSMutableString alloc]init];
    [myText appendFormat:@"Title: %@\n",[self.eventInfo eventTitle]];
    [myText appendFormat:@"\nDescription: %@\n",[self.eventInfo eventDescription]];
    [myText appendFormat:@"\nDay: %d\n", currentDay];
    if ([self.eventInfo eventStartTime]!=nil)
        [myText appendFormat:@"\nStarts at: %@\n",[dateFormatter stringFromDate:[self.eventInfo eventStartTime]]];
    if ([self.eventInfo eventFinishTime]!=nil)
        [myText appendFormat:@"\nFinishes at: %@\n",[dateFormatter stringFromDate:[self.eventInfo eventFinishTime]]];
    //[text setText:myText];
    text.numberOfLines = 0;
	[text setText:myText];
    [text sizeToFit];
    
	contentScroll.contentSize = CGSizeMake(text.frame.size.width,text.frame.size.height+44);
    [dateFormatter release];
    [myText release];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) shareButtonPushed:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share event" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Email",@"Text",@"Add to iPhone Calendar", @"CVB Website", nil];
    [actionSheet showInView:self.view];
    [actionSheet release];
}

/**
 ActionSheet is the  method selector for the last method. here is selected to take a photo with the camera or to select one picture of the library
 
 */
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    [actionSheet dismissWithClickedButtonIndex:5 animated:YES];
	if (buttonIndex == 0) {
        postInFacebook=[[NewsFeedViewController alloc]init];
        [postInFacebook shareOnFB:[self.eventInfo eventTitle]];
        [postInFacebook release];
        postInFacebook=nil;
	} else if (buttonIndex == 1) {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:[self.eventInfo eventTitle]];
        [controller setMessageBody:[NSString stringWithFormat:@"Title: %@\n Description: %@\n Starts at: %@\n Finishes at: %@",[self.eventInfo eventTitle],[self.eventInfo eventDescription],[self.eventInfo eventStartTime],[self.eventInfo eventFinishTime]] isHTML:NO];
        if (controller) [self presentModalViewController:controller animated:YES];
        [controller release];
	} 
    else if (buttonIndex ==2)
    {
        MFMessageComposeViewController* sms = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            sms.body = [NSString stringWithFormat:@"Title: %@\n Description: %@\n Starts at: %@\n Finishes at: %@",[self.eventInfo eventTitle],[self.eventInfo eventDescription],[self.eventInfo eventStartTime],[self.eventInfo eventFinishTime]];
            sms.messageComposeDelegate = self;
            [self presentModalViewController:sms animated:YES];
        }
        [sms release];
        
    }
    else if (buttonIndex == 3)
    {
        [self saveEventToiOSCalender];
    }
    else if (buttonIndex == 4)
    {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            UIActivityIndicatorView  *av = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge] autorelease];
            av.frame=CGRectMake(145, 230, 25, 25);
            av.tag  = 1;
            cvbWebViewController = [[LodgingWebsiteViewController alloc] init];
            [cvbWebViewController.view addSubview:av];
            [av startAnimating];	
            [[self navigationController] pushViewController:cvbWebViewController animated:YES];
            
            NSURL *url = [NSURL URLWithString:@"http://www.wheelingcvb.com/"];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            [cvbWebViewController.lodgingWeb loadRequest:request];
        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller  
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError*)error;
{
    [self dismissModalViewControllerAnimated:YES];
}


/*
 * Method to save Event in Calender
 */


-(void)saveEventToiOSCalender{
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    
    __block BOOL accessGranted = NO;

    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            accessGranted = granted;
            dispatch_semaphore_signal(sema);
        }];
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    } else { // we're on iOS 5 or older
        accessGranted = YES;
    }
    
    if (accessGranted) {
        [self createNewEvent:eventStore];
    }
}

-(void)createNewEvent:(EKEventStore*)eventStore{
    EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
    event.title     = [self.eventInfo eventTitle];
    event.notes     = [self.eventInfo eventDescription];
    //starting date
    event.startDate = [self.eventInfo eventStartTime];
    event.endDate   = [self.eventInfo eventFinishTime];
    
    [event setCalendar:[eventStore defaultCalendarForNewEvents]];
    NSError *error;
    [eventStore saveEvent:event span:EKSpanThisEvent error:&error];
    if (!error) {
        UIAlertView * msgAlertView=[[UIAlertView alloc] initWithTitle:@"WheelingCity" message:@"Event added to iPhone Calender ." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];;
        [msgAlertView show];
        [msgAlertView release];
    }
    [eventStore release];
}


@end
