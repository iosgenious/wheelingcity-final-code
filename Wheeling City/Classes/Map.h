//
//  Map.h
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 04/05/11.
//  Copyright 2011 W. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Map : NSObject {
    
	NSString *name;
	NSString *address;	
    NSString *latitude;
    NSString *longitude;
    NSString *category;
    NSString *description;
    
}

@property (nonatomic, retain) NSString *name, *address,*latitude, *longitude,*category,*description; 

- (id) initWithName:(NSString *)in_Name address:(NSString *)in_Address latitude:(NSString *)in_Latitude longitude:(NSString *)in_Longitude category:(NSString *)in_Category description:(NSString *)in_Description; 

@end
