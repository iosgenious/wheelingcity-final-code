//
//  YouTubePlayerController.h
//  JITH
//
//  Created by Santosh Singh on 07/08/13.
//  Copyright (c) 2013 W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YouTubePlayerController : UIViewController
{
    
    UIButton *button;
    IBOutlet UIActivityIndicatorView *lodingActivity;
    
}

@property(nonatomic,retain)NSString *toptitle;
@property(nonatomic,retain) NSString *weburl;
@property(nonatomic,retain)IBOutlet UIWebView *videoWebView;@end
