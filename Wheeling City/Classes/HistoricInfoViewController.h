//
//  HistoricInfoViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DataBaseSingleton.h"
#import "PagerViewController.h"

@interface HistoricInfoViewController : UIViewController<CLLocationManagerDelegate,UIActionSheetDelegate> {

	DataBaseSingleton *dataBaseSingleton;
    UILabel *descriptionLabel;
    UIImageView *descriptionBubble;
    UIImageView *historicImage;
    UIScrollView *contentScroll;
    UIBarButtonItem *actionButton;
    NSString *historicYoutube;
    NSString *historicLongitude;
    NSString *historicLatitude;
	BOOL sameHistoric;

}
@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UIImageView *descriptionBubble;
@property (nonatomic, retain) IBOutlet UIImageView *historicImage;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScroll;
@property (nonatomic, assign) UIBarButtonItem *actionButton;
@property (nonatomic, retain) NSString *historicYoutube;
@property (nonatomic, retain) NSString *historicLongitude;
@property (nonatomic, retain) NSString *historicLatitude;
@property (nonatomic, assign) BOOL sameHistoric;

- (void)bringHistoricVideoView;
- (void)bringGoogleMaps;

@end
