//
//  DiningViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Modified by Denis Santxez 
//  Copyright 2010 W. All rights reserved.
//

#import "DiningViewController.h"
#import "Dining.h"
#import "JITHAppDelegate.h"
#import "FirstViewController.h"

#import "Macro.h"

@interface DiningCell : UITableViewCell
@end

@implementation DiningCell

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.imageView setFrame:CGRectMake(4,4, 36, 36)];
    [[self.imageView layer] setShadowColor:SHADOW_COLOR];
    [[self.imageView layer] setShadowOffset:SHADOW_OFFSET];
    [[self.imageView layer] setShadowOpacity:OPTICITY];
    [[self.imageView layer] setShadowRadius:RADIUS];
    [[self.imageView layer] setBorderWidth:BORDER_WIDTH];
    [[self.imageView layer]setBorderColor:BORDER_COLOR];
    [self.textLabel setFrame:CGRectMake(50, 7, 240, 30)];
    //self.textLabel.textAlignment = UITextAlignmentRight;
	//[[self textLabel] setLineBreakMode:UILineBreakModeWordWrap];
   // self.textLabel.numberOfLines=0;

}

@end


@implementation DiningViewController
@synthesize myDiningInfoViewController, diningDict, diningTableView, images;

- (void)viewDidLoad {
	
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
	diningDict = [[NSDictionary alloc ] initWithDictionary:dataBaseSingleton.diningDict];
	diningTableView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	self.title = @"Dining";
    self.images = [[NSMutableDictionary alloc] init];
    //SlideShow Starter
    slideShowController = [[PagerViewController alloc] init];
    slideShowController.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [slideShowController.view setBackgroundColor:[UIColor redColor]];
    slideShowController.view.autoresizesSubviews = YES;
    [slideShowController setImages:[NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"oglebay park pictures 035.png"],
                                    [UIImage imageNamed:@"oglebay_park_pictures_013.jpg"],
                                    nil]];
    CGRect slideShowFrame=CGRectMake(0, 0, 320, 189);
    slideShowController.view.frame=slideShowFrame;
    [diningTableView setTableHeaderView:slideShowController.view];
    [diningTableView setNeedsDisplay];
    diningTableView.tableHeaderView.autoresizesSubviews = YES;
    [diningTableView.tableHeaderView setFrame:CGRectMake(0, 0, 320, 120)];
    flipButton = [[UIBarButtonItem alloc] initWithTitle:@"National" style:UIBarButtonItemStylePlain target:self action:@selector(flipCategories:)];
   [[self navigationItem] setRightBarButtonItem:flipButton];
    [flipButton release];
}

- (void)flipCategories:(id)sender {
    //NSLog(@"Button tittle %@",flipButton.title);
    if ([flipButton.title isEqualToString:@"Local"]) {
        [diningTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        CGPoint point = diningTableView.contentOffset;
        point .y -= 44.0;
        [diningTableView setContentOffset:point];
        flipButton.title = @"National";
    }
    else {
        [diningTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:NO];
        CGPoint point = diningTableView.contentOffset;
        point .y -= 44.0;
        [diningTableView setContentOffset:point];
        flipButton.title = @"Local";
    }
}
    

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated {
	[self.navigationController setNavigationBarHidden:FALSE animated:NO];
    myDiningInfoViewController = [[DiningInfoViewController alloc] init];
}

- (UIImage *)resizeImage:(UIImage *)image imageWidth:(CGFloat)width imageHeight:(CGFloat)height {
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();	
    return resizedImage;
}

- (void)dealloc {
	[myDiningInfoViewController release];myDiningInfoViewController=nil;
	[diningTableView release];diningTableView=nil;
    [slideShowController release];slideShowController=nil;
	[diningDict release];diningDict=nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Table view data source methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [diningDict count];
}



- (NSString *)getNameFromSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"YES";
            break;
        case 1:
            return @"NO";
            break;
        default:
            return nil;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( section == 0)
        return @"Local";
    else
        return @"National";
}


- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
    return [[diningDict objectForKey:[self getNameFromSection:section]] count];           
               
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"Cell";
	
	DiningCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	if (cell == nil) {
		cell = (DiningCell*)[[[DiningCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        cell.textLabel.textAlignment = UITextAlignmentLeft;
        [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
        [[cell imageView] setImage:[UIImage imageNamed:@"icon-loading-animated.gif"]];
        cell.textLabel.numberOfLines=0;
	}

	
	Dining *the_pDining = [[diningDict objectForKey:[self getNameFromSection:indexPath.section]] objectAtIndex:indexPath.row];
	
	cell.textLabel.font = [UIFont systemFontOfSize:14];// systemFontOfSize:16];
	cell.textLabel.textColor = [UIColor whiteColor];
	cell.textLabel.text = the_pDining.name;
 

    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        UIImage *image = [self.images objectForKey:the_pDining.image];
        
        if(!image)
        {
            // Image is not on the cached NSMUtableDictionary. Check if image is available on the documents folder and copy from there
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            
            NSString *documentsFolder = [paths objectAtIndex:0];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:[documentsFolder stringByAppendingPathComponent:the_pDining.image]]) {
                UIImage *unresizedImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pDining.image]];     
                image = unresizedImage;//[self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pDining.image];            
            }
            
            // NSFileManager *the_pFileManager = [NSFileManager defaultManager];
            // NSLog(@" Full path of file to be created %@", [documentsFolder stringByAppendingPathComponent:the_pArtist.image]);
            else {
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[@"http://wheeling.yourmobicity.com/English/Dinning/" stringByAppendingString:the_pDining.image]]];
                UIImage *unresizedImage = [[UIImage alloc] initWithData:receivedData];
                image = unresizedImage;//[self resizeImage:unresizedImage imageWidth:75 imageHeight:75];
                [self.images setValue:image forKey:the_pDining.image];
                NSData *pngFile = UIImagePNGRepresentation(unresizedImage);
                
                [pngFile writeToFile:[documentsFolder stringByAppendingPathComponent:the_pDining.image] atomically:YES];
                [unresizedImage release];
            }
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[cell imageView] setImage:image];
            [cell setNeedsLayout];
        });
    });
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

	Dining *the_pDining = [[diningDict objectForKey:[self getNameFromSection:indexPath.section]] objectAtIndex:indexPath.row];

	[[self navigationController] pushViewController:myDiningInfoViewController animated:YES];
	//Setting title of the artist view
	myDiningInfoViewController.title = the_pDining.name;
	myDiningInfoViewController.descriptionLabel.numberOfLines = 0;
	[myDiningInfoViewController.descriptionLabel setText:[the_pDining description]];
    [myDiningInfoViewController.descriptionLabel sizeToFit];

	myDiningInfoViewController.contentScroll.contentSize = CGSizeMake(319,	myDiningInfoViewController.descriptionLabel.frame.size.height + 280);

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    NSString *documentsFolder = [paths objectAtIndex:0];
    
    UIImage *the_pDiningImage = [UIImage imageWithContentsOfFile: [documentsFolder stringByAppendingPathComponent:the_pDining.image]];     
	
	myDiningInfoViewController.diningImage.image = the_pDiningImage;	
	myDiningInfoViewController.diningUrl = the_pDining.website;
    myDiningInfoViewController.diningCall = the_pDining.phone;
    myDiningInfoViewController.diningYoutube = the_pDining.video;
    myDiningInfoViewController.diningLatitude = the_pDining.latitude;
    myDiningInfoViewController.diningLongitude = the_pDining.longitude;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
	
    
	previousDiningId = currentDiningId; 
	currentDiningId = the_pDining.diningId;
	
	if (previousDiningId != currentDiningId){
		myDiningInfoViewController.contentScroll.contentOffset = CGPointMake(0, 0);
		myDiningInfoViewController.sameDining = FALSE;
	}
	else {
		myDiningInfoViewController.sameDining = TRUE;
	}
    myDiningInfoViewController.descriptionBubble.backgroundColor=[UIColor whiteColor];
    CGRect frame=myDiningInfoViewController.descriptionLabel.frame;
    frame.size.width+=18;
    frame.size.height+=18;
    frame.origin.x-=9;
    frame.origin.y-=9;
    [myDiningInfoViewController.descriptionBubble setFrame:frame];
    myDiningInfoViewController.descriptionBubble.layer.cornerRadius=10;
    [myDiningInfoViewController.descriptionBubble.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [myDiningInfoViewController.descriptionBubble.layer setBorderWidth: 1.0];
}

- (void) flushCache {

    [images release];
    images = [[NSMutableDictionary alloc] init];
    
}
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    
    [self flushCache];
    [super didReceiveMemoryWarning];
    // Will it create a new empty infoview controller? Apparently it does
    [myDiningInfoViewController release];
    myDiningInfoViewController = [[DiningInfoViewController alloc] init];
}
@end

