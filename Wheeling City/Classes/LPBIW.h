//
//  LPBIW.h
//  layarplayer
//
//  Created by Lawrence Lee on 10/23/10.
//  Copyright (c) 2010 Layar B.V. All rights reserved.
//

//Local imports
#import "LPBIWBase.h"

@class LPBIW;

@protocol LPBIWDelegate <NSObject>

- (void)biwClicked:(LPBIW*)biw;

@end

@interface LPBIW : LPBIWBase
{
	id <LPBIWDelegate> delegate;
}

@property (nonatomic, assign) id <LPBIWDelegate> delegate;

//- (void)focusedPoiUpdated:(NSNotification*)notification;

@end
