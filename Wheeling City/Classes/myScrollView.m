//
//  myScrollView.m
//  JITH
//
//  Created by Urko Fernandez on 6/7/10.
//  Copyright 2010 W. All rights reserved.
//

#import "myScrollView.h"


@implementation myScrollView

-(void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event 
{	
	if (!self.dragging) {
		[self.nextResponder touchesEnded: touches withEvent:event]; 
	}		
	
	[super touchesEnded: touches withEvent: event];
	 
}


@end
