//
//  AttractionInfoViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "DataBaseSingleton.h"
#import "AttractionWebsiteViewController.h"


@interface AttractionInfoViewController : UIViewController <UIActionSheetDelegate,CLLocationManagerDelegate> {

	DataBaseSingleton *dataBaseSingleton;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UIImageView* bubble;
    UIImageView *attractionImage;
    UIScrollView *contentScroll;
    AttractionWebsiteViewController *myAttractionWebsiteViewController;
	BOOL sameAttraction;
    UIBarButtonItem *actionButton;
    NSString *attractionCall;
    NSString *attractionURL;
    NSString *attractionYoutube;
    NSString *attractionLongitude;
    NSString *attractionLatitude;

}
@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UIImageView *attractionImage;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScroll;
@property (nonatomic, assign) BOOL sameAttraction;
@property (nonatomic, assign) AttractionWebsiteViewController *myAttractionWebsiteViewController;
@property (nonatomic, assign) UIBarButtonItem *actionButton;
@property (nonatomic, assign) NSString *attractionCall;
@property (nonatomic, assign) NSString *attractionURL;
@property (nonatomic, assign) NSString *attractionYoutube;
@property (nonatomic, assign) NSString *attractionLongitude;
@property (nonatomic, assign) NSString *attractionLatitude;
@property (nonatomic, retain) IBOutlet UIImageView* bubble;

- (void)callAttraction;
- (void)bringAttractoinWebsiteView;
- (void)bringAttractionVideoView;
- (void)bringGoogleMaps;
@end
