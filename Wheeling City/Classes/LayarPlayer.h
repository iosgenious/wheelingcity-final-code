/**
 * @file LayarPlayer.h
 * @brief Main include file of the LayarPlayer SDK
 *
 * Include this file to include all public header files of the LayarPlayer SDK
 * By implamenting the LayarPlayer in your application, you agree to the Terms and Conditions described in the Layar_Software_Development_Kit_License_Agreement.pdf
 * file which can be found in the LayarPlayer documentation folder
 *
 * @author Lawrence Lee, Layar B.V.
 * @date 25th October 2010
 */

#import "LPViewControllerBase.h"
#import "LPAugmentedRealityViewController.h"
#import "LayarPlayerDelegate.h"

/**
 * @mainpage LayarPlayer SDK Documentation
 *
 * @section introduction Introduction
 *
 * The LayarPlayer SDK is a static library that implements the core functionality of Layar. Using the LayarPlayer SDK it is
 * possible to load a layer and present the Augmented Reality view to the user. The LayarPlayer SDK will start all necessary
 * services such as location management and device motion control.
 *
 * @section minimumrequirements Minimum requirements
 *
 * The LayarPlayer SDK requires a minimum installed iOS version of 4.0, and that the device capabilities include OpenGL ES 2 support,
 * a camera, magnetometer, gps and an accelerometer. The LayarPlayer SDK is also built for ARM7 architecture.
 * The application in which the LayarPlayer SDK is used should reflect this by specifying the folowing values
 * in the applications .plist for the key UIRequiredDeviceCapabilities:
 *
 * @li opengles-2
 * @li video-camera
 * @li accelerometer
 * @li magnetometer
 * @li gps
 * @li armv7
 *
 * @section usage Usage
 *
 * In order to use the LayarPlayer SDK in an application, simply import the LayarPlayer.h in your application. When you wish to launch the
 * Layar Augmented Reality view, initialize an instance of the LPAugmentedRealityViewController. It is imperative that you present the
 * LPAugmentedRealityViewController modally in order for it to function correctly.
 * Once you have presented the Augmented Reality View, call the loadLayerWithName method with the name of the layer that you wish to load
 * as its argument. The LayarPlayer SDK will manage the loading of the layer as well as all POI's and their resources.
 * The LayerPlayer also supports backgrounding, and will pause all OpenGL rendering as well as relenquishing all device hardware usage.
 *
 * @subsection projectconfiguration Project configuration
 *
 * Any project using the LayarPlayer SDK must have the "Minimum OS Version" set to 4.0. Additionally, it must include the following Frameworks:
 *
 * @li AudioToolbox
 * @li AVFoundation
 * @li CFNetwork
 * @li CoreGraphics
 * @li CoreLocation
 * @li CoreMotion
 * @li Foundation
 * @li MapKit
 * @li MediaPlayer
 * @li OpenGLES
 * @li QuartzCore
 * @li UIKit
 * @li SystemConfiguration
 *
 * @subsubsection armv7 Armv7 build configuration
 *
 * When building a armv7 application the following steps must be taken to ensure that the project links against the "liblayarplayer.a" static library:
 *
 * The project settings should have the following changes made:
 *
 * @li The "Architectures" option set to "Optimized (armv7)"
 * @li The "Valid Architectures" option should only contain "armv7"
 * @li The "Header search paths" option must point to the location of the "include" directory contained by the SDK
 * @li If you wish to use the "#import <...>" import directive, you must ensure that the "Always search user paths" option is enabled
 * @li The "Library search paths" option must point to the directory containing the "liblayerplayer.a" static library
 * @li The "Other linker flags" option must contain the flags "-ObjC" and "-all_load"
 * @li The "Other C Flags" option should contain the "-fblocks" flag in order for the Clang static analyzer to be able to parse files
 * that include UIKit header files
 *
 * The application target that is using the static library should have the following change made to it:
 *
 * @li The "liblayarplayer.a" static library must be added under the "Link Binary With Libraries" build phase
 *
 * @subsubsection universal Universal build configuration
 *
 * The following steps must be taken in order to build a universal binary in which only the armv7 build links against the "liblayarplayer.a" static library:
 *
 * @li Do not add "liblayarplayer.a" to your xcode-project
 * @li Change the link type to "weak" for all Frameworks required by the LayarPlayer. This can be done via “Targets” -> \<yourTarget\> -> ”Get Info” -> “General”
 * @li In the build tab add the path to "liblayarplayer.a" to the “Library Search Paths” option
 * @li In the build tab add the path to the LayarPlayer header files to the “Header Search Paths” option
 * @li For the option “Other Linker Flags” select the whole line. On the bottom left of the window is a button.
 * Press on it and select “Add Build Setting Condition” in the next window.
 * Set the condition to “Any iOS Device” and “ARMv7”. Add all the linker flags you need in addition to the -"ObjC" and "-all_load" options and add “-llayarplayer”
 * @li You will need to examine the _ARM_ARCH_7 preprocessor macro to see if you are compiling for for the armv7 build and only call code to launch the LayarPlayer
 * from there. You can do this by wrapping your code to initialize LayarPlayer objects as follows:
 * @code #ifdef _ARM_ARCH_7
 * ...
 * #endif @endcode
 *
 * @subsection example Example
 *
 * The following code snippet can be used to present an Augmented Reality View and load a layer:
 *
 * @code
 * #import <LayarPlayer.h>
 * ...
 * NSString *layerName = ...;
 * NSString *consumerKey = ...;
 * NSString *consumerSecret = ...;
 *
 * NSArray *oauthKeys = [NSArray arrayWithObjects:LPConsumerKeyParameterKey, LPConsumerSecretParameterKey, nil];
 * NSArray *oauthValues = [NSArray arrayWithObjects:consumerKey, consumerSecret, nil];
 * NSDictionary *oauthParameters = [NSDictionary dictionaryWithObjects:oauthValues forKeys:oauthKeys];
 *
 * NSArray *layerKeys = [NSArray arrayWithObject:@"radius"];
 * NSArray *layerValues = [NSArray arrayWithObject:@"1000"];
 * NSDictionary *layerFilters = [NSDictionary dictionaryWithObjects:layerValues forKeys:layerKeys];
 *
 * LPAugmentedRealityViewController *augmentedRealityViewController = [[[LPAugmentedRealityViewController alloc] init] autorelease];
 * augmentedRealityViewController.delegate = self;
 * [self presentModalViewController:augmentedRealityViewController animated:YES];
 * [augmentedRealityViewController loadLayerWithName:layerName oauthParameters:oauthParameters layerFilters:layerFilters options:LPMapViewDisabled | LPListViewDisabled];
 * @endcode
 *
 * The above example disables the menu options for the user to navigate to the map and list views. Use the default option @c LPAllViewsEnabled to enable navigation to all views.
 *
 * @subsubsection oauth OAuth signing
 *
 * All requests made by the LayarPlayer need have a valid OAuth key and secret sent with them in order to successfully pass authentication checks by the Layar server.
 * This information is specific to each layer, and must be configured for the layer at http://www.layar.com/publishing/#layers
 *
 * @subsubsection delegatemethods Delegate methods
 *
 * By implementing the LayarPlayerDelegate protocol and program can receive events from the LayarPlayer. Currently two close events are sent when the user exits the program by
 * clicking the "Close" button in one of the views. If the program that calls the LayarPlayer implements the layarPlayerWillClose and the layarPlayerDidClose methods these will
 * be called before and after the program exits, respectively.
 *
 * @section launchingfromopenglapp Launching the LayarPlayer from OpenGL applications
 *
 * Extra attention must be paid to resource management when launching the LayarPlayer from an application that makes use of OpenGL to render content. The LayarPlayer is a resource
 * intensive application, and to avoid spawning memory warnings on the device it is running on, any application launching the LayarPlayer should free up as many resources as possible.
 * For OpenGL applications this means that all texture resources should be deleted, and if possible any framebuffers that are in use. Any OpenGL timers or display links should be paused
 * as well. By implementing the delegate methods and assigning a suitable delegate class, the application can rebind it's resources when the LayarPlayer closes. Care should be taken to
 * set the applications own OpenGL context when doing so. More information on OpenGL context switching can be found here:
 * http://developer.apple.com/library/ios/#documentation/3DDrawing/Conceptual/OpenGLES_ProgrammingGuide/WorkingwithOpenGLESContexts/WorkingwithOpenGLESContexts.html
 */