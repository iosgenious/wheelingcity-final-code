#import <UIKit/UIKit.h>

@interface PagerViewController : UIViewController <UIScrollViewDelegate>
{
	NSArray *imageSet;
	
	UIImageView *view1;
	UIImageView *view2;
	
	int view1Index;
	int view2Index;
	
	UIScrollView *scroll;
    UIView* dissapear;
}

@property (nonatomic,retain) UIView* dissapear;

- (void) setImages:(NSArray *) images;

@end
