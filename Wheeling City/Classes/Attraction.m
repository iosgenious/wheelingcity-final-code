//
//  Attraction.m
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//

#import "Attraction.h"



@implementation Attraction

@synthesize attractionId, name, description, category, address, phone, image, video, website,longitude,latitude;

- (id) initWithName:(NSInteger)in_AttractionId name:(NSString *)in_Name description:(NSString *)in_Description 
           category:(NSString *)in_Category address:(NSString *)in_Address phone:(NSString *)in_Phone image:(NSString *)in_Image
              video:(NSString *)in_Video website:(NSString *)in_Website longitude:(NSString *)in_longitude latitude:(NSString *)in_latitude{
	
	self = [super init];
	if(self)
	{
		self.attractionId = in_AttractionId;
		self.name = in_Name;
		self.description = in_Description;
        self.category = in_Category;
        self.address = in_Address;
        self.phone = in_Phone;
		self.image = in_Image;
        self.video = in_Video;
        self.website = in_Website;
        self.longitude = in_longitude;
        self.latitude = in_latitude;
	}
	return self;
}


- (void) dealloc {
	
	[name release];
	[image release];
	[description release];
    [category release];
    [address release];
    [phone release];
    [website release];
    [video release];
    [latitude release];
    [longitude release];
	[super dealloc];
}

@end
