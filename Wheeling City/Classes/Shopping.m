//
//  Shopping.m
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//

#import "Shopping.h"



@implementation Shopping

@synthesize shoppingId, name, description, location, phone, image, video, area,latitude,longitude; 

- (id) initWithName:(NSInteger)in_ShoppingId name:(NSString *)in_Name description:(NSString *)in_Description 
           location:(NSInteger)in_Location phone:(NSString *)in_Phone image:(NSString *)in_Image video:(NSString *)in_Video
               area:(NSString *)in_Area longitude:(NSString *)in_Longitude latitude:(NSString *)in_Latitude{
	
	self = [super init];
	if(self)
	{
		self.shoppingId = in_ShoppingId;
		self.name = in_Name;
		self.description = in_Description;
        self.location = in_Location;
		self.phone = in_Phone;
		self.image = in_Image;
		self.video = in_Video;
        self.area = in_Area;
        self.longitude = in_Longitude;
        self.latitude = in_Latitude;
	}
	return self;
}


- (void) dealloc {
	
	[name release];
	[image release];
	[description release];
    [phone release];
    [video release];
    [area release];
    [longitude release];
    [latitude release];
	[super dealloc];
}


@end
