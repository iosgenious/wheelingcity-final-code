//
//  SiteMapViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/26/10.
//  Copyright 2010 W. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ASIHTTPRequest.h"
#import "GMapDirectionsViewController.h"
#import "DataBaseSingleton.h"
#import "Map.h"
#import "LayarPlayer.h"

@interface SiteMapViewController : UIViewController <MKMapViewDelegate,MKMapViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIAlertViewDelegate> 
{
	IBOutlet MKMapView *map;
	NSArray *breweries;
    IBOutlet UITextField *categories;
    IBOutlet UIPickerView *pickerView;
    NSMutableArray *arrayNo;
    IBOutlet UIBarButtonItem *locationCurrent;
    MKAnnotationView *annotationView;
    NSString *urlString;
    DataBaseSingleton *dataBaseSingleton;
	NSMutableArray *diningDict;
    Map *theMap;
    UIBarButtonItem *actionButton;
    
    
    
}

@property (nonatomic, retain) NSMutableArray *diningDict;
@property (nonatomic, retain) UIBarButtonItem *actionButton;

-(IBAction) currentP:(id)sender;


@end