//
//  SponsorLoadViewController.h
//  JITH
//
//  Created by Urko Fernandez on 6/3/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SponsorLoadViewControllerDelegate;

@interface SponsorLoadViewController : UIViewController <UIWebViewDelegate> {

}
@property (nonatomic, assign) id <SponsorLoadViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end

@protocol SponsorLoadViewControllerDelegate

- (void) sponsorLoadViewControllerDidFinish:(SponsorLoadViewController *)controller;

@end
