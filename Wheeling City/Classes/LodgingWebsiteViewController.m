//
//  DiningWebsiteViewController.m
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//

#import "LodgingWebsiteViewController.h"

//
@implementation LodgingWebsiteViewController

@synthesize lodgingWeb;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	lodgingWeb.delegate = self;
	self.title = @"Website";
    lodgingWeb.scalesPageToFit = YES;
	UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(goBack:)];
	[[self navigationItem] setRightBarButtonItem:reloadButton];
	[reloadButton release];

}

- (void)goBack:(id) sender {
    [lodgingWeb goBack];
}

-(void) viewDidAppear:(BOOL)animated {
	self.navigationController.navigationBarHidden = FALSE;
	UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}

- (void) viewDidDisappear:(BOOL)animated {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;	
    UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}
- (void)webViewDidStartLoad:(UIWebView *)webView 
{
	//NSLog(@"web starts");

}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	[self blankPage];
	// Release any cached data, images, etc that aren't in use.
}

- (void) blankPage {
	[lodgingWeb loadHTMLString:@"<html><head></head><body></body></html>" baseURL:nil];
}
- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[lodgingWeb release];[lodgingWeb setDelegate:nil];
    lodgingWeb=nil;
   [super dealloc];
}


@end
