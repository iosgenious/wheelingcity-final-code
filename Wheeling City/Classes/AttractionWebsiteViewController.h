//
//  AttractionWebsiteViewController.h
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AttractionWebsiteViewController : UIViewController <UIWebViewDelegate>{
    UIWebView *attractionWeb;
}
@property (nonatomic, retain) IBOutlet UIWebView *attractionWeb;
- (void) blankPage;

@end
