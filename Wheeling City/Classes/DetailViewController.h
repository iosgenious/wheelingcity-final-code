//
//  DetailViewController.h
//  JITH
//
//  Created by Aitor Gutierrez Basauri on 10/03/11.
//  Copyright 2011 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JITHFeedsViewController.h"



@interface DetailViewController : UIViewController {
    
    /**
     IBOUtlets that appears in the view, it,s the comment that later it didn´t appear all, the name who writes the comment and the photo of the Facebook profile of him
     */
    IBOutlet UITextView *comment;
    IBOutlet UITextView *name;
    IBOutlet UIImageView* facebookPhoto;
    IBOutlet UIScrollView *scrollView;

    /*
     Variables that are passed for the other view controller and we stored in that ones
     */
    
    NSString *names;
    NSString *ids;
    NSString *comments; 
    NSDate *commentData;
}

@property (nonatomic,retain)    NSString *names;
@property (nonatomic,retain)    NSString *ids;

@property (nonatomic,retain)    NSDate *commentData;

@property (nonatomic,retain)    NSString *comments; 
@property (nonatomic,retain) IBOutlet UIScrollView *scrollView;


/**
 This is a method to convert the time that gives facebook that was the message written to put for example 4 hours ago, 3 minutes ago and something like that. It´s a method to convert the data
 */
-(NSString*)dateDiff:(NSDate *)origDate;

@end
