//
//  GoogleMapsViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import "GoogleMapsViewController.h"


@implementation GoogleMapsViewController 


/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = @"Maps";
	dataBaseSingleton = [[DataBaseSingleton alloc] init];

	CLLocationManager *locationManager=[[CLLocationManager alloc] init];
	locationManager.delegate=self;
	locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters;
	[locationManager startUpdatingLocation];
	
}

- (IBAction) showAddress {
	//Hide the keypad
	[addressField resignFirstResponder];
	MKCoordinateRegion region;
	MKCoordinateSpan span;
	span.latitudeDelta=0.2;
	span.longitudeDelta=0.2;
	
	CLLocationCoordinate2D location = [self addressLocation];
	region.span=span;
	region.center=location;
	
	if(addAnnotation != nil) {
		[mapView removeAnnotation:addAnnotation];
		[addAnnotation release];
		addAnnotation = nil;
	}
	
	addAnnotation = [[ParkPlaceMark alloc] initWithCoordinate:location];
	[mapView addAnnotation:addAnnotation];
	
	[mapView setRegion:region animated:TRUE];
	[mapView regionThatFits:region];
	//[mapView selectAnnotation:mLodgeAnnotation animated:YES];
}

-(void) parkCar {

	
	if(addAnnotation != nil) {
		[mapView removeAnnotation:addAnnotation];
		[addAnnotation release];
		addAnnotation = nil;
	}
	CLLocationCoordinate2D location = mapView.centerCoordinate;

	addAnnotation = [[ParkPlaceMark alloc] initWithCoordinate:location];
	addAnnotation.mTitle = @"Dude, here is my car!";
	NSString *latitude = [NSString stringWithFormat:@"%f",location.latitude];
	NSString *longitude = [NSString stringWithFormat:@"%f",location.longitude];
	addAnnotation.mSubTitle =[latitude stringByAppendingString:longitude];

	

	[mapView addAnnotation:addAnnotation];
	

}

/*
-(void) anotatePOI {
	ParkPlaceMark *poiAnnotation;
	//We don't delete other annotations until category changed
	//JITHAppDelegate *the_pAppDelegate = (JITHAppDelegate *)[[UIApplication sharedApplication] delegate];
	

	CLLocationCoordinate2D location;
	location.latitude = 40.075116;
	location.longitude = -80.697842;
	
	poiAnnotation = [[ParkPlaceMark alloc] initWithCoordinate:location];
	poiAnnotation.mTitle = @"Kroger";
	poiAnnotation.mSubTitle = @"Supermarket";
 
	//[dataBaseSingleton readPOIFromDatabase];
	[mapView addAnnotation:poiAnnotation];
	
}
 */

-(CLLocationCoordinate2D) addressLocation {
	NSString *urlString = [NSString stringWithFormat:@"http://maps.google.com/maps/geo?q=%@&output=csv", 
						   [addressField.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSString *locationString = [NSString stringWithContentsOfURL:[NSURL URLWithString:urlString] encoding:NSUTF8StringEncoding	 error:nil];
	NSArray *listItems = [locationString componentsSeparatedByString:@","];
	
	double latitude = 0.0;
	double longitude = 0.0;
	
	if([listItems count] >= 4 && [[listItems objectAtIndex:0] isEqualToString:@"200"]) {
		latitude = [[listItems objectAtIndex:2] doubleValue];
		longitude = [[listItems objectAtIndex:3] doubleValue];
	}
	else {
		//Show error
	}
	CLLocationCoordinate2D location;
	location.latitude = latitude;
	location.longitude = longitude;
	
	return location;
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
	MKPinAnnotationView *annView=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
	annView.pinColor = MKPinAnnotationColorGreen;
	annView.animatesDrop=TRUE;
	annView.canShowCallout = YES;
	annView.calloutOffset = CGPointMake(-5, 5);
	return annView;
}


/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
 [super viewDidLoad];
 }
 */

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (IBAction) showCarActionSheet {
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Dude, where's my car" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Mark your spot" otherButtonTitles:@"Get directions",nil];
	[actionSheet showInView:self.view];
	[actionSheet release];
}

- (IBAction) showViewActionSheet {
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Map Mode" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Standard" otherButtonTitles:@"Hybrid", @"Satellite", nil];
	[actionSheet showInView:self.view];
	[actionSheet release];
}

/*
- (IBAction) showPOIActionSheet {
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Points of Interest" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"ATM" otherButtonTitles:@"Lodging", @"Eating", @"Miscellanous", nil];
	[actionSheet showInView:self.view];
	[actionSheet release];
}
*/
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	NSLog(@"Which action sheet is this : %@", actionSheet.title);
	if ([actionSheet.title isEqualToString:@"Dude, where's my car"])
	{
		if (buttonIndex == 0) {
			NSLog(@"Marking your spot");
			[self parkCar];
			
		} else if (buttonIndex == 1) {
			NSLog(@"Not implemented, use webview?");    
		}
	}
	else if ([actionSheet.title isEqualToString:@"Select Map Mode"]){
		if (buttonIndex == 0) {
			NSLog(@"Standard view");
			mapView.mapType=MKMapTypeStandard;
		} else if (buttonIndex == 1) {
			NSLog(@"Hybrid view");   
			mapView.mapType=MKMapTypeHybrid;
		}
		else if (buttonIndex == 2) {
			NSLog(@"Satellite view");
			mapView.mapType=MKMapTypeSatellite;
		}
	}
	else {
		//Points of Interest
		if (buttonIndex == 0) {
			mapView.mapType=MKMapTypeStandard;
		} 
		else if (buttonIndex == 1) {
			mapView.mapType=MKMapTypeHybrid;
		}
		
		else if (buttonIndex == 2) {
			[self anotatePOI];
		}
		else if (buttonIndex == 3) {
			mapView.mapType=MKMapTypeSatellite;
		}
		
	}
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{

	CLLocationCoordinate2D location = newLocation.coordinate;
	//One location is obtained.. just zoom to that location
	
	MKCoordinateRegion region;
	region.center=location;
	//Set Zoom level using Span
	MKCoordinateSpan span;
	span.latitudeDelta=.005;
	span.longitudeDelta=.005;
	region.span=span;
	
	[mapView setRegion:region animated:TRUE];
	
}



- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
}


- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError *)error{
}

- (void)reverseGeocoder:(MKReverseGeocoder *)geocoder didFindPlacemark:(MKPlacemark *)placemark{
	NSLog(@"Geocoder completed");
	mPlacemark=placemark;
	[mapView addAnnotation:placemark];
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

@end