//
//  InfoViewController.m
//  JITH
//
//  Created by Urko Fernandez on 6/3/10.
//  Copyright 2010 W. All rights reserved.
//

#import "InfoViewController.h"
#import "DataBaseSingleton.h"


@implementation InfoViewController

@synthesize delegate=_delegate;
@synthesize infoWeb;

DataBaseSingleton *dataBaseSingleton;

- (void)webViewDidFinishLoad:(UIWebView *)_webView {
    [_webView setScalesPageToFit:true];
    NSLog(@"Hello finished info");
}

- (void)viewDidLoad {
    [super viewDidLoad];
	infoWeb.delegate = self;
    [infoWeb setScalesPageToFit:false];
    //  infoWeb.scalesPageToFit = YES;
    
    //Copy images from bundle if they are not already copied
    [self copyFromBundle];


   // [infoWeb loadHTMLString:@"" baseURL:nil];

}

-(void) viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [infoWeb setScalesPageToFit:false];
    //NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask ,YES );
    //NSString *documentsDirectory = [paths objectAtIndex:0];
    //NSString *path = [documentsDirectory stringByAppendingPathComponent:@"index.html"];
    //NSLog(@"The index.html is in the path: %@", path);
    NSString *urlAddress = @"http://wheeling.yourmobicity.com/English/info/index.html";
    
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [infoWeb loadRequest:requestObj];
//    [infoWeb loadHTMLString:@"http://www.google.com" baseURL:nil];
    
}
- (void)copyFromBundle {
    
    //It will iterate always even if files are copied
    dataBaseSingleton = [[DataBaseSingleton alloc] init];
    [dataBaseSingleton setDatabaseVars];
    NSArray *listOfFiles = [NSArray arrayWithObjects:@"chamber.gif", @"city.gif", @"cvb.gif", @"index.html", @"innovatec.jpg", @"redp.gif", @"w2w.gif", @"wnhac.gif", nil];
    for (NSString *file in listOfFiles) {
        NSLog(@"Current file is %@", file);
        [dataBaseSingleton CopyImage:file];
    }

    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate infoViewControllerDidFinish:self];
}


@end
