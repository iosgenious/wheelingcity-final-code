//
//  Shopping.h
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//


@interface Shopping : NSObject {
	NSInteger shoppingId;
	NSString *name;
	NSString *description;	
    NSInteger location;
    NSString *phone;
    NSString *image;  
    NSString *video;
    NSString *area;
    NSString *longitude;
    NSString *latitude;
}
  
@property (nonatomic, assign) NSInteger shoppingId, location;
@property (nonatomic, retain) NSString *name, *description, *phone, *image, *video, *area, *longitude, *latitude; 

- (id) initWithName:(NSInteger)in_ShoppingId name:(NSString *)in_Name description:(NSString *)in_Description 
			  location:(NSInteger)in_Location phone:(NSString *)in_Phone image:(NSString *)in_Image
              video:(NSString *)in_Video area:(NSString *)in_Area longitude:(NSString *)in_Longitude latitude:(NSString *)in_Latitude; 

@end