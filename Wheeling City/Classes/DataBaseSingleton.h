//
//  DataBaseSingleton.h
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//  

#import <Foundation/Foundation.h>
#import <sqlite3.h> 

@interface DataBaseSingleton : NSObject {
	
	//Database Variables
	NSString *databaseName;
	NSString *the_pDocumentsDir;
    
    NSString *newsTicker;

	NSMutableArray *lodgingArray;
    NSMutableDictionary *shoppingDict;
    NSMutableArray *areaArray;
    NSMutableArray *historicArray;
    NSMutableDictionary *attractionsDict;
   	NSMutableDictionary *diningDict;
	NSMutableDictionary *resourcesDict;
    NSMutableArray *resourceCategory;
    NSMutableArray *attractionCategory;
    NSMutableArray *coorArray;
    NSMutableArray *coorAttractionsArray;
    NSMutableArray *coorHistoricalArray;
    NSMutableArray *coorLodgingArray;
    NSMutableArray *coorShoppingArray;
    NSMutableArray *coorResourcesArray;
    NSMutableArray *categoryArray;
    
    //Layar configuration variables
	NSString *layarName;
    NSString *layarConsumerKey;
    NSString *layarConsumerSecret;
    bool layarIsAvailable;
    
	NSInteger partnerId;
    NSString *localMd5;
    
    CFMutableDictionaryRef connectionToInfoMapping;
    NSURLConnection *md5Conn, *sqlFileConn;
    NSFileHandle *file;
    bool finishedLoading;
}

@property (nonatomic, retain) NSMutableArray *lodgingArray;
@property (nonatomic, retain) NSMutableDictionary *shoppingDict;
@property (nonatomic, retain) NSMutableArray *areaArray;
@property (nonatomic, retain) NSMutableArray *historicArray;
@property (nonatomic, retain) NSMutableDictionary *attractionsDict;
@property (nonatomic, retain) NSMutableDictionary *diningDict;
@property (nonatomic, retain) NSMutableDictionary *resourcesDict;
@property (nonatomic, retain) NSString *databaseName;
@property (nonatomic, retain) NSString *databasePath;
@property (readwrite, copy) NSString *localMd5;
@property (nonatomic, retain) NSURLConnection *md5Conn, *sqlFileConn;
@property (nonatomic, retain) NSString *newsTicker;
@property (nonatomic, retain) NSMutableArray *coorArray;
@property (nonatomic, retain) NSMutableArray *coorAttractionsArray;
@property (nonatomic, retain) NSMutableArray *coorHistoricalArray;
@property (nonatomic, retain) NSMutableArray *coorLodgingArray;
@property (nonatomic, retain) NSMutableArray *coorShoppingArray;
@property (nonatomic, retain) NSMutableArray *coorResourcesArray;
@property (nonatomic, retain) NSMutableArray *categoryArray;
@property (nonatomic, retain) NSMutableArray *resourceCategory;
@property (nonatomic, retain) NSMutableArray *attractionCategory;
@property (nonatomic, retain) NSString *layarName;
@property (nonatomic, retain) NSString *layarConsumerKey;
@property (nonatomic, retain) NSString *layarConsumerSecret;

- (void)checkAndCreateDatabase;
- (void)readLodgingFromDatabase;
- (void)readShoppingFromDatabase;
- (void)readHistoricFromDatabase;
- (void)readAttractionsFromDatabase;
- (void)readDiningFromDatabase;
- (void)readResourcesFromDatabase;
- (void)readNewsTicker;
- (BOOL)isDBConnected;
- (BOOL)checkImage:(NSString *)imageName;
- (BOOL)CopyImage:(NSString *)imageName;
- (void)setDatabaseVars;
- (void)readCoordenatesDinning;
- (void)readCoordenatesAttractions;
- (void)readCoordenatesHistorical;
- (void)readCoordenatesLodging;
- (void)readCoordenatesShopping;
- (void)readCoordenatesResources;
- (void)readLayarConfiguration;
- (BOOL)isLayarAvailable;

///2- (void) CopyImageFromUrl:(NSURL *)imageURL;
///3- (void) cacheImages: (NSMutableDictionary *) images;
//add 4
- (NSString *) databasePath;



@end
