//
//  Dining.m
//  JITH
//
//  Created by Urko Fernandez on 3/23/11.
//  Copyright 2010 W. All rights reserved.
//

#import "Dining.h"



@implementation Dining

@synthesize diningId, name, description, location, phone, image, website, video, longitude, latitude; 

- (id) initWithName:(NSInteger)in_DiningId name:(NSString *)in_Name description:(NSString *)in_Description 
           location:(NSString *)in_Location phone:(NSString *)in_Phone image:(NSString *)in_Image
            website:(NSString *)in_Website video:(NSString *)in_Video longitude:(NSString *)in_Longitude latitude:(NSString *)in_Latitude{
	
	self = [super init];
	if(self)
	{
		self.diningId = in_DiningId;
		self.name = in_Name;
		self.description = in_Description;
        self.location = in_Location;
		self.phone = in_Phone;
		self.image = in_Image;
		self.website = in_Website;
		self.video = in_Video;
        self.longitude = in_Longitude;
        self.latitude = in_Latitude;
	}
	return self;
}


- (void) dealloc {
	
	[name release];
	[image release];
	[description release];
    [location release];
    [phone release];
    [website release];
    [video release];
    [longitude release];
    [latitude release];
	[super dealloc];
}

@end
