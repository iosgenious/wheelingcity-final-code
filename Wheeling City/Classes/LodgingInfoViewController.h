//
//  LodgingInfoViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataBaseSingleton.h"
#import <CoreLocation/CoreLocation.h>
#import "PagerViewController.h"
#import "LodgingWebsiteViewController.h"


@interface LodgingInfoViewController : UIViewController <UIActionSheetDelegate,CLLocationManagerDelegate> {

	DataBaseSingleton *dataBaseSingleton;
    UILabel *descriptionLabel;
    IBOutlet UIImageView* descriptionBubble;
    UIImageView *lodgingImage;
    UIScrollView *contentScroll;
    LodgingWebsiteViewController *myLodgingWebsiteViewController;
    NSString *lodgingUrl;
    NSString *lodgingYoutube;
    NSString *lodgingCall;
    NSString *lodgingLongitude;
    NSString *lodgingLatitude;
	BOOL sameLodging;
    UIBarButtonItem *optionsButton;
    UIImageView *backgroundImage;
}
@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UIImageView *descriptionBubble;
@property (nonatomic, retain) IBOutlet UIImageView *lodgingImage;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScroll;
@property (nonatomic, assign) BOOL sameLodging;
@property (nonatomic, retain) IBOutlet LodgingWebsiteViewController *myLodgingWebsiteViewController;
@property (nonatomic, retain) NSString *lodgingUrl;
@property (nonatomic, retain) NSString *lodgingYoutube;
@property (nonatomic, retain) NSString *lodgingCall;
@property (nonatomic, retain) NSString *lodgingLongitude;
@property (nonatomic, retain) NSString *lodgingLatitude;
@property (nonatomic, retain) UIBarButtonItem *optionsButton;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImage; 

- (void)callLodging;
- (void)bringLodgingWebsiteView;
- (void)bringLodgingVideoView;
- (void)bringGoogleMaps;


@end
