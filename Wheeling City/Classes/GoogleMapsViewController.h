//
//  GoogleMapsViewController.h
//  JITH
//
//  Created by Urko Fernandez on 3/22/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ParkPlaceMark.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKReverseGeocoder.h>
#import "DataBaseSingleton.h"


@interface GoogleMapsViewController : UIViewController <UIActionSheetDelegate, MKMapViewDelegate, MKReverseGeocoderDelegate, CLLocationManagerDelegate> {
	IBOutlet UITextField *addressField;
	IBOutlet UIButton *goButton;
	IBOutlet MKMapView *mapView;
	DataBaseSingleton *dataBaseSingleton;
	
	MKPlacemark *mPlacemark;

	ParkPlaceMark *addAnnotation;

}

- (IBAction) showAddress;

- (IBAction) showCarActionSheet;

- (IBAction) showViewActionSheet;

//- (IBAction) showPOIActionSheet;


- (CLLocationCoordinate2D) addressLocation;

@end
