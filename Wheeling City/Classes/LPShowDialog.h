//
//  LPShowDialog.h
//  layarplayer
//
//  Created by Nikita Ivanyushchenko on 01/25/11.
//  Copyright (c) 2011 Layar B.V. All rights reserved.
//

//System imports
#import <UIKit/UIKit.h>

#define LPShowDialogActionFired	@"LPShowDialogActionFired"


@class LPShowDialog;

@protocol LPShowDialogDelegate <NSObject>
- (void)showDialogWillClose:(LPShowDialog*)aShowDialog;
@end


@class LSAction;
@class LSShowDialogData;
@class LPImageRequestManager;
@class LPImageRequest;
@class LPCloseButton;
@class LPExtendedBIWButtonContainer;

@interface LPShowDialog : UIView <UIActionSheetDelegate>
{	
	LSShowDialogData *showDialogData;

	UILabel *titleLabel;
	UILabel *descriptionLabel;
	UIImage *placeholder;
	UIImageView *imageView;
	
	NSArray *moreActions;
	
	UIView *centerContainer;
	LPCloseButton *closeButton;
	LPExtendedBIWButtonContainer * buttonContainer;
	
	id<LPShowDialogDelegate> delegate;
	
	LPImageRequestManager *imageRequestManager;
	LPImageRequest *imageRequest;
	
	UIWindow * backWindow;
}
@property (nonatomic, assign) id<LPShowDialogDelegate> delegate;


- (id)init;
- (void)setShowDialogData:(LSShowDialogData*)aShowDialogData;
- (void)close;

@end
