//
//  LodgingWebsiteViewController.h
//  JITH
//
//  Created by Urko Fernandez on 4/26/10.
//  Copyright 2010 W. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LodgingWebsiteViewController : UIViewController <UIWebViewDelegate>{
    UIWebView *lodgingWeb;
}
@property (nonatomic, retain) IBOutlet UIWebView *lodgingWeb;
- (void) blankPage;

@end
