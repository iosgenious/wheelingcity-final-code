//
//  SponsorLoadViewController.m
//  JITH
//
//  Created by Urko Fernandez on 6/3/10.
//  Copyright 2010 W. All rights reserved.
//

#import "SponsorLoadViewController.h"
#import "DataBaseSingleton.h"


@implementation SponsorLoadViewController

@synthesize delegate=_delegate;

DataBaseSingleton *dataBaseSingleton;



- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    webView.backgroundColor = [UIColor blackColor];
    webView.opaque = YES;
}

-(void) viewDidAppear:(BOOL)animated {

}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}

#pragma mark - Actions

- (IBAction)done:(id)sender
{
    [self.delegate sponsorLoadViewControllerDidFinish:nil];
}


@end
