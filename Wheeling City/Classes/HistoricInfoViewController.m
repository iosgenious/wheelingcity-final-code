//
//  HistoricInfoViewController.m
//  JITH
//
//  Created by Urko Fernandez on 3/29/10.
//  Copyright 2010 W. All rights reserved.
//

#import "HistoricInfoViewController.h"
#import "NetworkReachability.h"
#import "Macro.h"

@implementation HistoricInfoViewController

@synthesize descriptionLabel, descriptionBubble, historicImage, contentScroll, sameHistoric,historicYoutube,historicLatitude,historicLongitude,actionButton;

UIWebView  *webview;
CLLocationManager *GPSlocation;
double userLongitude;
double userLatitude;

- (void)viewDidLoad {
    [super viewDidLoad];
	dataBaseSingleton = [[DataBaseSingleton alloc] init];
    actionButton = [[UIBarButtonItem alloc] initWithTitle:@"Options" style:UIBarButtonItemStylePlain target:self action:@selector(showCategories:)];
    [[self navigationItem] setRightBarButtonItem:actionButton];
    [actionButton release];
	contentScroll.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	descriptionLabel.userInteractionEnabled = TRUE;
}

- (void)showCategories:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose an option" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Video", @"Get me there!", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
        [self bringHistoricVideoView];
    }
    else if (buttonIndex == 1) {
        // ToDo: GoogleMaps
        [self bringGoogleMaps];
    }
}

-(void)bringGoogleMaps
{
    if(historicLatitude!=nil || historicLongitude!=nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Go to that location" message:@"This will open the Maps App" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alert show];
        [alert release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No location" message:@"There is no location information" delegate:self
                                              cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (void)checkNetwork{
	
	UIAlertView *errorView;
	NetworkReachability* internetReach;
	internetReach = [[NetworkReachability reachabilityForInternetConnection] retain];
	NetworkStatus netStatus = [internetReach currentReachabilityStatus];
	[internetReach release];
	if (netStatus == NotReachable) {
		// Could be ReachableViaWWAN and ReachableViaWiFi
		
		//NSLog(@"NotReachable");
		
		errorView = [[UIAlertView alloc]
					 initWithTitle: @"Network Error"
					 message: @"There is no internet conection!"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
        return;
	}
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    userLatitude=newLocation.coordinate.latitude;
    userLongitude=newLocation.coordinate.longitude;
    [GPSlocation stopUpdatingLocation];
    [GPSlocation release];
    [self openMap:[NSString stringWithFormat:@"saddr=%f,%f&daddr=%@,%@&output=dragdir",userLatitude,userLongitude,historicLatitude,historicLongitude]];
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex!=0)
    {
        GPSlocation=[[CLLocationManager alloc]init];
        GPSlocation.delegate=self;
        [GPSlocation startUpdatingLocation];
    }
}

- (void)bringHistoricVideoView {
    
    if (!historicYoutube)
    {
        UIAlertView *errorView;
        errorView = [[UIAlertView alloc]
					 initWithTitle: @"No Video"
					 message: @"There is no promotional video available"
					 delegate: self
					 cancelButtonTitle: @"OK" otherButtonTitles: nil];
		[errorView show];
		[errorView autorelease];
        
    }
    else{
        [self checkNetwork];
        YouTubePlayerController * yPlayer=[[YouTubePlayerController alloc] initWithNibName:@"YouTubePlayerController" bundle:nil];
        [yPlayer setWeburl:historicYoutube];
        [self.navigationController pushViewController:yPlayer animated:YES];
        [yPlayer release];
        
    }
}

-(void) viewDidAppear:(BOOL)animated {
	UIActivityIndicatorView *tmpimg = (UIActivityIndicatorView *)[self.view viewWithTag:1];
	[tmpimg removeFromSuperview];
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

#pragma mark - Memory Management Method

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidUnload{
    [super viewDidUnload];
    [descriptionLabel release];self.descriptionLabel=nil;
	[historicImage release];self.historicImage=nil;
	[contentScroll release];self.contentScroll=nil;
}
- (void)dealloc {
	[descriptionLabel release];self.descriptionLabel=nil;
	[historicImage release];self.historicImage=nil;
	[contentScroll release];self.contentScroll=nil;
	[super dealloc];
}

#pragma mark - Map Method

-(void)openMap:(NSString*)urlString{
    NSString *url;
    
    if([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
    {
        url = [NSString stringWithFormat:@"http://maps.google.com/maps?%@",urlString];
    }
    else
    {
        url = [NSString stringWithFormat:@"http://maps.apple.com/maps?%@",urlString];
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}

@end

