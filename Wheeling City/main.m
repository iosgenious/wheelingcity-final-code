    //
//  main.m
//  JITH
//
//  Created by Urko Fernandez on 3/12/10.
//  Copyright Wheeling Jesuit University 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}
